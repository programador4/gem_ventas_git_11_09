	@section("preload")

	@show



 @section("header")
			<header class="encabezado">
			<div class="idioma"><a href="./en/"><img src="{{ URL::asset('assets/img/icons/bandera-en.png') }}" alt="ingles" /></a></div>

			<div class="navigation menu">
				<label class="control" for="control-menu">
					<div class="icon">
						<span></span>
					</div>
				</label>
				<div class="lista">
					<div class="contiene-lista">
						<div class="logo-atk-menu">
							<a href="{!! url(''); !!}">
								<div align="center">
									<img src="{{ URL::asset('assets/img/atk-digital-marketing-blanco.png') }}" alt="Atomikal Digital Marketing" />
								</div>
							</a>
						</div>
						<nav class="nav-lista">
							<ul>
								<li>
									<a href="{!! url('/quienes-somos'); !!}">Quiénes somos</a>
								</li>
								<li>
									<a href="{!! url('/que-hacemos'); !!}" class="activo">Qué hacemos</a>
								</li>
								<!--<li>
									<a href="nuestros-proyectos.html">Nuestros proyectos</a>
								</li>-->
								<li>
									<a href="{!! url('/blog'); !!}">Blog</a>
								</li>
								<li>
									<a href="{!! url('/contacto'); !!}">Contacto</a>
								</li>
							</ul>
						</nav>
						<aside class="contacto">
							<div class="redes-sociales">
								<a href="https://www.instagram.com/atomikal/"  target="_blank">
									<span class="icon-rs-ins"></span>
								</a>
								<a href="https://www.facebook.com/Atomikal/" target="_blank">
									<span class="icon-rs-fb"></span>
								</a>
								<a href="https://twitter.com/atomikal" target="_blank">
									<span class="icon-rs-tw"></span>
								</a>
								<a href="https://www.youtube.com/user/atomikalperu" target="_blank">
									<span class="icon-youtube"></span>
								</a>
								<a href="#">
									<span class="icon-rs-in"></span>
								</a>
							</div>
							<ul>
								<li>
									<span class="icon-telefonos ico-web"></span> 01 421-3313</li>
								<li>
									<span class="icon-email ico-web"></span> contacto@atomikal.com</li>
								<li>
									<span class="icon-direccion ico-web"></span> Calle Mariano Melgar #225, Miraflores</li>
							</ul>
						</aside>
					</div>
				</div>
			</div>
		</header>
 @show


 