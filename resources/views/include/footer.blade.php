						@section("footer")
						<footer id="footer">
							<div class="contenedor">
								<div class="copy">© Copyright 2017 - Atomikal</div>
								<aside class="social">
									<a href="https://www.instagram.com/atomikal/" target="_blank">
										<span class="icon-rs-ins"></span>
									</a>
									<a href="https://www.facebook.com/Atomikal/" target="_blank">
										<span class="icon-rs-fb"></span>
									</a>
									<a href="https://twitter.com/atomikal" target="_blank">
										<span class="icon-rs-tw"></span>
									</a>
									<a href="https://www.youtube.com/user/atomikalperu" target="_blank">
										<i class="fa fa-youtube-play" aria-hidden="true"></i>
									</a>
									<a href="#">
										<span class="icon-rs-in"></span>
									</a>

								</aside>
							</div>
						</footer>
						@show