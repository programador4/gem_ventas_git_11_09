
    @section("cloud_base_js")
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js"></script>
    @show

    @section("base_js")
    <!--js principales-->
    <script type="text/javascript" src="{{ URL::asset('assets/js/picturefill.min.js') }}" async></script>
    <script type="text/javascript" src="{{ URL::asset('assets/js/general.js') }}" async defer></script>

    <!--js principales-->
    @show
