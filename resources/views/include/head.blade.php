    @section("metatags-utf")
        <meta charset="UTF-8">
        <meta charset="utf8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />

    @show

    <!--Metas-->
        @section("metattags-social")
            <meta name="description" content="Somos una agencia digital que piensa globalmente para actuar en un ámbito que conoce bien: las comunicaciones, la publicidad, las marcas y todo lo que nos ofrece la tecnología que dominamos.">
            <meta name="keywords" content="atk, atomikal, agencia digital, marketing digital, publicidad, desarrollo web, social media, app, aplicativos móviles">
             <!-- Twitter -->
             <meta name="twitter:card" content="summary_large_image">
             <meta name="twitter:site" content="@Atomikal">
             <meta name="twitter:title" content="Atomikal - Mercado Negro">
             <meta name="twitter:description" content="Somos una agencia digital que piensa globalmente para actuar en un ámbito que conoce bien: las comunicaciones, la publicidad, las marcas y todo lo que nos ofrece la tecnología que dominamos.">
             <meta name="twitter:image" content="http://atomikalperu.com/cms-adtualizate/public/img/seo/ADtualizate-share.jpg">
             <!-- SEO -->
             <!-- Facebook  -->
             <meta property="og:title" content="ADtualizate - Mercado Negro" />
             <meta property="og:type" content="website" />
             <meta property="og:url" content="http://atomikalperu.com/public" />
             <meta property="og:image" content="http://atomikalperu.com/cms-adtualizate/public/img/seo/ADtualizate-share.jpg" />
             <meta property="og:site_name" content="ADtualizate - Mercado Negro" />
             <meta property="og:description" content="Somos una agencia digital que piensa globalmente para actuar en un ámbito que conoce bien: las comunicaciones, la publicidad, las marcas y todo lo que nos ofrece la tecnología que dominamos." />
    @show



    @section("favicos")
    <!--favico-->
    <link rel="icon"  href="{{ URL::asset('assets/css/img/icono.png') }}" type="image/png">
    <link rel="shortcut icon" href="{{ URL::asset('assets/img/icono.png') }}" />
    <!---->
    @show



    @section("cloud_css")
        <!--Css cloud-->
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
    @show

    <!--Css bases-->
    @section("base_css")
    <link rel="stylesheet" href="{{ URL::asset('assets/css/general.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/slick.css') }}" media="screen" title="estilo">
    @show
    <!--Css bases-->







    @section("header_scripts")
    @push('pictures')
    <script>
        document.createElement("picture");
    </script>
    @endpush
    @push('google_analitics')
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-92144403-3', 'auto');
        ga('send', 'pageview');

    </script>
    @endpush

    @stack('pictures')
    @stack('google_analitics')
    @show



