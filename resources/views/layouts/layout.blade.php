    <head>
        <title>Atomikal Marketing Digital - @yield('title')</title>
        @include('../include/head')

    </head>

    <body  @yield('initialize')>
        @include('../include/header')


        @section("content")
        @show
        @include('../include/footer')
        @include('../include/scripts')
    <body>