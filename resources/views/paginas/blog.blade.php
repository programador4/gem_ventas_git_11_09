    @extends('..\layouts\master')




    @section('base_css')
        @parent
        <link rel="stylesheet" href="{{ URL::asset('assets/css/blog.css')}}" media="screen" title="estilo">

    @endsection

    @section("header_scripts")
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    @endsection





    @section('title', 'Blog')



    @section('content')
        <section class="pagina">
            <article id="cuerpo">
                <header class="head-tit">
                    <div class="contenedor">
                        <a href="index.html">
                            <img src="{{ URL::asset('assets/img/atk-digital-marketing.png')}}" />
                        </a>
                    </div>
                </header>


                <section class="cabecera-blog cabecera-1">
                    <div class="descrip-banner">
                        <a href="blog-det.html"></a>
                        <div class="contenedor">
                            <div class="frase1 wow bounceInLeft" data-wow-delay=".5s">
                                <h2>
                                    La digitalización del retail
                                    <span>es lo que marca la diferencia</span>
                                </h2>
                                <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. This is Photoshop's version.</p>
                            </div>
                        </div>

                    </div>

                </section>

                <section class="cabecera-blog cabecera-2">
                    <div class="descrip-banner">
                        <a href="blog-det.html"></a>
                        <div class="contenedor">
                            <div class="frase2">
                                <h2>
                                    La digitalización del retail
                                    <span>es lo que marca la diferencia</span>
                                </h2>
                                <p>This is Photoshop's version of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. This is Photoshop's version.</p>
                            </div>
                        </div>
                    </div>

                </section>


                <section class="contenido-blog">
                    <div class="contenedor">
                        @foreach($articles as $key => $article)
                            @if($key==0)
                            <article class="blog-item">

                                <div class="imagen-blog posi01 wow slideInLeft parallax-lvl-1 "><a  href="{{"blog/".$slug.$article->slug}}"><img src="{{asset("../storage/app/articles/".$article->front_image)}}" /></a></div>
                                <div class="box-blog posi01 wow slideInRight parallax-lvl-2">
                                    <a href="{{"blog/".$slug.$article->slug}}">
                                        <div class="blog-int">
                            @endif

                            @if($key==1)
                                <article class="blog-item ">
                                    <div class="imagen-blog posi02 wow slideInRight parallax-lvl-1"><a  href="{{"blog/".$slug.$article->slug}}"><img src="{{asset("../storage/app/articles/".$article->front_image)}}" /></a></div>
                                    <div class="box-blog posi02 wow slideInLeft parallax-lvl-2">
                                        <a href="{{"blog/".$slug.$article->slug}}">
                                            <div class="blog-int">
                            @endif

                            @if($key==2)
                                <article class="blog-item blog-item-ultimo">
                                    <div class="imagen-blog posi01 wow slideInLeft parallax-lvl-4"><a  href="{{"blog/".$slug.$article->slug}}"><img src="{{asset("../storage/app/articles/".$article->front_image)}}" /></a></div>
                                    <div class="imagen-blog posi01 wow slideInLeft parallax-lvl-4"><a  href="{{"blog/".$slug.$article->slug}}"><img src="{{asset("../storage/app/articles/".$article->front_image)}}" /></a></div>
                                    <div class="box-blog posi01 wow slideInRight parallax-lvl-3">
                                        <a href="{{"blog/".$slug.$article->slug}}">
                                            <div class="blog-int">
                            @endif

                                            <h2>{{$article->title}}</h2>
                                            {!!$article->description!!}
                                        </div>
                                    </a>
                                </div>
                            </article>
                        @endforeach

                        {{--<article class="blog-item">--}}
                            {{--<div class="imagen-blog posi01 wow slideInLeft parallax-lvl-1 "><a  href="blog-det.html"><img src="img/blog/articulo-1.jpg" /></a></div>--}}
                            {{--<div class="box-blog posi01 wow slideInRight parallax-lvl-2">--}}
                                {{--<a href="blog-det.html">--}}
                                    {{--<div class="blog-int">--}}
                                        {{--<h2>La digitalización del retail ES LO QUE MARCA LA DIFERENCIA</h2>--}}
                                        {{--<p>Marco Muñoz, nos cuenta como la revolución digital ha impactado los medios tradicionales, y cómo el panorama de la digitalización del punto de venta está cambiando como los consumidores se relacionan con las marcas en el momento de la compra. </p>--}}
                                    {{--</div>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</article>--}}



                        {{--<article class="blog-item ">--}}
                            {{--<div class="imagen-blog posi02 wow slideInRight parallax-lvl-1"><a  href="blog-det.html"><img src="{{asset("../storage/app/articles/".$article->front_image)}}" /></a></div>--}}
                            {{--<div class="box-blog posi02 wow slideInLeft parallax-lvl-2">--}}
                                {{--<a href="#">--}}
                                    {{--<div class="blog-int">--}}
                                        {{--<h2>This is Photoshop's version  of Lorem Ipsum</h2>--}}
                                        {{--<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh </p>--}}

                                    {{--</div>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</article>--}}



                        {{--<article class="blog-item blog-item-ultimo">--}}
                            {{--<div class="imagen-blog posi01 wow slideInLeft parallax-lvl-4"><a  href="blog-det.html"><img src="{{asset("../storage/app/articles/".$article->front_image)}}" /></a></div>--}}
                            {{--<div class="imagen-blog posi01 wow slideInLeft parallax-lvl-4"><a  href="blog-det.html"><img src="{{asset("../storage/app/articles/".$article->front_image)}}" /></a></div>--}}
                            {{--<div class="box-blog posi01 wow slideInRight parallax-lvl-3">--}}
                                {{--<a href="#">--}}
                                    {{--<div class="blog-int">--}}
                                        {{--<h2>This is Photoshop's version  of Lorem Ipsum</h2>--}}
                                        {{--<p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh </p>--}}

                                    {{--</div>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</article>--}}

                    </div>
                </section>

            </article>
        </section>
        <aside id="menu-inferior">
            <div class="contenedor">
                <div class="col">
                    <h5>Artículos anteriores</h5>
                    <ul class="lista-li2">
                        @foreach($last_articles as $article)
                            <li><a href="{!! url('blog/'.$article->slug)!!}">{{$article->title}}</a></li>
                        @endforeach

                    </ul>
                </div>

                <div class="col">
                    <h5>etiquetas</h5>
                    <div class="etiquetas">
                        @foreach($tags as $tag)
                            <a href="{!! url('blog/tag/'.$tag->slug) !!}"><span>{{$tag->name}}</span></a>
                        @endforeach
                        {{--<a href="#"><span>Digital</span></a>--}}
                            {{--<a href="#"><span>marketing</span></a>--}}
                        {{--<a href="#"><span>ceo</span></a>--}}
                        {{--<a href="#"><span>ceo</span></a>--}}
                        {{--<a href="#"><span>neuromarketing</span></a>--}}
                    </div>

                </div>

                <div class="col">
                    <h5>categorías</h5>
                    <ul class="lista-li2">
                        @foreach($categories as $category)
                        <li><a href="{!! url('blog/category/'.$category->slug) !!}">{{$category->name}}</a></li>
                        @endforeach
                        {{--<li><a href="#">Marketing</a></li>--}}
                        {{--<li><a href="#">CEO</a></li>--}}
                        {{--<li><a href="#">Beacons</a></li>--}}
                    </ul>
                </div>
            </div>
        </aside>
    @endsection

    @section("cloud_base_js")
        @parent
        <script src="https://cdnjs.cloudflare.com/ajax/libs/graingert-wow/1.2.2/wow.min.js"></script>
        <script type="text/javascript" src="js/waypoints.min.js"></script>
    @endsection




    @section("base_js")
        @parent
        <script src="{{URL::asset('assets/js/blog.js')}}" defer></script>
    @endsection


    @section('initialize', " onload=initialize() ")


