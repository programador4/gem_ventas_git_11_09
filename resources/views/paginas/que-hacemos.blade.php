@extends('..\layouts\master')

@section('base_css')
    @parent
    <link rel="stylesheet" href="{{ URL::asset('assets/css/hacemos.css')}}" media="screen" title="estilo">

    <style>
        @foreach($services as $key => $service)
        .info-servicios #tab-nav-{{$key+1}}:checked ~ .bg-all1 {
                    background: {{$service->color_hex}};
        }

        @endforeach

    </style>
@endsection

@section('title', 'Que hacemos')



@section('content')
    <section class="pagina">
        <article id="cuerpo">
            <div class="contenedor">
                <header class="head-tit">
                    <a href="index.html">
                        <img src="{{ URL::asset('assets/img/atk-digital-marketing-en-blanco.png')}}" />

                    </a>
                </header>
                <div class="info-servicios tabbed">
                    @foreach($services as $key => $service)
                        <input type="radio" name="tabs" id="tab-nav-{{$key+1}}" @if($key+1==1) checked @endif>
                        <label for="tab-nav-{{$key+1}}">{{$service->name}}</label>

                    @endforeach
                    
                    <div class="bg-all1"></div>






                    <div class="tabs">
                        @foreach($services as $key => $service)
                        <div class="informacion"  id="tab_{{$key+1}}">
                            {{--@if($id==$service->id) checked class="top-placeh"  @endif--}}
                            <div class="imgx">
                             {!!$service->canvas_description!!}
                            </div>
                            <div class="box-texto">
                                <h2>{{$service->name }}</h2>
                                <h3>{{$service->subtitle_service }}</h3>
                                <p>{!! $service->activity_description !!}</p>
                               <!-- <a href="nuestros-proyectos.html" class="btn1 btn-conoce creativ1">
                                    <span class="tx1">Conoce nuestros</span> casos de éxito
                                    <span class="sig">></span>
                                </a>-->
                            </div>
                        </div>
                        @endforeach




                    </div>
                </div>
            </div>
            <section id="formulario">
                <div id="parallax">
                    <div class="layer1" data-depth="0.4">
                        <div id="particles-js"></div>
                    </div>
                    <div class="layer" data-depth="0.3">
                        <div class="some-more-space"></div>
                    </div>
                    <div class="layer2" data-depth="0.4">
                        <div id="particles2-js"></div>
                    </div>
                </div>
                <form action="{{ action('Web\WebController@postContacto') }}" method="post" class="form-foot">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <h3 id="contacto">CONTÁCTATE CON NOSOTROS</h3>
                    @if ($errors->any())
                        <div class="">

                            <!--Checar si la validacion esta fallida o correcta-->

                            <div class="form-label">
                                <label class="w-100">
                                    <div class="error-box">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </label>
                            </div>

                            <!--Checar si la validacion esta fallida o correcta-->

                        </div>
                    @endif
                    @if(session()->has('mensaje'))
                        <br>
                        <div class="form-label">
                            <h2 align="center">{{   Session::get('mensaje')  }}</h2>
                        </div>
                        <br>

                    @else
                        <div class="columnas">

                            <div class="colum-1">
                                <label>
                                    <input type="text"  name="name" class="campo" placeholder="Nombre" name="" required>
                                    <span class="top-p">Nombre</span>
                                </label>

                                <label>
                                    <input type="text" class="campo" name="phone" placeholder="Teléfono de contacto"  required>
                                    <span class="top-p">Teléfono de contacto</span>
                                </label>

                                <label>
                                    <input type="email"  class="campo" placeholder="E-mail"  name="email" required><span class="top-p">E-mail</span>
                                </label>

                            </div>
                            <div class="colum-2">
                                <label>
                                    <textarea rows="6" class="campo textarea" name="question" rows="5" placeholder="Tu consulta" required></textarea>
                                    <span class="top-p">Tu consulta</span>
                                </label>
                            </div>
                        </div>
                    @endif
                    <button type="submit" class="btn1 btn2" name="">ENVIAR
                        <span class="sig"> > </span>
                    </button>
                </form>
            </section>
        </article>
    </section>
@endsection

@section("cloud_base_js")
    @parent


    <script src="http://matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>
    <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>

@endsection

@section("base_js")
    @parent
    <script>
        $(function() {

            @foreach($services as $key => $service)

                if (location.hash === "#{{$service->slug}}") {
                    document.getElementById("tab-nav-{{$key+1}}").checked = true;
                }

            @endforeach

            if (location.hash === "#contacto") {
//                $("#contacto").focus();
                $(window).scrollTop($('#contacto').offset().top);
            }

        });
    </script>

    <script type="text/javascript" src="{{URL::asset('assets/js/picturefill.min.js')}}" async></script>
    <script src="{{URL::asset('assets/js/hacemos.js')}}" defer></script>

@endsection



