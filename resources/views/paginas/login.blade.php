@extends('cms\layouts\master')

@section("css")
    {{--<link rel="stylesheet" href="css/general.css">--}}
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{URL::asset("assets/cms/css/login.css")}}" media="screen" title="estilo">
@endsection

@section('title', 'Dashboard')

@section("header")

@overwrite

@section('content')
    <section class="pagina">
        <main id="cuerpo">
            <section class="portada-home">
                <article id="login">


                    <!--Checar si la validacion esta fallida o correcta-->

                    <div class="form-label">
                        <label class="w-100">
                            <div class="error-box">
                                <ul>

                                </ul>
                            </div>
                        </label>
                    </div>

                    <!--Checar si la validacion esta fallida o correcta-->

                    <div class="form-login">
                        @foreach ($errors->all() as $error)
                        <div class="error">{{ $error }}</div>
                        @endforeach
                        @if(session()->has('mensaje'))

                        <div class="error">{{   Session::get('mensaje')  }}</div>

                        @endif
                        <form action="{{ action('Login\LoginController@login') }}" method="post">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="logo">
                                <img src="{{URL::asset("assets/cms/img/atomikal-logo.png")}}"  alt="Atomikal" />
                            </div>
                            <p class="tex">Sistema de ventas</p>
                            <!--<div class="error">
                    El usuario y/o la contraseña que ha ingresado no es correcto
                 </div>-->
                            <div class="grupo-label">
                                <input type="text" name="user" class="camp" placeholder="Usuario" required="required"> </div>
                            <div class="grupo-label">
                                <input type="password" name="password" class="camp" placeholder="Clave" required="required"> </div>
                            <input type="submit" name="" value="Ingresar" class="boton-1" /> </div>
                    </form>
                    </div>
                </article>
            </section>
        </main>
    </section>
@endsection

@section("js")

@endsection


