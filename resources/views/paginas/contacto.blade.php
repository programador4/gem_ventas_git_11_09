    @extends('..\layouts\master')

    @section('base_css')
        @parent
        <link rel="stylesheet" href="{{ URL::asset('assets/css/contacto.css')}}" media="screen" title="estilo">

    @endsection

    @section("header_scripts")

        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>


    @endsection


    @section('initialize', " onload=initialize() ")


    @section('title', 'Contacto')



    @section('content')
        <section class="pagina">
            <article id="cuerpo">
                <div id="parallax">
                    <div class="layer1" data-depth="0.4">
                        <div id="particles-js"></div>
                    </div>
                    <div class="layer" data-depth="0.3">
                        <div class="some-more-space"></div>
                    </div>
                    <div class="layer2" data-depth="0.4">
                        <div id="particles2-js"></div>
                    </div>
                </div>
                <div class="contenedor">
                    <header class="head-tit">
                        <a href="index.html">
                            <img src="{{URL::asset('assets/img/atk-digital-marketing-2.png')}}" />
                        </a>
                        <h2>Si quieres conocer más sobre el mundo digital</h2>
                        <h3>escríbenos y pronto te responderemos.</h3>

                    </header>
                    <h3 class="contactate">CONTÁCTATE CON NOSOTROS</h3>
                    <form action="{{ action('Web\WebController@postContacto') }}" method="post">
                        <article class="metodos-contacto">
                            <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <div class="formulario">
                                @if ($errors->any())
                                    <div class="">

                                <!--Checar si la validacion esta fallida o correcta-->

                                        <div class="form-label">
                                            <label class="w-100">
                                                <div class="error-box">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </label>
                                        </div>

                                <!--Checar si la validacion esta fallida o correcta-->

                                    </div>
                                @endif



                                <!--Checar si existe la sesion rapida-->

                                @if(session()->has('mensaje'))

                                <div class="form-label">
                                        <h2 align="center">{{   Session::get('mensaje')  }}</h2>
                                </div>
                                </div>
                                @else 
         


                                <!--Checar si existe la sesion rapida-->



                                <div class="form-label dos-col">
                                    <label>
                                        <input id="fname" type="text" class="campo" placeholder="Nombre" name="name" required="">
                                        <span class="top-p">Nombre</span>

                                    </label>
                                    <label>
                                        <input type="text" class="campo" placeholder="Teléfono de contacto" name="phone" required="">
                                        <span class="top-p">Teléfono de contacto</span>
                                    </label>
                                </div>
                                <div class="form-label">
                                    <label class="w-100">
                                        <input type="email" class="campo" placeholder="E-mail" name="email" required="">
                                        <span class="top-p">E-mail</span>
                                    </label>
                                </div>
                                <div class="form-label">
                                    <label class="w-100">
                                        {{Form::textarea('question')}}
                                        <span class="top-p">Tu consulta</span>
                                    </label>
                                </div>
                                <div class="form-label dos-col">
                                    <div class="g-recaptcha" data-sitekey="6Lf1eBsUAAAAAAFuwid8LpXvnFXVuE60D58dSCx0"></div>
                                    <button type="submit" class="enviar btn1 btn2" name="">ENVIAR
                                        <span class="sig">></span>
                                    </button>
                                </div>
                            </div>

                                @endif
                            <div class="mapa">
                                <ul>
                                    <li>
                                        <span class="icon-telefonos ico-web"></span> 01 421-3313</li>
                                    <li>
                                        <span class="icon-email ico-web"></span> contacto@atomikal.com</li>
                                    <li>
                                        <span class="icon-direccion ico-web"></span> Calle Mariano Melgar #225, Miraflores</li>
                                </ul>
                                <div id="map_canvas"></div>
                            </div>
                        </article>
                    </form>
                </div>
            </article>
        </section>
    @endsection

    @section("cloud_base_js")
        @parent
        <script src="https://www.google.com/recaptcha/api.js" async></script>
        <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
        <script src="http://matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>

    @endsection




    @section("base_js")
        @parent
      <script src="{{URL::asset('assets/js/contacto.js')}}" defer></script>
    @endsection





