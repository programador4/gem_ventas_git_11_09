@extends('..\layouts\master')

@section('base_css')
    @parent
    <link rel="stylesheet" href="{{ URL::asset('assets/css/blog-det.css')}}" media="screen" title="estilo">

@endsection

@section("header_scripts")
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
@endsection





@section('title', 'Blog')



@section('content')
    <section class="pagina">
        <article id="cuerpo">
            <header class="head-tit">
                <div class="contenedor">
                    <a href="index.html">
                        <img src="{{ URL::asset('assets/img/atk-digital-marketing.png')}}" />
                    </a>
                </div>
            </header>
            <section class="cabecera-blog-det">
                <div class="descrip-banner"></div>
            </section>




            <section class="contenido-blog">
                <div class="contenedor">
                    <header class="titulo-general">
                        <h2>
                            La digitalización del retail
                            <span>es lo que marca la diferencia</span>
                        </h2>
                    </header>
                    <p><b>Marco Muñoz</b>, nos cuenta como la revolución digital ha impactado los medios tradicionales, y cómo el panorama de la digitalización del punto de venta está cambiando como los consumidores se relacionan con las marcas en el momento de la compra.</p>

                    <h3>Digitalización del Punto de Venta, ¿qué significa?</h3>

                    <p>Consiste en implementar tecnologías Digitales dentro del Punto de Venta (PdV), que permitan la interacción de los Consumidores con los puntos de contacto virtuales de la marca.</p>

                    <p>Dentro del PdV, es posible implementar elementos de Digital Signage (Pantallas Digitales Táctiles), Digital KIOSKs, Bluetooth Beacons, Códigos QR, NFC Tags, APPs con Realidad Aumentada & Free WiFi con Captive Portals que permitan esta interacción Digital en un espacio físico.</p>

                    <p>Estas herramientas tienen como finalidad integrarse a las Estrategias de Marketing Omnicanal.</p>

                    <h3>¿Cuáles son los beneficios de la Digitalización del Punto de Venta?</h3>


                    <p>Distintos estudios han demostrado que el Consumidor de hoy emplea nuevas estrategias de obtención de información dentro del Punto de Venta, muchos de ellos consultan en páginas web y/o en redes sociales sobre los productos (bienes y/o servicios) que desea consumir en un espacio físico. Por esta razón, hoy hablamos del “ROPO” (Research Online Purchase Offline), que quiere decir “Consultar Online y Comprar Offline”, lo que nos brinda la oportunidad de crear un entorno digital en el PdV para el Consumidor y así permitirle sentirse libre en su búsqueda de información; pero siempre con la mirada atenta del propietario de la red, logrando obtener información de nuestro target durante su experiencia de compra en nuestro PdV.</p>

                    <p>Asimismo, crear un entorno digital dentro del PdV, permite una eficiencia en los procesos de compra, por ejemplo: Pantallas Digitales con sistemas de pagos y/o con probadores virtuales, cajas virtuales utilizando tecnologías como Bluetooth, Beacons y/o NFC Tags, APPs de Realidad Aumentada, Códigos QR como asistentes virtuales de venta, sin tener que depender del personal de la misma tienda, esto reforzaría la necesidad de autonomía que requiere el Consumidor Digital.</p>

                    <p>Asimismo, crear un entorno digital dentro del PdV, permite una eficiencia en los procesos de compra, por ejemplo: Pantallas Digitales con sistemas de pagos y/o con probadores virtuales, cajas virtuales utilizando tecnologías como Bluetooth, Beacons y/o NFC Tags, APPs de Realidad Aumentada, Códigos QR como asistentes virtuales de venta, sin tener que depender del personal de la misma tienda, esto reforzaría la necesidad de autonomía que requiere el Consumidor Digital.</p>

                    <div align="center">
                        <img src="{{ URL::asset('assets/img/blog/articulo-1/imagen01.jpg')}}" />
                    </div>

                    <h3>¿Qué rubros serían los más beneficiados?</h3>

                    <p>Definitivamente el Retail (Boutiques, Tiendas por Departamentos, Tiendas de Mejoramiento del Hogar, Supermercados, Tiendas de Conveniencia Etc.), en la Banca (mejorando los espacios de publicidad e información) y Restaurantes son rubros donde la Digitalización del Punto de Venta viene cobrando más adeptos. Pero existe un gran potencial en aplicar estas tecnologías en rubros de servicios y educación, sean Estatales y/o Privadas, logrando interacciones más eficientes y una mejor recolección de data de los usuarios.</p>

                    <aside class="autor">

                        <img src="{{ URL::asset('assets/img/blog/articulo-1/autor-marco-munoz.png')}}">

                        <p class="nom">Marco Muñoz</p>
                        <p>Gerente general ATK</p>
                    </aside>

                </div>
            </section>

        </article>
    </section>
    <aside id="menu-inferior">
        <div class="contenedor">
            <div class="col">
                <h5>Artículos anteriores</h5>
                <ul class="lista-li2">
                    <li><a href="#">This is Photoshop's version  of Lorem Ipsum.</a></li>
                    <li><a href="#">Sagittis sem nibh id elit. Duis sed odio sit amet</a></li>
                    <li><a href="#">Nam nec tellus a odio tincidunt auctor a ornare odio.</a></li>
                </ul>
            </div>

            <div class="col">
                <h5>etiquetas</h5>
                <div class="etiquetas">
                    <a href="#"><span>Digital</span></a> <a href="#"><span>marketing</span></a>
                    <a href="#"><span>ceo</span></a>
                    <a href="#"><span>ceo</span></a>
                    <a href="#"><span>neuromarketing</span></a>
                </div>

            </div>

            <div class="col">
                <h5>categorías</h5>
                <ul class="lista-li2">
                    <li><a href="#">Nuestros Artículos</a></li>
                    <li><a href="#">Marketing</a></li>
                    <li><a href="#">CEO</a></li>
                    <li><a href="#">Beacons</a></li>
                </ul>
            </div>
        </div>
    </aside>
@endsection

@section("cloud_base_js")
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/graingert-wow/1.2.2/wow.min.js"></script>

@endsection




@section("base_js")
    @parent
    <script type="text/javascript" src="{{URL::asset('assets/js/waypoints.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/blog.js')}}" defer></script>
@endsection




