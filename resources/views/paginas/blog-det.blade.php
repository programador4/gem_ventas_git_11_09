@extends('..\layouts\master')


@section('base_css')
    @parent
    <link rel="stylesheet" href="{{ URL::asset('assets/css/blog-det.css')}}" media="screen" title="estilo">

@endsection

@section('base_css')
    @parent
    <link rel="stylesheet" href="{{ URL::asset('assets/css/blog-det.css')}}" media="screen" title="estilo">

@endsection



@section("metattags-social")
    <meta name="description" content="{{$article->description}}">
    <meta name="keywords" content="{{$tag_keys}}">
    <!-- Twitter -->
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@Atomikal">
    <meta name="twitter:title" content="{{$article->title}}">
    <meta name="twitter:description" content="{{$article->description}}">
    <meta name="twitter:image" content="{{asset("../storage/app/articles/".$article->front_image)}}">
    <!-- SEO -->
    <!-- Facebook  -->
    <meta property="og:title" content="{{$article->title}}" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="www.atomikal.com/public/blog/{{$article->slug}}" />
    <meta property="og:image" content="{{asset("../storage/app/articles/".$article->front_image)}}" />
    <meta property="og:site_name" content="Atomikal" />
    <meta property="og:description" content="{{$article->description}}" />
@overwrite






@section('title', 'Blog')



@section('content')
    <section class="pagina">
        <article id="cuerpo">
            <header class="head-tit">
                <div class="contenedor">
                    <a href="index.html">
                        <img src="{{ URL::asset('assets/img/atk-digital-marketing.png')}}" />
                    </a>
                </div>
            </header>
            <section class="cabecera-blog-det">
                <div class="descrip-banner"></div>
            </section>




            <section class="contenido-blog">
                <div class="contenedor">
                    {{--<header class="titulo-general">--}}
                        {{--<h2>--}}
                            {{--{{$article->title}}--}}
                        {{--</h2>--}}
                    {{--</header>--}}

                    {!!$article->content!!}
                    <aside class="autor">

                        <img src="{{ URL::asset('assets/img/blog/articulo-1/autor-marco-munoz.png')}}">

                        <p class="nom">{{$article->user->fullname}}</p>
                        <p>{{$article->user->cargo}}</p>
                    </aside>

                </div>
            </section>

        </article>
    </section>
    <aside id="menu-inferior">
        <div class="contenedor">
            <div class="col">
                <h5>Artículos anteriores</h5>
                <ul class="lista-li2">
                    @foreach($last_articles as $article)
                        <li><a href="{!! url('blog/'.$article->slug)!!}">{{$article->title}}</a></li>
                    @endforeach

                </ul>
            </div>

            <div class="col">
                <h5>etiquetas</h5>
                <div class="etiquetas">
                    @foreach($tags as $tag)
                        <a href="{!! url('blog/tag/'.$tag->slug) !!}"><span>{{$tag->name}}</span></a>
                    @endforeach
                    {{--<a href="#"><span>Digital</span></a>--}}
                    {{--<a href="#"><span>marketing</span></a>--}}
                    {{--<a href="#"><span>ceo</span></a>--}}
                    {{--<a href="#"><span>ceo</span></a>--}}
                    {{--<a href="#"><span>neuromarketing</span></a>--}}
                </div>

            </div>

            <div class="col">
                <h5>categorías</h5>
                <ul class="lista-li2">
                    @foreach($categories as $category)
                        <li><a href="{!! url('blog/category/'.$category->slug) !!}">{{$category->name}}</a></li>
                    @endforeach
                    {{--<li><a href="#">Marketing</a></li>--}}
                    {{--<li><a href="#">CEO</a></li>--}}
                    {{--<li><a href="#">Beacons</a></li>--}}
                </ul>
            </div>
        </div>
    </aside>
@endsection

@section("cloud_base_js")
    @parent
    <script src="https://cdnjs.cloudflare.com/ajax/libs/graingert-wow/1.2.2/wow.min.js"></script>

@endsection




@section("base_js")
    @parent
    <script type="text/javascript" src="{{URL::asset('assets/js/waypoints.min.js')}}"></script>
    <script src="{{URL::asset('assets/js/blog.js')}}" defer></script>
@endsection




