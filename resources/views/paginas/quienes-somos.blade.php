@extends('..\layouts\master')

@section('base_css')
    @parent
    <link rel="stylesheet" href="{{ URL::asset('assets/css/quienes.css')}}" media="screen" title="estilo">
@show

@section('title', 'Quienes somos')



@section('content')
    <section class="pagina">
        <article id="cuerpo">
            <div class="contenedor">
                <header class="head-tit">
                    <a href="index.html">
                        <img src="{{ URL::asset('assets/img/atk-digital-marketing-2.png')}}" />
                    </a>
                    <h2>Atomikal es una agencia de comunicación y marketing</h2>
                    <h3>con 10 años de experiencia en el mercado,</h3>
                    <p>especializada en el proceso estratégico y creativo para el desarrollo de proyectos digitales.</p>
                </header>
                <article class="informacion" align="center">
                    <img src="{{ URL::asset('assets/img/quienes/sobre-atk.jpg')}}" class="img-p" />
                    <div class="box-texto b-text">
                        <p>Generamos experiencias en los usuarios a partir de insights, logrando un verdadero <b>engagement</b> en los canales digitales mediante contenidos con una visión transversal a los medios tradicionales.</p>
                    </div>
                </article>
            </div>
            <section id="formulario">
                <div id="parallax">
                    <div class="layer1" data-depth="0.4">
                        <div id="particles-js"></div>
                    </div>
                    <div class="layer" data-depth="0.3">
                        <div class="some-more-space"></div>
                    </div>
                    <div class="layer2" data-depth="0.4">
                        <div id="particles2-js"></div>
                    </div>
                </div>
                <form action="{{ action('Web\WebController@postContacto') }}" method="post" class="form-foot">
                    <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                    <h3 id="contacto">CONTÁCTATE CON NOSOTROS</h3>
                    @if ($errors->any())
                        <div class="">

                            <!--Checar si la validacion esta fallida o correcta-->

                            <div class="form-label">
                                <label class="w-100">
                                    <div class="error-box">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </label>
                            </div>

                            <!--Checar si la validacion esta fallida o correcta-->

                        </div>
                    @endif
                    @if(session()->has('mensaje'))
                        <br>
                        <div class="form-label">
                            <h2 align="center">{{   Session::get('mensaje')  }}</h2>
                        </div>
                        <br>

                    @else
                    <div class="columnas">

                        <div class="colum-1">
                            <label>
                                <input type="text"  name="name" class="campo" placeholder="Nombre" name="" required>
                                <span class="top-p">Nombre</span>
                            </label>

                            <label>
                                <input type="text" class="campo" name="phone" placeholder="Teléfono de contacto"  required>
                                <span class="top-p">Teléfono de contacto</span>
                            </label>

                            <label>
                                <input type="email"  class="campo" placeholder="E-mail"  name="email" required><span class="top-p">E-mail</span>
                            </label>

                        </div>
                        <div class="colum-2">
                            <label>
                                <textarea rows="6" class="campo textarea" name="question" rows="5" placeholder="Tu consulta" required></textarea>
                                <span class="top-p">Tu consulta</span>
                            </label>
                        </div>
                    </div>
                    @endif
                    <button type="submit" class="btn1 btn2" name="">ENVIAR
                        <span class="sig"> > </span>
                    </button>
                </form>
            </section>
        </article>
        </section>
    @endsection

    @section("cloud_base_js")
        @parent
        <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
        <script src="http://matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>

    @overwrite

    @section("base_js")
        @parent



        <script src="{{URL::asset('assets/js/quienes.js')}}" defer></script>

    @overwrite
                                                                                                                                                                                    