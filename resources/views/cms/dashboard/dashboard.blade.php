@extends('cms\layouts\master')

@section("css")
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/home.css" media="screen" title="estilo">
@endsection

@section('title', 'Dashboard')

@section('content')
    <section class="pagina">
        <main id="cuerpo">
            <article class="superior">
                <h1>Home</h1>
                <div class="acciones">
                    <div class="filtro">
                        <input type="search" class="search" name="buscar" data-sort="name" id="buscar" placeholder="Filtrar">  </div>
                    <a href="#" id="agregar">Agregar</a>
                </div>
            </article>
            <section class="contenedor">
                {{--<ul id="contenido" class="listado list">--}}
                    {{--<!-- item -->--}}
                    {{--<div class="item" data-categoria="a">--}}
                        {{--<figure>--}}
                            {{--<picture>--}}
                                {{--<source srcset="img/img-intranet-ejemplo.jpg" media="(min-width: 600px)">--}}
                                {{--<img src="img/img-intranet-ejemplo.jpg" /> </picture>--}}
                        {{--</figure>--}}
                        {{--<h3> Buenaventura presentó portafolio diversificado para optimizar ... </h3>--}}
                        {{--<div class="categoria"> Titulo categoria </div>--}}
                        {{--<div class="acciones">--}}
                            {{--<div class="elimina">--}}
                                {{--<form class="eliminar" action="" method="post">--}}
                                    {{--<input type="hidden" name="id" value="">--}}
                                    {{--<button type="submit" class="btn eliminar">Eliminar</button>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                            {{--<a class="btn" href="edicion.blade.php">Editar</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- / item -->--}}
                    {{--<!-- item -->--}}
                    {{--<div class="item" data-categoria="a">--}}
                        {{--<figure>--}}
                            {{--<picture>--}}
                                {{--<source srcset="img/img-intranet-ejemplo.jpg" media="(min-width: 600px)">--}}
                                {{--<img src="img/img-intranet-ejemplo.jpg" /> </picture>--}}
                        {{--</figure>--}}
                        {{--<h3> Buenaventura presentó portafolio diversificado para optimizar ... </h3>--}}
                        {{--<div class="categoria"> Titulo categoria </div>--}}
                        {{--<div class="acciones">--}}
                            {{--<div class="elimina">--}}
                                {{--<form class="eliminar" action="" method="post">--}}
                                    {{--<input type="hidden" name="id" value="">--}}
                                    {{--<button type="submit" class="btn eliminar">Eliminar</button>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                            {{--<a class="btn" href="edicion.blade.php">Editar</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- / item -->--}}
                    {{--<!-- item -->--}}
                    {{--<div class="item" data-categoria="a">--}}
                        {{--<figure>--}}
                            {{--<picture>--}}
                                {{--<source srcset="img/img-intranet-ejemplo.jpg" media="(min-width: 600px)">--}}
                                {{--<img src="img/img-intranet-ejemplo.jpg" /> </picture>--}}
                        {{--</figure>--}}
                        {{--<h3> Buenaventura presentó portafolio diversificado para optimizar ... </h3>--}}
                        {{--<div class="categoria"> Titulo categoria </div>--}}
                        {{--<div class="acciones">--}}
                            {{--<div class="elimina">--}}
                                {{--<form class="eliminar" action="" method="post">--}}
                                    {{--<input type="hidden" name="id" value="">--}}
                                    {{--<button type="submit" class="btn eliminar">Eliminar</button>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                            {{--<a class="btn" href="edicion.blade.php">Editar</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- / item -->--}}
                    {{--<!-- item -->--}}
                    {{--<div class="item" data-categoria="a">--}}
                        {{--<figure>--}}
                            {{--<picture>--}}
                                {{--<source srcset="img/img-intranet-ejemplo.jpg" media="(min-width: 600px)">--}}
                                {{--<img src="img/img-intranet-ejemplo.jpg" /> </picture>--}}
                        {{--</figure>--}}
                        {{--<h3> Buenaventura presentó portafolio diversificado para optimizar ... </h3>--}}
                        {{--<div class="categoria"> Titulo categoria </div>--}}
                        {{--<div class="acciones">--}}
                            {{--<div class="elimina">--}}
                                {{--<form class="eliminar" action="" method="post">--}}
                                    {{--<input type="hidden" name="id" value="">--}}
                                    {{--<button type="submit" class="btn eliminar">Eliminar</button>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                            {{--<a class="btn" href="edicion.blade.php">Editar</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- / item -->--}}
                    {{--<!-- item -->--}}
                    {{--<div class="item" data-categoria="a">--}}
                        {{--<figure>--}}
                            {{--<picture>--}}
                                {{--<source srcset="img/img-intranet-ejemplo.jpg" media="(min-width: 600px)">--}}
                                {{--<img src="img/img-intranet-ejemplo.jpg" /> </picture>--}}
                        {{--</figure>--}}
                        {{--<h3> Buenaventura presentó portafolio diversificado para optimizar ... </h3>--}}
                        {{--<div class="categoria"> Titulo categoria </div>--}}
                        {{--<div class="acciones">--}}
                            {{--<div class="elimina">--}}
                                {{--<form class="eliminar" action="" method="post">--}}
                                    {{--<input type="hidden" name="id" value="">--}}
                                    {{--<button type="submit" class="btn eliminar">Eliminar</button>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                            {{--<a class="btn" href="edicion.blade.php">Editar</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<!-- / item -->--}}
                {{--</div>--}}
            </section>
        </main>
    </section>
@endsection

@section("js")
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/picturefill.min.js" async></script>
    <script type="text/javascript" src="js/general.js" async defer></script>
@endsection


