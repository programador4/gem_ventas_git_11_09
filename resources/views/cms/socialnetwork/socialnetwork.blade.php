@extends('cms\layouts\master')

@section("css")
    <link rel="stylesheet" href="{{asset("assets/cms/css/home.css")}}" media="screen" title="estilo">
@endsection

@section('title', 'Servicios')


@section('content')

    <section class="pagina">
        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif


            <article class="superior">

                @if($socialnetwork->id)
                    <h1>Red social : Detalle</h1>
                @else
                    <h1>Creando Red social</h1>
                @endif



            </article>
            <section class="contenedor">


                @if($socialnetwork->id)
                    {!! Form::open(['url' => 'cms/socialnetworks/'.$socialnetwork->id,'method' => 'put', 'files' => true] ) !!}

                @else
                    {!! Form::open(['url' => 'cms/socialnetworks', 'method' => 'post', 'files' => true]) !!}

                @endif


                <div id="contenido">
                    <section class="principal">
                        <div class="atributo#s">
                            <article class="seo">
                                {{ csrf_field() }}
                                <div class="fila">
                                    <span class="tit-1">Nombre de la red social</span>
                                    <input value="{{ ( $socialnetwork->name )? $socialnetwork->name : old('name')  }}" id="name" type="text" placeholder="Red social" name="name" required="">
                                </div>

                                <div class="fila">
                                    <span class="tit-1">Color de la red social</span>
                                    <input value="{{ ( $socialnetwork->color_hex )? $socialnetwork->color_hex : old('color_hex')  }}" id="color_hex" type="text" placeholder="Color asignado a la red social" name="color_hex" required="">
                                </div>


                                <div class="fila">
                                    {{--<span class="tit-1">Maque las casillas</span>--}}
                                    <div class="radios">
                                        <label>

                                            {!! Form::checkbox('is_active', '1', ( $socialnetwork->is_active == 1)? true : false  )!!}
                                            <span>Activo</span>
                                        </label>
                                    </div>
                                </div>






                            </article>
                        </div>


                        


                    </section>
                    <section class="lateral">
                        <article id="medios">
                                <div class="imagen">
                                    <div class="titular">
                                        <h3>Logo de la red social
                                            <span class="medida-sug">| &nbsp; Medida recomendada 30x30 </span>
                                        </h3>
                                    </div>
                                    <div class="upload">
                                        <label for="adjunto-img" class="campo">
                                            <input type="file" id="adjunto-img" name="logo" class="adjunto-img" onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))">
                                            <input value="{{ ( $socialnetwork->logo )? $socialnetwork->logo : old('logo')  }}" id="logo_t" type="hidden" name="logo_t" >
                                            <span>Elegir</span>
                                        </label>
                                    </div>
                                    <figure>
                                        <picture>
                                            <source srcset="{{ ( $socialnetwork->logo )? asset("../storage/app/socialnetworks/".$socialnetwork->logo) : old('logo')  }}"  media="(min-width: 600px)">
                                            <img src="{{ ( $socialnetwork->logo )? asset("../storage/app/socialnetworks/".$socialnetwork->logo) : old('logo')  }}" /> </picture>
                                    </figure>
                                </div>
                                

                            </article>
                    </section>
                </div>
                <div id="acciones">
                    <a class="boton-1 boton-2" href="{!! url('cms/socialnetworks/') !!}">Cancelar</a>
                    <button type="submit" class="boton-1 ">Actualizar</button>
                </div>
                </form>
            </section>
        </main>
    </section>




    {{--<div class="">--}}
    {{--{{ Form::label('color_hex', 'Descripcion de canvas', ['class' => 'form-label']) }}--}}
    {{--<label class="">--}}
    {{--<input value="{{ ( $socialnetwork->color_hex )? $socialnetwork->color_hex : old('color_hex')  }}" type="text" class="" placeholder="Color hexagesimal" name="color_hex" >--}}
    {{--</label>--}}
    {{--</div>--}}



    {{--<div class="">--}}
    {{--{{ Form::label('canvas_front', 'Canvas principal (HTML) ', ['class' => 'form-label']) }}--}}
    {{--<label class="">--}}
    {{--{{Form::textarea('canvas_front',  ( $socialnetwork->canvas_front )? $socialnetwork->canvas_front : old('canvas_front')  )}}--}}
    {{--</label>--}}
    {{--</div>--}}


    {{--<div class="">--}}
    {{--{{ Form::label('activity_description', 'Descripccion del servicio', ['class' => 'form-label']) }}--}}
    {{--<label class="">--}}
    {{--{{Form::textarea('activity_description',  ( $socialnetwork->activity_description )? $socialnetwork->activity_description : old('activity_description')  )}}--}}
    {{--</label>--}}
    {{--</div>--}}



    {{--<div class="">--}}
    {{--{{ Form::label('Es_activo', 'Activo', ['class' => 'form-label']) }}--}}
    {{--<label class="">--}}
    {{--{!! Form::checkbox('is_active', '1', ( $socialnetwork->is_active == 1)? true : false  )!!}--}}
    {{--</label>--}}
    {{--</div>--}}

    {{--<div class="">--}}
    {{--<button type="submit" class="" name="">ENVIAR--}}
    {{--<span class="">></span>--}}
    {{--</button>--}}
    {{--</div>--}}

    {{--<div class="">--}}
    {{--<a   href="{!! url('cms/socialnetworks') !!}" class="" name="">Regresar--}}
    {{--<span class="">></span>--}}
    {{--</a>--}}
    {{--</div>--}}

@endsection

@section("cloud_base_js")
    @parent
    <script src="https://www.google.com/recaptcha/api.js" async></script>
    <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script src="http://matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>

@endsection




@section("js")
    @parent
    <script src="{{URL::asset('assets/cms/js/socialnetwork_cms.js')}}" defer></script>
@endsection





