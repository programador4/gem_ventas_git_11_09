@extends('cms\layouts\master')

@section("css")

@endsection

@section('title', 'Dashboard')

@section('content')
    <div id="alerta">
        <div class="msg-box">
            <h2>¿Estás seguro?</h2>
            <p>Estas apunto de eliminar este registro</p>
            <div class="acciones">
                <button id="cancelar" class="btn eliminar">Cancelar</button>
                <button class="btn confirm">Aceptar</button>
            </div>
        </div>
    </div>


    <section class="pagina">
        <main id="cuerpo">

            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            <article class="superior">
                <h1>Usuarios</h1>
                <div class="acciones">
                    <div class="filtro">
                        <input type="search" class="search" name="buscar" data-sort="name" id="buscar" placeholder="Filtrar"> </div>
                           <a href="{!! url('cms/users/create') !!}" id="agregar">Agregar</a>
                </div>
            </article>
            <section class="contenedor">
                <ul id="contenido" class="listado list">



                  @foreach($users as $key => $user)
                      {{--@if(Auth::id()!=$user->id)--}}
                    <li class="item" data-categoria="a">
                        {{--<figure>--}}
                            {{--<picture>--}}
                                {{--<source srcset="" media="(min-width: 600px)">--}}
                                {{--<img src="{{asset("../storage/app/avatars/".$user->avatar)}}" /> </picture>--}}
                        {{--</figure>--}}
                        <h3 class="name"> {{$user->nombre}} </h3>
                        {{--<div class="categoria">{{$user->correo}} </div>--}}
                        <div class="acciones">
                            <div>

                                <form action='users/{{$user->idusuario}}' method="post" id="frm-{{$user->idusuario}}">
                                    <input type="hidden" name="_method" value="delete" />
                                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                                </form>


                                    <button type="button" class="btn eliminar delete" data-form='frm-{{ $user->idusuario }}'>Eliminar</button>
                                </form>
                            </div>
                            <a class="btn" href="users/{{$user->idusuario}}/edit">Editar</a>
                        </div>
                    </li>
                        {{--@endif--}}
                         @endforeach  
                    <!-- / item -->
                </ul>
            </section>
        </main>
    </section>
@endsection


@section("js")
    //list.min.js
    <script src="{{URL::asset('assets/js/list.min.js')}}" defer></script>
    {{--<script src="{{URL::asset('assets/cms/js/user_index.js')}}" defer></script>--}}
@endsection


