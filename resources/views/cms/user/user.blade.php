@extends('cms\layouts\master')

@section("css")

@endsection

@section('title', 'Dashboard')

@section('content')
    <div id="alerta">
        <div class="msg-box">
            <h2>¿Estás seguro?</h2>
            <p>Estas apunto de eliminar un archivo</p>
            <div class="acciones">
                <button id="cancelar" class="btn eliminar">Cancelar</button>
                <button class="btn">Aceptar</button>
            </div>
        </div>
    </div>
    <section class="pagina">
            <main id="cuerpo">
                @if(session()->has("mensaje"))
                    <div id="msg-confirmation">
                        <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                        <!--<p class="negativo msg">Error para guardar registro</p>-->
                    </div>
                @endif
                @if ($errors->any())
                        <div id="msg-confirmation">
                            <p class="negativo msg">
                                @foreach ($errors->all() as $error)
                                    {{ $error  }}
                                    <br>
                                @endforeach

                            </p>
                            <!--<p class="negativo msg">Error para guardar registro</p>-->
                        </div>
                @endif

                <article class="superior">
                    @if($user->idusuario!==null)
                    <h1>Usuario : Agregar usuario </h1>
                    @else
                    <h1>Usuario : Editar usuario </h1>
                    @endif
                </article>
                <section class="contenedor">
                @if($user->idusuario!==null)
                       <form action='{{url('cms/users/'.$user->idusuario)}}' method="post" >
                           <input type="hidden" name="_method" value="put" />
                @else
                      <form action='{{url("cms/users")}}' method="post" >
                @endif
                          <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

                        <div id="contenido">


                            <section class="principal">
                                <div class="atributos">
                                <div class="atributos">
                                    <article class="seo">


                                        <div class="fila">
                                            <span class="tit-1">Nombre</span>
                                                <input value="{{ ( $user->nombre )? $user->nombre : old('nombre')  }}" id="nombre" type="text" placeholder="Nombre" name="nombre" required="">
                                        </div>

                                        <div class="fila">
                                            <span class="tit-1">Apellido</span>
                                            <input value="{{ ( $user->apellido )? $user->apellido : old('apellido')  }}" type="apellido" class="campo"  placeholder="Apellido" name="apellido" >
                                        </div>
                                       <div class="fila">
                                            <span class="tit-1">Contraseña</span>
                                                <input value="" id="password" type="password" placeholder="Contraseña" name="password" required="">
                                       </div>

                                        <div class="fila">

                                            <span class="tit-1">Correo de usuario</span>
                                                <input value="{{ ( $user->correo )? $user->correo : old('correo')  }}" name="correo" type="text" class="campo" placeholder="Email" >
                                        </div>
                                        <div class="fila">
                                            <span class="tit-1">Tipo de usuario</span>
                                            <select name="tipo_usuario_idtipo_usuario" id="id_user_type">
                                                <option disabled >Seleccione tipo de usuario</option>
                                                @foreach($user_types as $key=>$user_type)
                                                <option value="{{$user_type->idtipo_usuario}}" @if($user->tipo_usuario_idtipo_usuario==$user_type->idtipo_usuario) selected @endif >{{$user_type->nombre}}</option>
                                                @endforeach
                                            </select>

                                        </div>
                                        <div class="fila">

                                            <span class="tit-1">Telefono</span>
                                            <input value="{{ ( $user->telefono )? $user->telefono : old('telefono')  }}" name="telefono" type="text" class="campo" placeholder="Telefono" >
                                        </div>


                                        
                                    </article>
                                </div>

                            </section>

                        </div>
                        <div id="acciones">
                            <a class="boton-1 boton-2" href="{!! url('cms/users/') !!}">Cancelar</a>
                            <button type="submit" class="boton-1 ">Actualizar</button>
                        </div>
                    </form>
                </section>
            </main>
    </section>
@endsection

@section("js")
    @parent
    <script src="{{URL::asset('assets/cms/js/user_index.js')}}" defer></script>
@endsection


