@extends('cms\layouts\master')

@section("css")
    <link rel="stylesheet" href="{{asset("assets/cms/css/home.css")}}" media="screen" title="estilo">
@endsection

@section('title', 'Servicios')


@section('content')

    <section class="pagina">
        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif


            <article class="superior">

                @if($testimony->id)
                    <h1>Testimonio : Detalle</h1>
                @else
                    <h1>Creando Testimonio</h1>
                @endif



            </article>
            <section class="contenedor">


                @if($testimony->id)
                    {!! Form::open(['url' => 'cms/testimonies/'.$testimony->id,'method' => 'put', 'files' => true] ) !!}

                @else
                    {!! Form::open(['url' => 'cms/testimonies', 'method' => 'post', 'files' => true]) !!}

                @endif


                <div id="contenido">
                    <section class="principal">
                        <div class="atributo#s">
                            <article class="seo">
                                {{ csrf_field() }}
                                <div class="fila">
                                    <span class="tit-1">Autor del testimonio</span>
                                    <input value="{{ ( $testimony->author )? $testimony->author : old('author')  }}" id="author" type="text" placeholder="Autor" name="author" required="">
                                </div>
                                <div class="fila">
                                    <span class="tit-1">Cargo del autor del testimonio</span>
                                    <input value="{{ ( $testimony->position )? $testimony->position : old('position')  }}" id="position" type="text" placeholder="Cargo" name="position" required="">
                                </div>
                                <div class="fila">
                                    <span class="tit-1">Seleccionar Cliente</span>
                                    {{Form::select('id_client', $clients, ( $testimony->id_client )? $testimony->id_client : old('id_client') ,['placeholder' => 'Seleccione el cliente']) }}
                                </div>
                                <div class="fila">
                                    <span class="tit-1">Red social</span>
                                    {{Form::select('id_social_network', $social_networks, ( $testimony->id_social_network )? $testimony->id_social_network : old('id_social_network') ,['placeholder' => 'Seleccione la red social']) }}
                                    </div>

                                <div class="fila">
                                    <span class="tit-1">Url del testimonio</span>
                                    <input value="{{ ( $testimony->url_social_network )? $testimony->url_social_network : old('url_social_network')  }}" id="url_social_network" type="text" placeholder="http//:www.web.com" name="url_social_network" required="">
                                </div>
                                

                                <div class="fila">
                                    {{--<span class="tit-1">Maque las casillas</span>--}}
                                    <div class="radios">
                                        <label>

                                            {!! Form::checkbox('is_active', '1', ( $testimony->is_active == 1)? true : false  )!!}
                                            <span>Activo</span>
                                        </label>
                                    </div>
                                </div>






                            </article>
                        </div>


                        <div class="contenidos">
                            <span class="tit-1">Testimonio</span>
                            {{Form::textarea('testimony', ( $testimony->testimony )? $testimony->testimony : old('testimony'))}}

                        </div>
                        
                        
                    </section>
                    <section class="lateral">
                        <article id="medios">
                            
                            

                        </article>
                    </section>
                </div>
                <div id="acciones">
                    <a class="boton-1 boton-2" href="{!! url('cms/projects/') !!}">Cancelar</a>
                    <button type="submit" class="boton-1 ">Actualizar</button>
                </div>
                </form>
            </section>
        </main>
    </section>



@endsection

@section("cloud_base_js")
    @parent
    <script src="https://www.google.com/recaptcha/api.js" async></script>
    <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script src="http://matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>

@endsection

@section("js")
    @parent
    <script src="{{asset("assets/cms/js/testimony_cms.js")}}"></script>
@endsection





