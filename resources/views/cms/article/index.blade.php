
@extends('cms\layouts\master')

{{--@section("css")--}}
{{--<link rel="stylesheet" href="css/general.css">--}}
{{--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}
{{--<link rel="stylesheet" href="css/home.css" media="screen" title="estilo">--}}
{{--@endsection--}}

@section('title', 'Dashboard')



@section('content')
    <div id="alerta">
        <div class="msg-box">
            <h2>¿Estás seguro?</h2>
            <p>Estas apunto de eliminar este registro</p>
            <div class="acciones">
                <button id="cancelar" class="btn eliminar">Cancelar</button>
                <button class="btn confirm">Aceptar</button>
            </div>
        </div>
    </div>



    <section class="pagina">
        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guard    ar registro</p>-->
                </div>
            @endif
            <article class="superior">
                <h1>Home</h1>
                <div class="acciones">
                    <div class="filtro">
                        <input class="search" type="search" name="buscar" id="buscar" placeholder="Filtrar"> </div>
                    <a href="{!! url('cms/articles/create') !!}" id="agregar">Agregar</a>
                </div>
            </article>
            <section class="contenedor">


                <ul id="contenido" class="listado list">


                    @foreach($articles as $key => $article)

                        <li class="item" data-categoria="a">
                            <figure>
                                <picture>
                                    <source srcset="" media="(min-width: 600px)">
                                    <img src="{{asset("../storage/app/articles/".$article->front_image)}}" /> </picture>
                            </figure>
                            <h3 class="name"> {{$article->title}} </h3>
                            {{--                            <div class="categoria">{{$article->name}}</div>--}}
                            <div class="acciones">
                                <div class="elimina">
{{--                                    {!! Form::open(['url' => 'cms/articles/'.$article->id,'method' => 'delete','id' => "frm-".$article->id]) !!}--}}
                                    <button type="button" class="btn eliminar delete" data-form='frm-{{ $article->id }}'>Eliminar</button>
                                    {{--{{ Form::close() }}--}}
                                </div>
                                <a class="btn" href="articles/{{$article->id}}/edit">Editar</a>
                            </div>
                        </li>
                @endforeach
                </ul>
            </section>
@endsection



