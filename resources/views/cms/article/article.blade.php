@extends('cms\layouts\master')

@section("css")
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/typicons/2.0.8/typicons.css">
    <link rel="stylesheet" href="{{asset("assets/cms/css/article_cms.css")}}" media="screen" title="estilo">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.0/multiple-select.min.css">
@endsection

@section('title', 'Articulos')

@section('content')
    <div id="alerta">
        <div class="msg-box">
            <h2>¿Estás seguro?</h2>
            <p>Estas apunto de eliminar este registro</p>
            <div class="acciones">
                <button id="cancelar" class="btn eliminar">Cancelar</button>
                <button class="btn confirm">Aceptar</button>
            </div>
        </div>
    </div>


    <section class="pagina">
        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif

            <article class="superior">

                @if($article->id)
                    <h1>Articulo : Detalle</h1>
                @else
                    <h1>Creando Artículo</h1>
                @endif



            </article>
            <section class="contenedor">


                @if($article->id)
{{--                    {!! Form::open(['url' => 'cms/articles/'.$article->id,'method' => 'put', 'files' => true] ) !!}--}}

                @else
{{--                    {!! Form::open(['url' => 'cms/articles', 'method' => 'post', 'files' => true]) !!}--}}

                @endif


                <div id="contenido">

                    <section class="principal">
                        <div class="atributo#s">
                            <article class="seo">
{{--                                {{ csrf_field() }}--}}
                                <div class="fila">
                                    <span class="tit-1">Nombre del artículo</span>
                                    <input value="{{ ( $article->title )? $article->title : old('title')  }}" id="title" type="text" placeholder="Nombre" name="title" required="">
                                </div>

                                <div class="fila">
                                    {{--<span class="tit-1">Maque las casillas</span>--}}
                                    <div class="radios">
                                        <label>

{{--                                            {!! Form::checkbox('is_active', '1', ( $article->is_active == 1)? true : false  )!!}--}}
                                            <span>Activo</span>
                                        </label>
                                    </div>
                                </div>



                            </article>
                        </div>
                        <div class="contenidos">

                            <span class="tit-1">Contenido</span>
{{--                            {{Form::textarea('content', ( $article->content )? $article->content : old('content'), ['class' => 'content'])}}--}}

                            {{--{{ ( $article->keywords )? $article->keywords : old('keywords')  }}--}}
                        </div>
                        <div class="contenidos">

                            <span class="tit-1">Descripcion breve</span>

{{--                              {{Form::textarea('description', ( $article->description )? $article->description : old('description'), ['class' => 'description'])}}--}}

                            {{--{{ ( $article->keywords )? $article->keywords : old('keywords')  }}--}}
                        </div>




                    {{--<div class="fila">--}}

                            {{--<span class="tit-1">Palabras clave,Porfavor separlas en comas</span>--}}
                            {{--<input value="{{ ( $article->keywords )? $article->keywords : old('keywords')  }}" id="keywords" type="text" placeholder="Palabras clave" name="keywords" required="">--}}

                            {{--{{ ( $article->keywords )? $article->keywords : old('keywords')  }}--}}
                        {{--</div>--}}

                    </section>
                    <section class="lateral">
                    <section class="lateral">
                        <article id="medios">
                            <div class="imagen">
                                <div class="titular">
                                    <h3>Imegen frontal
                                        <span class="medida-sug">| &nbsp; Medida recomendada 653x363 </span>
                                    </h3>
                                </div>
                                <div class="upload">
                                    <label for="adjunto-img" class="campo">
                                        <input type="file" id="adjunto-img" name="front_image" class="adjunto-img" onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))">
                                        <input type="hidden" id="front_image_t" name="front_image_t"  value="{{ ( $article->front_image )? $article->front_image: old('front_image')  }}">
                                        <span>Elegir</span>
                                    </label>
                                </div>
                                <figure>
                                    <picture>
                                        <source srcset="{{ ( $article->front_image )? asset("../storage/app/articles/".$article->front_image) : old('front_image')  }}"  media="(min-width: 600px)">
                                        <img src="{{ ( $article->front_image )? asset("../storage/app/articles/".$article->front_image) : old('front_image')  }}" /> </picture>
                                </figure>
                            </div>


                        </article>
                    </section>
                </div>
                <div id="acciones">
                    <a class="boton-1 boton-2" href="{!! url('cms/articles/') !!}">Cancelar</a>
                    <button type="submit" class="boton-1 ">Actualizar</button>
                </div>
                {{--</form>--}}
            </section>
        </main>
    </section>


@endsection

@section("js")
    @parent

    <script src="{{asset("assets/cms/js/article_cms.js")}}"></script>
@endsection
