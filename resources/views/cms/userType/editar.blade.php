@extends('..\layouts\master')

@section('base_css')
    @parent
    <link rel="stylesheet" href="{{ URL::asset('assets/css/contacto.css')}}" media="screen" title="estilo">

@endsection

@section("header_scripts")

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>


@endsection


@section('initialize', " onload=initialize() ")


@section('title', 'Dashboard')



@section('content')
    <section class="pagina">
        <article id="cuerpo">
            <div id="parallax">
                <div class="layer1" data-depth="0.4">
                    <div id="particles-js"></div>
                </div>
                <div class="layer" data-depth="0.3">
                    <div class="some-more-space"></div>
                </div>
                <div class="layer2" data-depth="0.4">
                    <div id="particles2-js"></div>
                </div>
            </div>
            <div class="contenedor">
                <header class="head-tit">
                    <a href="index.html">
                        <img src="{{URL::asset('assets/img/atk-digital-marketing-2.png')}}" />
                    </a>
                    @if($usertype->id)
                        <h2>Editando tipo de usuario</h2>
                        <h3>Editando tipo de usuario</h3>
                    @else
                        <h2>Creando tipo de usuario</h2>
                        <h3>Creando tipo de usuario</h3>

                    @endif

                </header>
                <h3 class="contactate">DASHBOARD DE ATOMAKA</h3>
                @if($usertype->id)
                    {!! Form::open(['url' => 'cms/usertypes/'.$usertype->id,'method' => 'put']) !!}
                    {{--{{ method_field('PUT') }}--}}
                @else
                    {!! Form::open(['url' => 'cms/usertypes', 'method' => 'post']) !!}

                @endif
                    <article class="metodos-contacto">
                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                        <div class="formulario">
                            @if ($errors->any())
                                <div class="">

                                    <!--Checar si la validacion esta fallida o correcta-->

                                    <div class="form-label">
                                        <label class="w-100">
                                            <div class="error-box">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            @endif



                        <!--Checar si existe la sesion rapida-->

                            @if(session()->has('mensaje'))

                                <div class="form-label">
                                    <h2 align="center">{{   Session::get('mensaje')  }}</h2>
                                </div>
                                {{--</div>--}}
                            @endif



                        <!--Checar si existe la sesion rapida-->


                            <div class="form-label">
                                    {{ Form::label('usertype', 'Tipo de usuario', ['class' => 'form-label']) }}
                                <label class="w-100">
                                    <input value="{{ ( $usertype->type )? $usertype->type : old('type')  }}" type="text" class="campo"  name="type" >

                                </label>
                            </div>



                            <div class="form-label">
                                {{ Form::label('is_active', 'Es activo', ['class' => 'form-label']) }}
                                <label class="w-100">
                                   {!! Form::checkbox('is_active', '1', ( $usertype->is_active == 1)? true : false  )!!}
                                </label>
                            </div>

                            <div class="form-label dos-col">
                                <button type="submit" class="enviar btn1 btn2" name="">ENVIAR
                                    <span class="sig">></span>
                                </button>
                            </div>

                            <div class="form-label dos-col">
                                <a   href="{!! url('cms/usertypes') !!}" class="enviar btn1 btn2" name="">Regresar
                                    <span class="sig">></span>
                                </a>
                            </div>



        </article>
                {{ Form::close() }}
        </div>
        </article>
    </section>
@endsection

@section("cloud_base_js")
    @parent
    <script src="https://www.google.com/recaptcha/api.js" async></script>
    <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script src="http://matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>

@endsection




@section("base_js")
    @parent
    <script src="{{URL::asset('assets/js/contacto.js')}}" defer></script>
@endsection





