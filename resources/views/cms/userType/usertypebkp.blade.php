@extends('cms\layouts\master')

@section("css")
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/home.css" media="screen" title="estilo">
@endsection

@section('title', 'Dashboard')

@section('content')
    <section class="pagina">
            <main id="cuerpo">
                <article class="superior">
                    <h1>Interna : Detalle</h1>
                </article>
                <section class="contenedor">
                @if($usertype->id)
                    {!! Form::open(['url' => 'cms/usertypes/'.$usertype->id,'method' => 'put', 'files' => true] ) !!}

                @else
                    {!! Form::open(['url' => 'cms/usertypes', 'method' => 'post', 'files' => true]) !!}

                @endif
                        <input type="hidden" name=" " value="">
                        <input type="hidden" name=" " value="">
                        <div id="contenido">
                             @if ($errors->any())
                                <div class="">
                                    <div class="form-label">
                                        <label class="w-100">
                                            <div class="error-box">
                                                <ul>
                                                    @foreach ($errors->all() as $error)
                                                        <li>{{ $error }}</li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                            @endif
                            <section class="principal">
                                <div class="atributo#s">
                                    <article class="seo">

                                <div class="fila">
                                            <span class="tit-1">Contraseña</span>
                                                                        <input value="{{ old('password')  }}" type="password" class="campo"  placeholder="Contraseña" name="password" >
                                                                         </div>
                                        <div class="fila">
                                           {{ csrf_field() }}
                                            <span class="tit-1">Nombre Real</span>
                                                <input value="{{ ( $usertype->fullname )? $usertype->fullname : old('fullname')  }}" id="fullname" type="text" placeholder="Usuario" name="fullname" required="">
                                        </div>                                        <div class="fila">
                                           {{ csrf_field() }}
                                            <span class="tit-1">Cargo </span>
                                                <input value="{{ ( $usertype->cargo )? $usertype->cargo : old('cargo')  }}" id="cargo" type="text" placeholder="Cargo" name="cargo" required="">
                                        </div>
                                       <div class="fila">
                                           {{ csrf_field() }}
                                            <span class="tit-1">Nombre de usuario</span>
                                                <input value="{{ ( $usertype->usertypename )? $usertype->usertypename : old('usertypename')  }}" id="usertypename" type="text" placeholder="Usuario" name="usertypename" required=""></div>
                                        <div class="fila">

                                        <div class="fila">
                                           {{ csrf_field() }}
                                            <span class="tit-1">Correo de usuario</span>
                                                <input value="{{ ( $usertype->email )? $usertype->email : old('email')  }}" name="email" type="text" class="campo" placeholder="Email" >
                                            <div class="fila">
                                        

                                            <span class="tit-1">Tipo de usuario</span>
                                    {{Form::select('id_usertype_type', $usertype_types, ( $usertype->id_usertype_type )? $usertype->id_usertype_type : old('id_usertype_type') ,['placeholder' => 'Seleccione el tipo de usuario']) }}
                                        </div>
                                        
                                    </article>
                                </div>
                                {{--<div class="contenidos">--}}
                                    {{--<span class="tit-1">Contenido</span>--}}
                                    {{--<input name="contenido" type="hidden" value="contenido ">--}}
                                    {{--<div class="editor"> </div>--}}
                                {{--</div>--}}
                            </section>
                            <section class="lateral">
                                <article id="medios">
                                    <div class="imagen">
                                        <div class="titular">
                                            <h3>Imagen
                                                <span class="medida-sug">| &nbsp; Medida recomendada 900x564 </span>
                                            </h3>
                                        </div>
                                        <div class="upload">
                                            <label for="adjunto-img" class="campo">
                                                <input type="file" id="adjunto-img" name="avatar" class="adjunto-img" onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))">
                                                <span>Elegir</span>
                                            </label>
                                        </div>
                                        <figure>
                                            <picture>
                                                <source srcset="{{ ( $usertype->avatar )? asset("../storage/app/avatars/".$usertype->avatar) : old('avatar')  }}"" media="(min-width: 600px)">
                                                <img src="{{ ( $usertype->avatar )? asset("../storage/app/avatars/".$usertype->avatar) : old('avatar')  }}" /> </picture>
                                        </figure>
                                    </div>
                                    {{--<div class="video">--}}
                                        {{--<div class="titular">--}}
                                            {{--<h3>Video de ...</h3>--}}
                                        {{--</div>--}}
                                        {{--<div class="upload">--}}
                                            {{--<label for="adjunto-img" class="campo">--}}
                                                {{--<input type="file" id="adjunto-img" name="adjunto-img" class="adjunto-img" onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))">--}}
                                                {{--<span>Elegir</span>--}}
                                            {{--</label>--}}
                                        {{--</div>--}}
                                        {{--<figure>--}}
                                            {{--<picture>--}}
                                                {{--<source srcset="img/img-intranet-ejemplo3.jpg" media="(min-width: 600px)">--}}
                                                {{--<img class="imagen" src="img/img-intranet-ejemplo3.jpg" alt=" "> </picture>--}}
                                        {{--</figure>--}}
                                    {{--</div>--}}
                                </article>
                            </section>
                        </div>
                        <div id="acciones">
                            <a class="boton-1 boton-2" href="javascript:history.back();">Cancelar</a>
                            <button type="submit" class="boton-1 ">Actualizar</button>
                        </div>
                    </form>
                </section>
            </main>
        </section>
@endsection

@section("js")
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/picturefill.min.js" async></script>
    <script type="text/javascript" src="js/general.js" async defer></script>
@endsection


