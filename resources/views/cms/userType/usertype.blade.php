@extends('cms\layouts\master')

@section("css")
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/home.css" media="screen" title="estilo">
@endsection

@section('title', 'Dashboard')

@section('content')
    <section class="pagina">
        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            <article class="superior">
                <h1>Interna : Detalle</h1>
            </article>
            <section class="contenedor">

                @if($usertype->id)
                    {!! Form::open(['url' => 'cms/usertypes/'.$usertype->id,'method' => 'put', 'files' => true] ) !!}

                @else
                    {!! Form::open(['url' => 'cms/usertypes', 'method' => 'post', 'files' => true]) !!}

                @endif
                <input type="hidden" name=" " value="">
                <input type="hidden" name=" " value="">
                <div id="contenido">


                    <section class="principal">
                        <div class="atributo#s">
                            <article class="seo">


                                <div class="fila">
                                    {{ csrf_field() }}
                                    <span class="tit-1">Tipo de usuario</span>
                                    <input value="{{ ( $usertype->type )? $usertype->type : old('type')  }}" id="type" type="text" placeholder="Tipo de usuario" name="type" required="">
                                </div>



                                <div class="fila">
                                    {{--<span class="tit-1">Maque las casillas</span>--}}
                                    <div class="radios">
                                        <label>

                                            {!! Form::checkbox('is_active', '1', ( $usertype->is_active == 1)? true : false  )!!}
                                            <span>Activo</span>
                                        </label>
                                    </div>
                                </div>

                            </article>
                        </div>
                        {{--<div class="contenidos">--}}
                        {{--<span class="tit-1">Contenido</span>--}}
                        {{--<input name="contenido" type="hidden" value="contenido ">--}}
                        {{--<div class="editor"> </div>--}}
                        {{--</div>--}}
                    </section>
                    <section class="lateral">
                        <article id="medios">
                            {{--<div class="video">--}}
                            {{--<div class="titular">--}}
                            {{--<h3>Video de ...</h3>--}}
                            {{--</div>--}}
                            {{--<div class="upload">--}}
                            {{--<label for="adjunto-img" class="campo">--}}
                            {{--<input type="file" id="adjunto-img" name="adjunto-img" class="adjunto-img" onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))">--}}
                            {{--<span>Elegir</span>--}}
                            {{--</label>--}}
                            {{--</div>--}}
                            {{--<figure>--}}
                            {{--<picture>--}}
                            {{--<source srcset="img/img-intranet-ejemplo3.jpg" media="(min-width: 600px)">--}}
                            {{--<img class="imagen" src="img/img-intranet-ejemplo3.jpg" alt=" "> </picture>--}}
                            {{--</figure>--}}
                            {{--</div>--}}
                        </article>
                    </section>
                </div>
                <div id="acciones">
                    <a class="boton-1 boton-2" href="{!! url('cms/usertypes/') !!}">Cancelar</a>
                    <button type="submit" class="boton-1 ">Actualizar</button>
                </div>
                </form>
            </section>
        </main>
    </section>
@endsection

@section("js")
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/picturefill.min.js" async></script>
    <script type="text/javascript" src="js/general.js" async defer></script>
@endsection


