
@extends('cms\layouts\master')

{{--@section("css")--}}
{{--<link rel="stylesheet" href="css/general.css">--}}
{{--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}
{{--<link rel="stylesheet" href="css/home.css" media="screen" title="estilo">--}}
{{--@endsection--}}

@section('title', 'Dashboard')



@section('content')
    <div id="alerta">
        <div class="msg-box">
            <h2>¿Estás seguro?</h2>
            <p>Estas apunto de eliminar este registro</p>
            <div class="acciones">
                <button id="cancelar" class="btn eliminar">Cancelar</button>
                <button class="btn confirm">Aceptar</button>
            </div>
        </div>
    </div>



    <section class="pagina">
        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guard    ar registro</p>-->
                </div>
            @endif
            <article class="superior">
                <h1>Home</h1>
                <div class="acciones">
                    <div class="filtro">
                        <input type="search" class="search" name="buscar" data-sort="name" id="buscar" placeholder="Filtrar">  </div>
                    <a href="{!! url('cms/categories/create') !!}" id="agregar">Agregar</a>
                </div>
            </article>
            <section class="contenedor">


                <ul id="contenido" class="listado list">


                    @foreach($categories as $key => $category)

                        <li class="item" data-categoria="a">

                            <h3 class="name"> {{$category->name}} </h3>
                            {{--                            <div class="categoria">{{$category->name}}</div>--}}
                            <div class="acciones">
                                <div class="elimina">
                                    {!! Form::open(['url' => 'cms/categories/'.$category->id,'method' => 'delete','id' => "frm-".$category->id]) !!}
                                    <button type="button" class="btn eliminar delete" data-form='frm-{{ $category->id }}'>Eliminar</button>
                                    {{ Form::close() }}
                                </div>
                                <a class="btn" href="categories/{{$category->id}}/edit">Editar</a>
                            </div>
                        </li>
                @endforeach
                </ul>
            </section>
@endsection


