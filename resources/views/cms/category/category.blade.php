@extends('cms\layouts\master')

@section("css")
    <link rel="stylesheet" href="{{asset("assets/cms/css/home.css")}}" media="screen" title="estilo">
@endsection

@section('title', 'articleos')

@section('content')
    <div id="alerta">
        <div class="msg-box">
            <h2>¿Estás seguro?</h2>
            <p>Estas apunto de eliminar este registro</p>
            <div class="acciones">
                <button id="cancelar" class="btn eliminar">Cancelar</button>
                <button class="btn confirm">Aceptar</button>
            </div>
        </div>
    </div>


    <section class="pagina">
        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif

            <article class="superior">

                @if($category->id)
                    <h1>Articulo : Detalle</h1>
                @else
                    <h1>Creando Artículo</h1>
                @endif



            </article>
            <section class="contenedor">


                @if($category->id)
                    {!! Form::open(['url' => 'cms/categories/'.$category->id,'method' => 'put', 'files' => true] ) !!}

                @else
                    {!! Form::open(['url' => 'cms/categories', 'method' => 'post', 'files' => true]) !!}

                @endif


                <div id="contenido">

                    <section class="principal">
                        <div class="atributo#s">
                            <article class="seo">
                                {{ csrf_field() }}
                                <div class="fila">
                                    <span class="tit-1">Nombre</span>
                                    <input value="{{ ( $category->name )? $category->name : old('name')  }}" id="name"  type="text" placeholder="Categoria" name="name" required="">
                                </div>

                                <div class="fila">
                                    {{--<span class="tit-1">Maque las casillas</span>--}}
                                    <div class="radios">
                                        <label>

                                            {!! Form::checkbox('is_active', '1', ( $category->is_active == 1)? true : false  )!!}
                                            <span>Activo</span>
                                        </label>
                                    </div>
                                </div>

                            </article>
                        </div>
                        
                    
                    </section>
                    <section class="lateral">
                        <article id="medios">
                            
                            

                        </article>
                    </section>
                </div>
                <div id="acciones">
                    <a class="boton-1 boton-2" href="{!! url('cms/categories/') !!}">Cancelar</a>
                    <button type="submit" class="boton-1 ">Actualizar</button>
                </div>
                </form>
            </section>
        </main>
    </section>


@endsection

@section("js")
    <script src="//cdn.quilljs.com/1.2.2/quill.js"></script>
@endsection




