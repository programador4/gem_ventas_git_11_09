@extends("cms/layouts/master")

@section('base_css')
	@parent
	<link rel="stylesheet" href="{{ URL::asset('backend_assets/css/edicion.css')}}" media="screen" title="estilo">

@endsection

@section("header_scripts")
	<link href="//cdn.quilljs.com/1.2.2/quill.snow.css" rel="stylesheet">
	<link href="//cdn.quilljs.com/1.2.2/quill.bubble.css" rel="stylesheet">
@endsection


@section('content')

<section class="pagina">
			<main id="cuerpo">
				<article class="superior">
					<h1>Interna : Detalle</h1>
				</article>
				<section class="contenedor">
					<form action="#" method="post">
						<input type="hidden" name=" " value="">
						<input type="hidden" name=" " value="">
						<div id="contenido">
							<section class="principal">
								<div class="atributo#s">
									<article class="seo">
										<div class="fila">
											<span class="tit-1">Título</span>
											<input type="text" name=" " value=""> </div>
										<div class="fila">
											<span class="tit-1">Categoria</span>
											<select>
												<option>Seleccione una categoria</option>
												<option value="1">Categoria 1</option>
												<option value="1">Categoria 2</option>
												<option value="1">Categoria 3</option>
												<option value="1">Categoria 4</option>
												<option value="1">Categoria 5</option>
											</select>
										</div>
										<div class="fila">
											<span class="tit-1">Dirección url</span>
											<input type="text" name=" " value=" "> </div>
									</article>
								</div>
								<div class="contenidos">
									<span class="tit-1">Contenido</span>
									<input name="contenido" type="hidden" value="contenido ">
									<div class="editor"> </div>
								</div>
							</section>
							<section class="lateral">
								<article id="medios">
									<div class="imagen">
										<div class="titular">
											<h3>Imagen
												<span class="medida-sug">| &nbsp; Medida recomendada 900x564 </span>
											</h3>
										</div>
										<div class="upload">
											<label for="adjunto-img" class="campo">
												<input type="file" id="adjunto-img" name="adjunto-img" class="adjunto-img" onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))">
												<span>Elegir</span>
											</label>
										</div>
										<figure>
											<picture>
												<source srcset="img/img-intranet-ejemplo2.jpg" media="(min-width: 600px)">
												<img src="img/img-intranet-ejemplo2.jpg" /> </picture>
										</figure>
									</div>
									<div class="video">
										<div class="titular">
											<h3>Video de ...</h3>
										</div>
										<div class="upload">
											<label for="adjunto-img" class="campo">
												<input type="file" id="adjunto-img" name="adjunto-img" class="adjunto-img" onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))">
												<span>Elegir</span>
											</label>
										</div>
										<figure>
											<picture>
												<source srcset="img/img-intranet-ejemplo3.jpg" media="(min-width: 600px)">
												<img class="imagen" src="img/img-intranet-ejemplo3.jpg" alt=" "> </picture>
										</figure>
									</div>
								</article>
							</section>
						</div>
						<div id="acciones">
							<a class="boton-1 boton-2" href="javascript:history.back();">Cancelar</a>
							<button type="submit" class="boton-1 ">Actualizar</button>
						</div>
					</form>
				</section>
			</main>
		</section>

@endsection

@section("cloud_base_js")
	@parent
	<script src="https://www.google.com/recaptcha/api.js" async></script>
	<script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
	<script src="http://matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>

@endsection




@section("base_js")
	@parent
	<script src="{{URL::asset('assets/js/contacto.js')}}" defer></script>
@endsection





