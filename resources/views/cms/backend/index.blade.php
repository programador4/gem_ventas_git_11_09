@extends('../layouts/master')


<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Unilever Extranet</title>
    <meta charset="utf8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/general.css"/>
  </head>
  <body>
    <div id="pagina">
      <section id="contenido">
        <div class="contenedor"></div>
      </section>
      <footer id="pie">
        <div class="contenedor"></div>
      </footer>
    </div>
            <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js" defer></script>
            <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/URI.js/1.18.2/URI.min.js" defer></script>
            <script type="text/javascript" src="js/general.js" defer></script>
  </body>
</html>