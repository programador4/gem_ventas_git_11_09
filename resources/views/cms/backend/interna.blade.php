@extends("cms/layouts/master")

@section('base_css')
	@parent
	<link rel="stylesheet" href="{{ URL::asset('backend_assets/css/home.css')}}" media="screen" title="estilo">

@endsection

@section("header_scripts")
	<link href="//cdn.quilljs.com/1.2.2/quill.snow.css" rel="stylesheet">
	<link href="//cdn.quilljs.com/1.2.2/quill.bubble.css" rel="stylesheet">
@endsection


@section('content')
<section class="pagina">
			<main id="cuerpo">
				<article class="superior">
					<h1>Home</h1>
					<div class="acciones">
						<div class="filtro">
							<input type="search" class="search" name="buscar" data-sort="name" id="buscar" placeholder="Filtrar">  </div>
						<a href="#" id="agregar">Agregar</a>
					</div>
				</article>
				<section class="contenedor">
					<ul id="contenido" class="listado list">
						<!-- item -->
						<div class="item" data-categoria="a">
							<figure>
								<picture>
									<source srcset="img/img-intranet-ejemplo.jpg" media="(min-width: 600px)">
									<img src="img/img-intranet-ejemplo.jpg" /> </picture>
							</figure>
							<h3> Buenaventura presentó portafolio diversificado para optimizar ... </h3>
							<div class="categoria"> Titulo categoria </div>
							<div class="acciones">
								<div class="elimina">
									<form class="eliminar" action="" method="post">
										<input type="hidden" name="id" value="">
										<button type="submit" class="btn eliminar">Eliminar</button>
									</form>
								</div>
								<a class="btn" href="edicion.blade.php">Editar</a>
							</div>
						</div>
						<!-- / item -->
						<!-- item -->
						<div class="item" data-categoria="a">
							<figure>
								<picture>
									<source srcset="img/img-intranet-ejemplo.jpg" media="(min-width: 600px)">
									<img src="img/img-intranet-ejemplo.jpg" /> </picture>
							</figure>
							<h3> Buenaventura presentó portafolio diversificado para optimizar ... </h3>
							<div class="categoria"> Titulo categoria </div>
							<div class="acciones">
								<div class="elimina">
									<form class="eliminar" action="" method="post">
										<input type="hidden" name="id" value="">
										<button type="submit" class="btn eliminar">Eliminar</button>
									</form>
								</div>
								<a class="btn" href="edicion.blade.php">Editar</a>
							</div>
						</div>
						<!-- / item -->
						<!-- item -->
						<div class="item" data-categoria="a">
							<figure>
								<picture>
									<source srcset="img/img-intranet-ejemplo.jpg" media="(min-width: 600px)">
									<img src="img/img-intranet-ejemplo.jpg" /> </picture>
							</figure>
							<h3> Buenaventura presentó portafolio diversificado para optimizar ... </h3>
							<div class="categoria"> Titulo categoria </div>
							<div class="acciones">
								<div class="elimina">
									<form class="eliminar" action="" method="post">
										<input type="hidden" name="id" value="">
										<button type="submit" class="btn eliminar">Eliminar</button>
									</form>
								</div>
								<a class="btn" href="edicion.blade.php">Editar</a>
							</div>
						</div>
						<!-- / item -->
						<!-- item -->
						<div class="item" data-categoria="a">
							<figure>
								<picture>
									<source srcset="img/img-intranet-ejemplo.jpg" media="(min-width: 600px)">
									<img src="img/img-intranet-ejemplo.jpg" /> </picture>
							</figure>
							<h3> Buenaventura presentó portafolio diversificado para optimizar ... </h3>
							<div class="categoria"> Titulo categoria </div>
							<div class="acciones">
								<div class="elimina">
									<form class="eliminar" action="" method="post">
										<input type="hidden" name="id" value="">
										<button type="submit" class="btn eliminar">Eliminar</button>
									</form>
								</div>
								<a class="btn" href="edicion.blade.php">Editar</a>
							</div>
						</div>
						<!-- / item -->
						<!-- item -->
						<div class="item" data-categoria="a">
							<figure>
								<picture>
									<source srcset="img/img-intranet-ejemplo.jpg" media="(min-width: 600px)">
									<img src="img/img-intranet-ejemplo.jpg" /> </picture>
							</figure>
							<h3> Buenaventura presentó portafolio diversificado para optimizar ... </h3>
							<div class="categoria"> Titulo categoria </div>
							<div class="acciones">
								<div class="elimina">
									<form class="eliminar" action="" method="post">
										<input type="hidden" name="id" value="">
										<button type="submit" class="btn eliminar">Eliminar</button>
									</form>
								</div>
								<a class="btn" href="edicion.blade.php">Editar</a>
							</div>
						</div>
						<!-- / item -->
					</div>
				</section>
			</main>
		</section>

@endsection

@section("cloud_base_js")
	@parent
	<script src="https://www.google.com/recaptcha/api.js" async></script>
	<script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
	<script src="http://matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>

@endsection




@section("base_js")
	@parent
	<script src="{{URL::asset('assets/js/contacto.js')}}" defer></script>
@endsection





