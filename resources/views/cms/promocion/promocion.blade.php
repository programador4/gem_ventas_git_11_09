@extends('cms\layouts\master')

@section("css")
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="{{asset("assets/cms/css/home.css")}}" media="screen" title="estilo">
    <link rel="stylesheet" href="{{asset("assets/cms/css/proyect.css")}}" media="screen" title="estilo">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


@endsection

@section('title', 'Promocion')

@section('content')
    {{--<div id="alerta">--}}
    {{--<div class="msg-box">--}}
    {{--<h2>¿Estás seguro?</h2>--}}
    {{--<p>Estas apunto de eliminar este registro</p>--}}
    {{--<div class="acciones">--}}
    {{--<button id="cancelar" class="btn eliminar">Cancelar</button>--}}
    {{--<button class="btn confirm">Aceptar</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}


    <section class="pagina">
        <main id="cuerpo">
            @if($promociones->idpromocion)
                <h1>Editar Promoción</h1>
            @else
                <h1>Nuevo Promoción</h1>
            @endif
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif


            <div class="container">
                {{--<form class="form" method="post" action='{{url("cms/visitas")}}' >--}}
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="wizard">
                        
                        <div class="tab-content">
                            <div class="tab-pane active text-center" role="tabpanel" id="step1">
                                {{--1) Del Contacto--}}
                                
                                <div class="row promocion_form" >
                                    <form action="" id="promocion_form_container" style="width: 100%;">
                                        <div class="row">
                                            <div class="col-md-2">
                                                <div class="fila">
                                                    <span class="tit-1">Nombre</span>
                                                    <input class="form-group small" onkeypress="return soloNumeros(event)"  type="text" name="nombre" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="fila">
                                                    <span class="tit-1">Factor de descuento</span>
                                                    <input class="form-group small" onkeypress="return soloNumeros(event)"  type="text" name="descuento" value=""> %
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="fila">
                                                    <span class="tit-1">Fecha de inicio</span>
                                                    <input class="form-group small"  type="date" name="fecha_inicio" value=""> </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="fila">
                                                    <span class="tit-1">Fecha de fin</span>
                                                    <input class="form-group small" type="date" name="fecha_fin" value=""> </div>
                                            </div>
                                        </div>                                            
                                    </form>
                                </div>
                                <ul class="list-inline text-md-center">
                                    <li><span class="btn btn-lg btn-common promocion_button ">Grabar</span></li>
                                </ul>
                            </div>                            
                        </div>

                    </div>
                {{--</form>--}}
            </div>

            <article class="superior">
                <div class="row">

                </div>

            </article>

        </main>
    </section>

@endsection

@section("js")

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script >
        var site_url="{{url("cms")}}";
        var id_usuario="{{Auth::user()->idusuario}}";
        var name_usuario="{{Auth::user()->nombre}}";
    </script>>
    <script src="{{asset("assets/cms/js/validate.js")}}"></script>>
    <script src="{{asset("assets/cms/js/promocion_cms_editar.js")}}"></script>>    
@endsection

