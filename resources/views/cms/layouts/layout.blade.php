    <head>
        <title>Cms Atomikal - @yield('title')</title>
        <meta charset="UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0" />

        <link rel="icon" href="img/icono.png" type="image/png">
        <link rel="shortcut icon" href="img/icono.png" />

        <!-- Metatags -->
        @section("metattags")
        @show


        <link rel="stylesheet" href="{{ URL::asset('assets/cms/css/general.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('assets/cms/css/home.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="//cdn.quilljs.com/1.2.2/quill.snow.css" rel="stylesheet">
        <link href="//cdn.quilljs.com/1.2.2/quill.bubble.css" rel="stylesheet">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        @section("css")
        @show
        <!-- Js -->
        <script type="text/javascript">
            console.log("LAG");
            document.createElement("picture");
        </script>

    </head>

    <body>

        @include('cms/include/header')

        @section('content')

        @show
        {{--<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>--}}
        <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js"></script>
        <script type="text/javascript" src="//cloud.tinymce.com/stable/tinymce.min.js?apiKey=ymrc087e8k8ctmsz2231a0c894r2q7tevexvyqupda3a51pw"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.0/multiple-select.min.js"></script>
        <script type="text/javascript" src="{{asset("assets/cms/js/picturefill.min.js") }}"></script>
        <script type="text/javascript" src="{{URL::asset('assets/js/list.min.js')}}" ></script>
        <script type="text/javascript" src="{{asset("assets/cms/js/general.js")}}"></script>
        @section("js")

        @show
    <body>