@extends('cms\layouts\master')

@section("css")

@endsection

@section('title', 'Dashboard')



@section('content')
    <div id="alerta">
        <div class="msg-box">
            <h2>¿Estás seguro?</h2>
            <p>Estas apunto de eliminar este registro</p>
            <div class="acciones">
                <button id="cancelar" class="btn eliminar">Cancelar</button>
                <button class="btn confirm">Aceptar</button>
            </div>
        </div>
    </div>


    <section class="pagina">
        <main id="cuerpo">

            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            <article class="superior">
                <h1>Home</h1>
                <div class="acciones">
                    <div class="filtro">
                        <input type="search" class="search" name="buscar" data-sort="name" id="buscar" placeholder="Filtrar">  </div>
                    <a href="{!! url('cms/clients/create') !!}" id="agregar">Agregar</a>
                </div>
            </article>
            <section class="contenedor">
                <ul id="contenido" class="listado list">


                @foreach($clients as $key => $client)

                        <li class="item" data-categoria="a">
                            <figure>
                                <picture>
                                    <source srcset="" media="(min-width: 600px)">
                                    <img src="{{asset("../storage/app/clients/".$client->logo)}}" /> </picture>
                            </figure>
                            <h3 class="name"> {{$client->name}} </h3>
                            {{--<div class="categoria">{{$client->name}}</div>--}}
                            <div class="acciones">
                                <div class="elimina">
                                    {!! Form::open(['url' => 'cms/clients/'.$client->id,'method' => 'delete','id' => "frm-".$client->id]) !!}
                                    <button type="button" class="btn eliminar delete" data-form='frm-{{ $client->id }}'>Eliminar</button>
                                    {{ Form::close() }}
                                </div>
                                <a class="btn" href="clients/{{$client->id}}/edit">Editar</a>
                            </div>
                        </li>
                @endforeach
                </ul>
            </section>
@endsection




