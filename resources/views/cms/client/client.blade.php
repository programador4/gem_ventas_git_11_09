@extends('cms\layouts\master')

@section("css")
   <link rel="stylesheet" href="{{asset("assets/cms/css/home.css")}}" media="screen" title="estilo">
@endsection

@section('title', 'Dashboard')

@section('content')
    <section class="pagina">
        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif

            <article class="superior">

                @if($client->id)
                    <h1>Cliente : Detalle</h1>
                @else
                    <h1>Creando Cliente</h1>
                @endif



            </article>
            <section class="contenedor">
                @if($client->id)
                    {!! Form::open(['url' => 'cms/clients/'.$client->id,'method' => 'put', 'files' => true] ) !!}

                @else
                    {!! Form::open(['url' => 'cms/clients', 'method' => 'post', 'files' => true]) !!}

                @endif
                    <div id="contenido">

                        <section class="principal">
                            <div class="atributo#s">
                                <article class="seo">


                                    <div class="fila">
                                        {{ csrf_field() }}
                                        <span class="tit-1">Nombre del Cliente</span>
                                        <input value="{{ ( $client->name )? $client->name : old('name')  }}" id="fullname" type="text" placeholder="Usuario" name="name" required="">
                                    </div>                                        <div class="fila">

                                        <span class="tit-1">Web </span>
                                        <input value="{{ ( $client->web )? $client->web : old('web')  }}" name="web" type="text"  placeholder="http\\:www.web.com" required="">
                                    </div>

                                    <div class="fila">
                                        {{--<span class="tit-1">Maque las casillas</span>--}}
                                        <div class="radios">
                                            <label>

                                                {!! Form::checkbox('is_active', '1', ( $client->is_active == 1)? true : false  )!!}
                                                <span>Activo</span>
                                            </label>
                                        </div>
                                    </div>

                                </article>
                            </div>
                            {{--<div class="contenidos">--}}
                            {{--<span class="tit-1">Contenido</span>--}}
                            {{--<input name="contenido" type="hidden" value="contenido ">--}}
                            {{--<div class="editor"> </div>--}}
                            {{--</div>--}}
                        </section>
                        <section class="lateral">
                            <article id="medios">
                                <div class="imagen">
                                    <div class="titular">
                                        <h3>Imagen
                                            <span class="medida-sug">| &nbsp; Medida recomendada 103x103 </span>
                                        </h3>
                                    </div>
                                    <div class="upload">
                                        <label for="adjunto-img" class="campo">
                                            <input type="file" id="adjunto-img" name="logo" class="adjunto-img" onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))">
                                            <input value="{{ ( $client->logo )? $client->logo: old('logo')  }}" name="logo_t" id="logo_t"  type="hidden" >
                                            <span>Elegir</span>
                                        </label>
                                    </div>
                                    <figure>
                                        <picture>
                                            <source srcset="{{ ( $client->logo )? asset("../storage/app/clients/".$client->logo) : old('logo')  }}" media="(min-width: 600px)">
                                            <img src="{{ ( $client->logo )? asset("../storage/app/clients/".$client->logo) : old('logo')  }}" /> </picture>
                                    </figure>
                                </div>

                            </article>
                        </section>

                    </div>
                    <div id="acciones">
                        <a class="boton-1 boton-2" href="{!! url('cms/users/') !!}">Cancelar</a>
                        <button type="submit" class="boton-1 ">Actualizar</button>
                    </div>
                    </form>
            </section>
        </main>
    </section>

@endsection

@section("js")
    @parent
    <script src="{{URL::asset('assets/cms/js/client_cms.js')}}" defer></script>
@endsection





