@extends('cms\layouts\master')

@section("css")
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="{{asset("assets/cms/css/home.css")}}" media="screen" title="estilo">
    <link rel="stylesheet" href="{{asset("assets/cms/css/proyect.css")}}" media="screen" title="estilo">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


@endsection

@section('title', 'Projectos')

@section('content')
    {{--<div id="alerta">--}}
    {{--<div class="msg-box">--}}
    {{--<h2>¿Estás seguro?</h2>--}}
    {{--<p>Estas apunto de eliminar este registro</p>--}}
    {{--<div class="acciones">--}}
    {{--<button id="cancelar" class="btn eliminar">Cancelar</button>--}}
    {{--<button class="btn confirm">Aceptar</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}


    <section class="pagina">
        <main id="cuerpo">
            @if($proyectos->id)
                <h1>Editar Proyecto</h1>
            @else
                <h1>Nuevo Proyecto</h1>
            @endif
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif


            <div class="container">
                {{--<form class="form" method="post" action='{{url("cms/projectos")}}' >--}}
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="nav-item">
                                    <a href="#step1"  data-toggle="tab" aria-controls="step1" role="tab" title="General" class="nav-link active">
                                <span class="round-tab">
                                    <i class="fa fa-building"></i>
                                </span>
                                    </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#step2"  data-toggle="tab" aria-controls="step2" role="tab" title="Tipo de departamento" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="fa fa-building"></i>
                                </span>
                                    </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#step3"  data-toggle="tab" aria-controls="step3" role="tab" title="Lista de precios" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="fa fa-building"></i>
                                </span>
                                    </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#step4"  data-toggle="tab" aria-controls="step4" role="tab" title="Departamentos" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="fa fa-building"></i>
                                </span>
                                    </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#step5" data-toggle="tab" aria-controls="step5" role="tab" title="Estacionamientos" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="fa fa-building"></i>
                                </span>
                                    </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#step6" data-toggle="tab" aria-controls="step6" role="tab" title="Depósitos" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="fa fa-building"></i>
                                </span>
                                    </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#step7"  data-toggle="tab" aria-controls="step7" role="tab" title="Documentos" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="fa fa-building"></i>
                                </span>
                                    </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#step8"  data-toggle="tab" aria-controls="step8" role="tab" title="Vendedores" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="fa fa-building"></i>
                                </span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active text-center" role="tabpanel" id="step1">
                                <h1 class="text-md-center">General</h1>

                                {{--1) General--}}
                                <div class="row project_form" >
                                    <form action="" id="project_form_container" style="width: 100%;">
                                    <div class="form-group" style="width: 100%;">
                                        <label style=" width: 20%; float: left;">Nombre</label>
                                        <input class="form-control" type="text" name="nombre" value="" style=" width: 80%; float: left;">
                                    </div>

                                    <div class="form-group" style="width: 100%;">
                                        <label style=" width: 20%; float: left;">Distrito</label>
                                        <select name="distrito_iddistrito" class="form-control" style=" width: 80%; float: left;">
                                            <option value="">Seleccione un Distrito</option>
                                            @foreach($distritos as $key => $distrito)
                                                <option value="{{$distrito->iddistrito}}">{{$distrito->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group" style="width: 100%;">
                                        <label style=" width: 20%; float: left;">Dirección</label>
                                        <input class="form-control" type="text" name="direccion" style=" width: 80%; float: left;"> </div>
                                    <div class="form-group" style="width: 100%;">
                                        <label style=" width: 20%; float: left;">Estado del Proyecto</label>
                                        <select class="form-control" name="estado_proyecto" style=" width: 80%; float: left;">
                                            <option value="">Seleccione Estado del Proyecto</option>
                                            <option value="1">Preregistrado</option>
                                            <option value="2">Preventa</option>
                                            <option value="3">En construcción</option>
                                            <option value="4">Entrega Inmediata</option>
                                            <option value="5">Entregado</option>
                                        </select>
                                    </div>
                                    <div class="form-group" style="width: 100%;">
                                        <label style=" width: 20%; float: left;">Banco Financiador</label>
                                        <select class="form-control" name="banco_idbanco" style=" width: 80%; float: left;">
                                            <option value="">Seleccione una Banco</option>
                                            @foreach($bancos as $key => $banco)
                                                <option value="{{$banco->idbanco}}">{{$banco->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group" style="width: 100%;">
                                    </div>
                                    </form>
                                </div>
                                <ul class="list-inline text-md-center">
                                    <li><span class="btn btn-lg btn-common  next-button boton-1 project_button ">Empezar</span></li>
                                </ul>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="step2">
                                {{--2) Tipo de departamento--}}
                                <h1 class="text-md-center">Tipo de departamento</h1>
                                <form id="form_tipodepartamento">
                                <div class="row" id="tipodepartamento">                                    
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Nombre tipo de Departamento</span>
                                            <input  class="campo_llenar form-group small" type="text" name="nombre" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Inicio Área Maxima</span>
                                            <input onkeypress="return soloNumeros(event)" class="campo_llenar form-group small" type="text" name="inicio_area_maxima" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Fin Área Maxima</span>
                                            <input onkeypress="return soloNumeros(event)" class="campo_llenar form-group small" type="text" name="fin_area_maxima" value="">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Número de habitaciones</span>
                                            <input onkeypress="return soloNumeros(event)" class="campo_llenar form-group small" type="text" name="nro_habitacion" value="">
                                        </div>
                                    </div>                
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Número de baños</span>
                                            <input onkeypress="return soloNumeros(event)" class="campo_llenar form-group small" type="text" name="nro_banos" value="">
                                        </div>
                                    </div>                                    
                                    <div class="col-md-2">
                                        <span class="tit-1">Vista a la Calle</span>
                                        <div class="form-check form-check-inline">
                                            <label class="form-check-label">
                                                <input class="form-check-input campo_llenar" name="vista_parque"  type="checkbox" id="inlineCheckbox1" value="1">
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <span class="tit-1">Seleccione Plano</span>
                                        <input type="file" name="url_plano" class="form-control-file border campo_llenar">
                                    </div>
                                    <div class="col-md-2">
                                        <span type="#" class="boton-1 agregar_a_tabla">Agregar</span>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive"  tabla="tipodepartamento_t" id="tipodepartamento_t">
                                            <table class="table table-bordered" id="tipodepartamento_table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Nombre</th>
                                                    <th scope="col">Inicio de Área</th>
                                                    <th scope="col">Hasta Fin Área</th>
                                                    <th scope="col">Nro habitaciones</th>
                                                    <th scope="col">Nro de baños</th>
                                                    <th scope="col">Vista a la calle</th>
                                                    <th scope="col">Plano</th>
                                                    <th scope="col">Área total</th>
                                                    <th scope="col"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <ul class="list-inline text-md-center">
                                    <li><span  class="btn btn-lg btn-common  next-button boton-1 tipodepartamento_button">Siguiente</span></li>
                                </ul>
                                </form>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="step3">
                                {{--3) Lista de valores--}}
                                <h1 class="text-md-center">Lista de precios</h1>

                                {{--General--}}
                                <form id="lp_general" action="">
                                {{--lp1 general--}}
                                <div class="row">                                        
                                        <div class="col-md-12">
                                            <h3>General</h3>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="fila">
                                                <span class="tit-1">Factor de área libre</span>
                                                <input class="form-group small" type="text" name="factor_area_libre" onkeypress="return soloNumeros(event)" value=""> %
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="fila">
                                                <span class="tit-1">Factor descuento preventa</span>
                                                <input onkeypress="return soloNumeros(event)"  class="form-group small" type="text" name="factor_desc_preventa" value=""> %</div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="fila">
                                                <span class="tit-1">Factor Incremento entrega</span>
                                                <input class="form-group small" onkeypress="return soloNumeros(event)"  type="text" name="factor_incr_entrega" value=""> %</div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="fila">
                                                <span class="tit-1">Descuento de ubicación</span>
                                                <input class="form-group small" onkeypress="return soloNumeros(event)"  type="text" name="factor_vista_exterior" value=""> %</div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="fila">
                                                <span class="tit-1">Moneda </span>
                                                <select class="campo_llenar" name="moneda">
                                                    <option value="">Seleccione un tipo</option>
                                                    <option value="1">S/. Soles</option>
                                                    <option value="2">$ Dolares</option>
                                                </select>
                                            </div>
                                        </div>

                                </div>
                                </form>
                                {{--Promociones--}}
                                {{--lp2 promociones--}}
                                <form id="lp_promociones">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Promociones</h3>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Promoción</span>
                                            <select class="campo_llenar" name="piso_idpiso">
                                                <option value="">Seleccione una promoción</option>
                                                @foreach($promociones as $key => $promocion)
                                                    <option value="{{$promocion->idpromocion}}">{{$promocion->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Factor de descuento</span>
                                            <input class="form-group small" onkeypress="return soloNumeros(event)"  type="text" name="descuento" value=""> %
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="fila">
                                            <span class="tit-1">Fecha de inicio</span>
                                            <input class="form-group small"  type="date" name="fecha_inicio" value=""> </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="fila">
                                            <span class="tit-1">Fecha de fin</span>
                                            <input class="form-group small" type="date" name="fecha_fin" value=""> </div>
                                    </div> -->
                                </div>
                                </form>
                                {{--Departamento--}}
                                {{--lp3 departamento--}}
                                <form id="lp_departamentos">
                                <div class="row" id="preciodepa">
                                    <div class="col-md-12">
                                        <h3>Departamento</h3>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Tipo de departamento</span>
                                            <select class="campo_llenar" name="piso_idpiso">
                                                <option value="">Seleccione un tipo de departamento</option>
                                                @foreach($tipodepartamentos as $key => $tipodepartamento)
                                                    <option value="{{$tipodepartamento->idtipo_departamento}}">{{$tipodepartamento->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Inicio Área Maxima</span>
                                            <input onkeypress="return soloNumeros(event)" class="campo_llenar form-group small" type="text" name="area_inicio" value="">
                                        </div>
                                    </div>                                     -->
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Inicio Piso Maximo</span>
                                            <input onkeypress="return soloNumeros(event)" class="campo_llenar form-group small" type="text" name="piso_inicio" value=""> </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Fin Piso Maximo</span>
                                            <input onkeypress="return soloNumeros(event)" class="campo_llenar form-group small" type="text" name="piso_fin" value=""> </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Preció Área (M2)</span>
                                            <input onkeypress="return soloNumeros(event)" class="campo_llenar form-group small" type="text" name="precio_area" value=""> </div>
                                    </div>
                                    <div class="col-md-2">
                                        <span type="#" class="boton-1 agregar_a_tabla">Agregar</span>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive"  tabla="preciodepa_t" id="preciodepa_t">
                                            <table class="table table-bordered" id="preciodepa_table">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Inicio de Área</th>
                                                    <th scope="col">Hasta Fin Área</th>
                                                    <th scope="col">Inicio de Piso</th>
                                                    <th scope="col">Hasta Fin Piso</th>
                                                    <th scope="col">Precio (M2)</th>
                                                    <th scope="col"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                {{--Estacionamiento--}}
                                {{--lp4 Estacionamiento--}}
                                <form id="lp_estacionamientos">
                                <div class="row" id="precioesta">
                                    <div class="col-md-12">
                                        <h3>Estacionamiento</h3>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="fila">
                                            <span class="tit-1">Piso</span>
                                            <select class="campo_llenar" name="piso_idpiso">
                                                <option value="">Seleccione un Piso</option>
                                                @foreach($flg_estacionmientos as $key => $flg_estacionmiento)
                                                    <option value="{{$flg_estacionmiento->idpiso}}">{{$flg_estacionmiento->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="fila">
                                            <span class="tit-1">Tipo </span>
                                            <select class="campo_llenar" name="tipo">
                                                <option value="">Seleccione un tipo</option>
                                                <option value="0">Simple</option>
                                                <option value="1">Doble</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="fila">
                                            <span class="tit-1">Precio</span>
                                            <input class="form-group small campo_llenar" type="text" name="precio" >
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span type="#" class="boton-1 agregar_a_tabla">Agregar</span>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="table-responsive"  tabla="precioesta_t" id="precioesta_t">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr>
                                                    <th scope="col">Piso</th>
                                                    <th scope="col">Tipo</th>
                                                    <th scope="col">Precio</th>
                                                    <th scope="col"></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                </form>
                                {{--Depósitos--}}
                                {{--lp5 Depositos--}}
                                <form id="lp_depositos" action="">
                                <div class="row">
                                    <div class="col-md-12">
                                        <h3>Depósitos</h3>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="fila">
                                            <span class="tit-1">Valor depósito por área (M2)</span>
                                            <input class="form-group small"  onkeypress="return soloNumeros(event)"  type="text" name="valor_deposito" >
                                        </div>
                                    </div>
                                </div>
                                </form>
                                <ul class="list-inline text-md-center">
                                    <li><button  class="btn btn-lg btn-common precios_button  next-button boton-1">Siguiente</button></li>
                                </ul>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="step4">
                                {{--4) Departamentos--}}
                            <form id="form_departamentos" action="" enctype="multipart/form-data">
                                
                                <h1 class="text-md-center">Departamentos</h1>
                                <div class="row">

                                    <div class="container">
                                        <div class="row" tabla="departamento" id="departamento">

                                            <div class="col-md-2">

                                                <div class="fila">
                                                    <span class="tit-1">Piso</span>
                                                    <select class="campo_llenar" name="piso_idpiso">
                                                        <option value="">Seleccionar un Piso</option>
                                                        @foreach($flg_departamentos as $key => $flg_departamento)
                                                            <option value="{{$flg_departamento->idpiso}}">{{$flg_departamento->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="fila">
                                                    <span class="tit-1">Departamento</span>
                                                    <input class="campo_llenar" style="width: 100px;" type="text" name="departamento" value=""> </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="fila">
                                                    <span class="tit-1">Área Libre (M2)</span>
                                                    <input class="campo_llenar"  style="width: 100px;"type="text" name="area_libre" id="area_libre" value=""> </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="fila">
                                                    <span class="tit-1">Área techada (M2)</span>
                                                    <input class="campo_llenar"  style="width: 100px;"type="text" name="area_techada" id="area_techada" value=""> </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="fila">
                                                    <span class="tit-1">Nro de Habitaciones</span>
                                                    <input class="campo_llenar" style="width: 100px;" type="text" name="nro_habitacion" value=""> </div>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="fila">
                                                    <span class="tit-1">Nro de baños</span>
                                                    <input class="campo_llenar" style="width: 100px;" type="text" name="nro_banos" value=""> </div>
                                            </div>
                                            <div class="col-md-2">
                                                <div class="fila">
                                                    <span class="tit-1">Valor de Venta</span>
                                                    <input class="campo_llenar"  style="width: 100px;"stype="text" name="valor_venta" value=""> </div>
                                            </div>
                                            <div class="col-md-5">
                                                <span class="tit-1">Seleccione Plano</span>
                                                <input type="file" name="url_plano" class="form-control-file border campo_llenar">
                                            </div>
                                            <div class="col-md-2">
                                                <span class="tit-1">Vista a la Calle</span>
                                                <div class="form-check form-check-inline">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input campo_llenar" name="is_vista_calle"  type="checkbox" id="inlineCheckbox1" value="1">
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <span type="#" class="boton-1 agregar_a_tabla">Agregar</span>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <table class="table" tabla="departamento_t" id="departamento_t">
                                        <thead>
                                        <tr>
                                            <th scope="col">Piso</th>
                                            <th scope="col">Departamento</th>
                                            <th scope="col">Área libre (M2)</th>
                                            <th scope="col">Área Techada (M2)</th>
                                            <th scope="col">Nro de habitaciones</th>
                                            <th scope="col">Nro de baños</th>
                                            <th scope="col">Valor Venta</th>
                                            <th scope="col">Plano</th>
                                            <th scope="col">Tiene vista de calle</th>

                                            {{--<th scope="col">Área Total (M2)</th>--}}
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <!-- <tr>
                                            <th scope="col">1</th>
                                            <th scope="col">101</th>
                                            <th scope="col">22.36</th>
                                            <th scope="col">107.61</th>
                                            <th scope="col">3</th>
                                            <th scope="col">2</th>
                                            <th scope="col">235000</th>
                                            <th scope="col">129.97</th>
                                            <th>
                                                <a class="btn btn-xs" href="projectos/3/edit">Borrar</a>
                                                <a class="btn" href="projectos/3/edit">Editar</a>
                                            </th>
                                        </tr> -->
                                        </tbody>
                                    </table>
                                </div>
                                <ul class="list-inline text-md-center">
                                    <li><span  class="btn btn-lg btn-common  next-button boton-1 departamento_button">Siguiente</span></li>
                                </ul>
                            </form>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="step5">
                                {{--5) Estacionamiento--}}
                            <form action="" id="form_estacionamiento">
                                <h1 class="text-md-center">Estacionamiento</h1>
                                <div class="row">
                                    <div class="row">

                                        <div class="container">
                                            <div class="row" id="estacionamiento">
                                                <div class="col-md-2">
                                                    <div class="fila">
                                                        <span class="tit-1">Piso</span>
                                                        <select class="campo_llenar" name="piso_idpiso">
                                                            <option value="">Seleccione un Piso</option>
                                                            @foreach($flg_estacionmientos as $key => $flg_estacionmiento)
                                                                <option value="{{$flg_estacionmiento->idpiso}}">{{$flg_estacionmiento->nombre}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="fila">
                                                        <span class="tit-1">Estacionamiento</span>
                                                        <input class="campo_llenar" type="text" name="estacionamiento" value=""> </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="fila">
                                                        <span class="tit-1">Área Libre (M2)</span>
                                                        <input class="campo_llenar" type="text" name="area_libre" id="area_libre_e" value=""> </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="fila">
                                                        <span class="tit-1">Área techada (M2)</span>
                                                        <input class="campo_llenar" type="text" name="area_techada" id="area_techada_e" value=""> </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="fila">
                                                        <span class="tit-1">Tipo Estacionamiento</span>
                                                        <select class="campo_llenar" name="tipo_estacionamiento">
                                                            <option value="">Seleccione un Estacionamiento</option>
                                                            <option value="Simple">Simple</option>
                                                            <option value="Doble">Doble</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="fila">
                                                        <span class="tit-1">Valor de Venta</span>
                                                        <input class="campo_llenar" type="text" name="valor_venta" value=""> </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <span type="#" class="boton-1 agregar_a_tabla ">Agregar</span>
                                                    <!-- <button  class="boton-1 agregar_a_tabla">Agregar</button> -->
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">
                                    <table class="table" tabla="estacionamiento_t" id="estacionamiento_t">
                                        <thead>
                                        <tr>
                                            <th scope="col">Piso</th>
                                            <th scope="col">Estacionamiento</th>
                                            <th scope="col">Área libre (M2)</th>
                                            <th scope="col">Área Techada (M2)</th>
                                            <th scope="col">Tipo Estacionamiento</th>
                                            <th scope="col">Valor Venta (M2)</th>
                                            <th scope="col">Área Total(M2)</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{-- <tr>
                                            <th scope="col">Sotano 1</th>
                                            <th scope="col">Estacionamiento 1</th>
                                            <th scope="col">7.9</th>
                                            <th scope="col">20.52</th>
                                            <th scope="col">Simple</th>
                                            <th scope="col">16000</th>
                                            <th scope="col">28.42</th>

                                            <th>
                                                <a class="btn" href="projectos/3/edit">Editar</a>
                                            </th>
                                        </tr> --}}

                                        </tbody>
                                    </table>
                                </div>
                                </form>
                                <ul class="list-inline text-md-center">
                                    <li><span  class="btn btn-lg btn-common estacionamiento_button next-button boton-1">Siguiente</span></li>
                                </ul>
                            </div>

                            <div class="tab-pane" role="tabpanel" id="step6">
                                {{--6) Depositos--}}
                                <h1 class="text-md-center">Depositos</h1>
                                <form id="form_depositos">
                                 <div class="row" id="depositos">
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Piso</span>
                                            <select class="campo_llenar" name="piso_idpiso">
                                                <option value="">Seleccionar un Piso</option>
                                                @foreach($flg_depositos as $key => $flg_deposito)
                                                    <option value="{{$flg_deposito->nombre}}">{{$flg_deposito->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Deposito</span>
                                            <input class="campo_llenar" type="text" name="deposito" value=""> </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Área techada (M2)</span>
                                            <input class="campo_llenar" type="text" name="area_techada" value=""> </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Dpto Asociado</span>
                                            <select class="campo_llenar" id="deposito_dpo" name="dpto_asociado">
                                                <option value="">Seleccione Dpto Asociado</option>
                                                {{--@foreach($departamentos as $key => $departamento)--}}
                                                    {{--<option value="{{$departamento->iddepartamentos}}">{{$departamento->departamento}}</option>--}}
                                                {{--@endforeach--}}
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="fila">
                                            <span class="tit-1">Valor de Venta</span>
                                            <input class="campo_llenar" type="text" name="precio_venta" value=""> </div>
                                    </div>
                                    <div class="col-md-2">
                                        <span type="#" class="boton-1 agregar_a_tabla">Agregar</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <table class="table" tabla="depositos_t" id="depositos_t">
                                        <thead>
                                        <tr>
                                            <th scope="col">Piso</th>
                                            <th scope="col">Depósito</th>
                                            <th scope="col">Área Techada (M2)</th>
                                            <th scope="col">Dpto. Asociado</th>
                                            <th scope="col">Valor Venta</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{-- <tr>
                                            <th scope="col">Sotano 1</th>
                                            <th scope="col">Deposito 1</th>
                                            <th scope="col">4.92</th>
                                            <th scope="col">Dpto 001</th>
                                            <th scope="col">3444</th>
                                            <th>
                                                <a class="btn" href="#">Borrar</a>
                                            </th>
                                        </tr> --}}

                                        </tbody>
                                    </table>
                                </div>
                                <ul class="list-inline text-md-center">
                                    <li><span  class="btn btn-lg btn-common deposito_button next-button boton-1">Siguiente</span></li>
                                </ul>
                                </form>
                            </div>

                            <div class="tab-pane" role="tabpanel" id="step7">
                                {{--7) Documentos--}}
                                <h1 class="text-md-center">Documentos</h1>
                                <form action="" id="form_documentos">
                                <div class="row" id="documento">
                                    <div class="col-md-3">
                                        <div class="fila">
                                            <span class="tit-1">Tipo</span>
                                            <select class="campo_llenar" name="tipo">
                                                <option value="">Seleccione un Tipo</option>
                                                <option value="Anexos principales">Anexos principales</option>
                                                <option value="Publicidad">Publicidad</option>
                                                <option value="Varios">Varios</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="fila">
                                            <span class="tit-1">Nombre</span>
                                            <input class="campo_llenar" type="text" name="nombre" value=""> </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="upload">
                                            <label for="adjunto-img" class="campo">
                                                <input  type="file" id="adjunto-img" name="url_plano" class="adjunto-img campo_llenar " onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))">
                                                <span>Elegir</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <span type="#" class="boton-1 agregar_a_tabla deposito_button">Agregar</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <table class="table" tabla="documento_t" id="documento_t">
                                        <thead>
                                        <tr>
                                            <th scope="col">Tipo</th>
                                            <th scope="col">Nombre</th>
                                            <th scope="col">Archivo</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{-- <tr>
                                            <th scope="col">Publicidad</th>
                                            <th scope="col">Flyer Publicitario</th>
                                            <th scope="col">FlyerPublicitario.pdf</th>
                                            <th>
                                                <a class="btn" href="projectos/3/edit">Borrar</a>
                                            </th>
                                        </tr> --}}

                                        </tbody>
                                    </table>
                                </div>
                                <ul class="list-inline text-md-center">
                                    <li><span  class="btn btn-lg btn-common documento_button next-button boton-1">Siguiente</span></li>
                                </ul>
                                </form>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="step8">
                                {{--8) Vendedores--}}
                                <h1 class="text-md-center">Vendedores</h1>
                                <form id="form_vendedores" action="">
                                <div class="row" id="vendedores">
                                    <div class="col-md-4">
                                        <div class="fila">
                                            <span class="tit-1">Vendedor</span>
                                            <select class="campo_llenar" name="usuario_idusuario">
                                                <option value="">Seleccione un Vendedor</option>
                                                @foreach($usuarios_v as $key => $usuario_v)
                                                    <option value="{{$usuario_v->idusuario}}">{{$usuario_v->nombre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="fila">
                                            <span class="tit-1">Porcentaje de Comisión</span>
                                            <input class="campo_llenar" type="text" name="porcentaje_comision" value=""> </div>
                                    </div>
                                    <div class="col-md-4">
                                        <span type="#" class="boton-1 agregar_a_tabla">Agregar</span>
                                    </div>
                                </div>

                                <div class="row">
                                    <table class="table" tabla="vendedores_t" id="vendedores_t">
                                        <thead>
                                        <tr>
                                            <th scope="col">Vendedor</th>
                                            <th scope="col">Porcentaje de Comisión</th>
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {{-- <tr>
                                            <td scope="col">Ana Pareces</td>
                                            <td scope="col">3 %</td>
                                            <td>
                                                <a class="btn" href="#">Borrar</a>
                                            </td>
                                        </tr> --}}

                                        </tbody>
                                    </table>
                                </div>
                                <ul class="list-inline text-md-center">
                                    <li><span type="submit" placeholder="Crear Proyecto" class="btn btn-lg btn-common  next-button boton-1 vendedor_button">Finalizar</span></li>
                                </ul>
                                </form>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                {{--</form>--}}
            </div>




            <article class="superior">
                <div class="row">



                </div>




            </article>






        </main>
    </section>

@endsection

@section("js")

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script >
        var site_url="{{url("cms")}}";
    </script>>
    <script src="{{asset("assets/cms/js/validate.js")}}"></script>>
    <script src="{{asset("assets/cms/js/validate_aditional.js")}}"></script>>


    <script src="{{asset("assets/cms/js/project_cms_editar.js")}}"></script>>

@endsection

