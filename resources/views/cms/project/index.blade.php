@extends('cms\layouts\master')

{{--@section("css")--}}
    {{--<link rel="stylesheet" href="css/general.css">--}}
    {{--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}
    {{--<link rel="stylesheet" href="css/home.css" media="screen" title="estilo">--}}
{{--@endsection--}}

@section('title', 'Dashboard')



@section('content')
    <div id="alerta">
        <div class="msg-box">
            <h2>¿Estás seguro?</h2>
            <p>Estas apunto de eliminar este registro</p>
            <div class="acciones">
                <button id="cancelar" class="btn eliminar">Cancelar</button>
                <button class="btn confirm">Aceptar</button>
            </div>
        </div>
    </div>



    <section class="pagina">

        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guard    ar registro</p>-->
                </div>
            @endif
            <article class="superior">
                <h1>Proyectos</h1>
                <div class="acciones">
                    <div class="filtro">
                        {{--<input type="search" class="search" name="buscar" data-sort="name" id="buscar" placeholder="Filtrar">  </div>--}}
                    <a href="{!! url('cms/projectos/create') !!}" id="agregar">Agregar</a>
                </div>
            </article>
            <section class="contenedor">
                <form action="{!! url('/cms/projects_wizard_create'); !!}" method="post">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    {{--<input type="submit" placeholder="Test">--}}
                    {{--  <a class="btn" href="projects/{{$project->id}}/edit">Editar</a>--}}
                </form>

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Nombre</th>
                        <th scope="col">Distrito</th>
                        <th scope="col">Direccion</th>
                        <th scope="col">Estado del proyecto</th>
                        {{--<th scope="col">Editar</th>--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($projects as $key => $project)
                        <tr>
                            <th scope="col">{{$project->nombre}}</th>
                            <th scope="col">{{$distrito::find($project->distrito_iddistrito)->nombre}}</th>
                            <th scope="col">{{$project->direccion}}</th>
                            <th scope="col">
                                @if($project->estado_proyecto==1){{"Preregistrado"}}@endif
                                @if($project->estado_proyecto==2){{"Preventa"}}@endif
                                @if($project->estado_proyecto==3){{"En construcción"}}@endif
                                @if($project->estado_proyecto==4){{"Entrega Inmediata"}}@endif
                                @if($project->estado_proyecto==5){{"Entregado"}}@endif




                            </th>
                            {{--<th>--}}
                                {{--<a class="btn" href="projectos/{{$project->idproyectos}}/edit">Editar</a>--}}
                            {{--</th>--}}
                        </tr>
                    @endforeach

                    </tbody>
                </table>



            </section>
@endsection






