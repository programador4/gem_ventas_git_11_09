
    @section("base_js")
    <!--js principales-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.pack.js" type="text/javascript"></script>

    <script type="text/javascript" src="{{ URL::asset('assets/cms/js/picturefill.min.js') }}" async></script>
    <script type="text/javascript" src="{{ URL::asset('assets/cms/js/general.js') }}" async defer></script>
    <!--js principales-->
    @show
