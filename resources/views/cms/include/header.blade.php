	@section("preload")

	@show



 @section("header")
		<header id="encabezado">
			<div class="head-top">
				<h1 class="logo">
					<img src="{{ URL::asset('backend_assets/img/atomikal-logo-blanco.png')}}" alt=""> </h1>
				<aside id="usuario">
					<h2>{{isset(Auth::user()->username) ? Auth::user()->fullname : ""}}</h2>
					<a style="color:#000" href="{{action('Login\LoginController@logout')}}">Cerrar sesión</a>
				</aside>
			</div>
			<nav id="menu">
				<label class="control" for="control-menu"></label>
				<input type="checkbox" id="control-menu" />
				<ul class="lista">

					{{--<li class="enlace">--}}
						{{--<a href="{!! url('/cms/usertypes'); !!}">Tipos de usuario</a>--}}
					{{--</li>--}}


					<li class="enlace">
						<a href="{!! url('/cms/projectos'); !!}">Proyectos</a>
					</li>
					<li class="enlace">
						<a href="{!! url('/cms/visitas'); !!}">Visitas</a>
					</li>
					<li class="enlace">
						<a href="{!! url('/cms/seguimiento'); !!}">Seguimiento</a>
					</li>
					<li class="enlace">
						<a href="{!! url('/cms/Pagos'); !!}">Pagos</a>
					</li>

					<li class="enlace">
						<a href="{!! url('/cms/users'); !!}">Usuarios</a>
					</li>

					<li class="enlace">
						<a href="{!! url('/cms/reportes'); !!}">Reportes</a>
					</li>

					<li class="enlace">
						<a href="{!! url('/cms/promocion'); !!}">Promoción</a>
					</li>

					<li class="enlace">
						<a href="{!! url('/cms/precios'); !!}">Precios</a>
					</li>

				</ul>
			</nav>
		</header>
 @show
