

    <div style="width:100%;">

        <div style="float: left;"><img style="width: 250px;"height="250px" src="{!!url()!!}/proyect_logos/{{$projecto->url_plano}}" alt=""></div>
        <div style="float: right;">
            <span>PROYECTO:{{$projecto->nombre}}</span>
            <br>
            <span>Fecha:{{$cliente->fecha}}</span>
        </div>
    </div>
    <div>
    <div><span>DNI:{{$cliente->dni}}</span></div>
    <div><span>CORREO:{{$cliente->correo}}</span></div>
    <div><span>TELEFONO:{{$cliente->telefono}}</span></div>
    </div>

    <table style="border: 1px solid #000;width: 100%;border-color: black;">
        <tr>
        <td  style="text-align: center;background-color: green">Nombre del Departamento	</td>
        <td  style="text-align: center;background-color: green">Área techada m2	</td>
        <td  style="text-align: center;background-color: green">Área Libre m2	</td>
        <td  style="text-align: center;background-color: green">Precio	</td>
        <td  style="text-align: center;background-color: green">Promoción	</td>
        <td  style="text-align: center;background-color: green">Total</td>
        </tr>
        <tr>
            <td>{{ $departamento->departamento }}</td>
            <td>{{ $tipo_departamento->inicio_area_maxima }}</td>
            <td>{{ $tipo_departamento->fin_area_maxima }}</td>
            <td>{{ $tipo_departamento->valor_venta }}</td>
            <td>{{ $promocion->descuento }}</td>
            <td><?php  echo($tipo_departamento->precio * ((100-$promocion->descuento) / 100));?></td>
        </tr>
    </table>
    <br>
    <table style="border: 1px solid #000;width: 100%;border-color: black">
        <tr>
            <td  style="text-align: center;background-color: green">Nombre del Estacionamiento	</td>
            <td  style="text-align: center;background-color: green">Área techada m2	</td>
            <td  style="text-align: center;background-color: green">Área Libre m2	</td>
            <td  style="text-align: center;background-color: green">Precio	</td>
            <td  style="text-align: center;background-color: green">Promoción	</td>
            <td  style="text-align: center;background-color: green">Total</td>
        </tr>
        <tr>
            <td>{{ $estacionamiento->estacionamiento }}</td>
            <td>{{ $estacionamiento->area_libre }}</td>
            <td>{{ $estacionamiento->area_techada }}</td>
            <td>{{ $estacionamiento->precio_venta }}</td>
            <td>{{ $estacionamiento->dscto_estacionamiento }}</td>
            <td><?php  echo($estacionamiento->precio_venta * ((100-$estacionamiento->dscto_estacionamiento ) / 100))?></td>
        </tr>
    </table>
    <br>
    <table style="width: 100%;border-color: black;border: 1px solid #000;">
        <tr>
            <td  style="text-align: center;background-color: green">Nombre del Deposito	</td>
            <td  style="text-align: center;background-color: green">Área techada m2	</td>
            <td  style="text-align: center;background-color: green">Precio	</td>
            <td  style="text-align: center;background-color: green">Promoción	</td>
            <td  style="text-align: center;background-color: green">Total</td>
        </tr>
        <tr>
            <td>{{ $deposito->deposito }}</td>
            <td>{{ $deposito->area_techada }}</td>
            <td>{{ $deposito->precio_venta }}</td>
            <td>{{ $promocion->descuento  }}</td>
            <td><?php  echo($deposito->precio_venta * ((100-$promocion->descuento  ) / 100));?></td>
        </tr>
    </table>
    <br>
    <br>
    <div><span>Tipo de financiamiento:{{$projecto->financiamiento}}</span></div>
    <div><span>Banco financiero:{{$projecto->banco_idbanco}}</span></div>
    <br>
    <div><b>Datos de vendedor</b></div>
    <div><span>Nombre:{{$usuario->nombre}} {{$usuario->apellido}}</span></div>
    <div><span>Teléfono:{{$usuario->telefono}}</span></div>
    <div><span>Correo:{{$usuario->correo}}</span></div>
    <div><span>Legales:</span></div>
    <div style="min-height: 250px;border: 1px solid #000; border-color: #000;width: 90%;float: none;display: block;margin: 0 auto;">{{$projecto->legales}}</div>