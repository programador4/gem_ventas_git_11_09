@extends('cms\layouts\master')

{{--@section("css")--}}
    {{--<link rel="stylesheet" href="css/general.css">--}}
    {{--<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">--}}
    {{--<link rel="stylesheet" href="css/home.css" media="screen" title="estilo">--}}
{{--@endsection--}}

@section('title', 'Dashboard')



@section('content')
    <div id="alerta">
        <div class="msg-box">
            <h2>¿Estás seguro?</h2>
            <p>Estas apunto de eliminar este registro</p>
            <div class="acciones">
                <button id="cancelar" class="btn eliminar">Cancelar</button>
                <button class="btn confirm">Aceptar</button>
            </div>
        </div>
    </div>



    <section class="pagina">

        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guard    ar registro</p>-->
                </div>
            @endif
            <article class="superior">
                <h1>Visitas</h1>
                <div class="acciones">
                    <div class="filtro">
                        {{--<input type="search" class="search" name="buscar" data-sort="name" id="buscar" placeholder="Filtrar">  </div>--}}
                    <a href="{!! url('cms/visitas/create') !!}" id="agregar">Agregar</a>
                </div>
            </article>
            <section class="contenedor">
                <form action="{!! url('/cms/visitas_wizard_create'); !!}" method="post">
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                </form>
                <form action="{!! url('/cms/visitas'); !!}" method="get">
                <div class="row">
                    <div class="col-md-3">
                        <div class="fila">
                            <span class="tit-1">Proyecto</span>
                            <select name="project" class="form-control" style=" width: 80%; float: left;">
                                <option value="" disabled >Seleccione un tipo</option>
                                @foreach($projects as $key => $project)
                                <option @if($visitas_r->project==$project->idproyectos) selected @endif  value="{{$project->idproyectos}}">{{$project->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="fila">
                            <span class="tit-1">Fecha desde:</span>
                            <input class="form-group small" type="date" name="fecha_inicio" value="{{ ( $visitas_r->fecha_inicio )? $visitas_r->fecha_inicio : old('fecha_inicio')  }}">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="fila">
                            <span class="tit-1">Hasta:</span>
                            <input class="form-group small" type="date" name="fecha_fin" value="{{ ( $visitas_r->fecha_fin )? $visitas_r->fecha_fin : old('fecha_fin')  }}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="fila">
                            <span class="tit-1">Estado de seguimiento</span>
                            <select name="financiamiento" class="form-control" style=" width: 80%; float: left;">
                                <option value="0">Seleccione un tipo</option>                                
                                <option value="1">Pendientes</option>
                                <option value="2">Finalizado</option>                                
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <span class="tit-1"></span>
                        <input type="submit"  placeholder="Buscar"  value="Buscar" class="btn btn-primary">

                    </div>
                </div>
                </form>


                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Fecha de visita</th>
                        <th scope="col">Nombres y Apellidos</th>
                        <th scope="col">Teléfono</th>
                        <th scope="col">Proyecto</th>
                        <th scope="col">Departamento</th>
                        <th scope="col">Interesado</th>
                        <th scope="col">Ultima Acción</th>
                        <th scope="col">Estado del Seguimiento</th>                        
                    </tr>
                    </thead>
                    <tbody>                        
                        @foreach($visita as $key => $visitas)
                            <?php

                            $skip=true;
                                  ?>
                            @if($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->count()>0 && $visitas_r->estado)
                                @if($visitas_r->estado==$seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->get()->last()->detalle)<?php $skip=true;?>@endif
                            @endif
                            <?php
                            $id_proyect   = $visitas->proyectos_idproyectos;
                            $departamento=$departamentos_visitas::where("proyectos_idproyectos",$id_proyect)->get()->last()->departamento;

                            /*****************************************************************/
                            //Aplicando filtros
                            //Fecha desde


                            //                          $filtro_estado=$seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->get()->last()->created_at;
                            if(isset($_GET['fecha_inicio']) && $_GET['fecha_inicio']!="") {
                                if($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->count()>0){
                                    $filtro_fecha_creacion=
                                   $seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->get()->last()->created_at;

                                 if(!($filtro_fecha_creacion < $_GET['fecha_inicio'])){
                                      $skip=false;

                                  }
                                }
                            }

                            //fecha_fin
                            if(isset($_GET['fecha_fin']) && $_GET['fecha_fin']!="") {
                                if($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->count()>0){
                                    $filtro_fecha_creacion=
                                        $seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->get()->last()->created_at;
                                    if($filtro_fecha_creacion > $_GET['fecha_fin']){
                                        $skip=false;
                                  }
                                }
                            }
                            if(isset($_GET['financiamiento']) && $_GET['financiamiento']!="") {
                                if($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->count()>0){
                                    if($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->get()->last()->estado==$_GET['financiamiento']){
                                        $skip=false;
                                    }
                                }
                            }
//                            dd($skip);
//                                dd($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->get()->last()->created_at);
//                            }
//
                            //Fecha hasta
//                            dd($skip);
//                            var_dump($skip);

                            if(!isset($_GET['fecha_fin']) && !isset($_GET['fecha_inicio']) && !isset($_GET['proyecto']) && !isset($_GET['financiamiento'])) $skip=false;
                              if(!$skip){

                            ?>
                        <tr>
                            <th>
                                @if($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->count()>0)
                                    {{$seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->get()->last()->created_at}}
                                @endif
                            </th>
                            <th>{{$visitas->nombres}} {{$visitas->apellidos}}</th>                            
                            <th>{{$visitas->telefono}}</th>
                            <th>
                                {{$projects_visitas::find($visitas->proyectos_idproyectos)->nombre}}
                            </th>

                            <th>
                            <?php
                            $id_proyect   = $visitas->proyectos_idproyectos;
//                                $departamento = $departamentos_visitas::where("proyectos_idproyectos",$id_proyect)->get()->last()->departamento;
//                                dd($departamentos_visitas::where("proyectos_idproyectos",$id_proyect));
                                echo $departamentos_visitas::where("proyectos_idproyectos",$id_proyect)->get()->last()->departamento;
                            ?>
                            </th>
                            <th>
                                @if($visitas->interesado==1){{"Si"}}@endif
                                @if($visitas->interesado==2){{"No"}}@endif
                            </th>
                            <th>
                            @if($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->count()>0)
                            {{$seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->get()->last()->detalle}}
                            @endif
                            </th>

                            <th>
                                @if($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->count()>0)
                                    @if($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->get()->last()->estado==1){{"Activo"}}@endif
                                    @if($seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->get()->last()->estado==0){{"Inactivo"}}@endif
                                @endif

                                {{--{{$seguimientos_visitas::where("visitas_idvisitas",$visitas->idvisitas)->last()->cumple}}--}}
                            </th>
                            <th><a href="{!! url('cms/visitas/') !!}{{"/".$visitas->idvisitas."/edit"}}"><button type="button" class="btn btn-lg btn-common next-step next-button">editar</button></a></th>
                        </tr>
                            <?php }?>
                        @endforeach
                    </tbody>
                </table>

            </section>
@endsection






