@extends('cms\layouts\master')

@section("css")
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="{{asset("assets/cms/css/home.css")}}" media="screen" title="estilo">
    <link rel="stylesheet" href="{{asset("assets/cms/css/proyect.css")}}" media="screen" title="estilo">
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


@endsection

@section('title', 'Visitas')

@section('content')
    {{--<div id="alerta">--}}
    {{--<div class="msg-box">--}}
    {{--<h2>¿Estás seguro?</h2>--}}
    {{--<p>Estas apunto de eliminar este registro</p>--}}
    {{--<div class="acciones">--}}
    {{--<button id="cancelar" class="btn eliminar">Cancelar</button>--}}
    {{--<button class="btn confirm">Aceptar</button>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}


    <section class="pagina">
        <main id="cuerpo">
            @if($visitas->id)
                <h1>Editar Visitas</h1>
            @else
                <h1>Nuevo Visita</h1>
            @endif
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif


            <div class="container">
                {{--<form class="form" method="post" action='{{url("cms/visitas")}}' >--}}
                    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                    <div class="wizard">
                        <div class="wizard-inner">
                            <div class="connecting-line"></div>
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="nav-item">
                                    <a href="#step1"  data-toggle="tab" aria-controls="step1" role="tab" title="Nueva Visita" class="nav-link active">
                                <span class="round-tab">
                                    <i class="fa fa-building"></i>
                                </span>
                                    </a>
                                </li>
                                <li role="presentation" class="nav-item">
                                    <a href="#step2"  data-toggle="tab" aria-controls="step2" role="tab" title="Seguimiento" class="nav-link disabled">
                                <span class="round-tab">
                                    <i class="fa fa-building"></i>
                                </span>
                                    </a>
                                </li>                                                                
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active " role="tabpanel" id="step1">
                                {{--1) Del Contacto--}}
                                <div class="row visitas_form" >
                                    <form action="" id="visitas_form_container" style="width: 100%;">
                                        {{-- 1.1) Datos Personales--}}
                                        <input name="_method" type="hidden" value="PUT">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3>Datos Personales</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">DNI</label>
                                                    <input class="form-control" type="text" name="dni" value="{{$visitas->dni}}" style=" width: 80%; float: left;">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Nombres</label>
                                                    <input class="form-control" type="text" name="nombres" value="{{$visitas->nombres}}" style=" width: 80%; float: left;">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Apellidos</label>
                                                    <input class="form-control" type="text" name="apellidos" value="{{$visitas->apellidos}}" style=" width: 80%; float: left;">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Email</label>
                                                    <input class="form-control" type="text" name="correo" value="{{$visitas->correo}}" style=" width: 80%; float: left;">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Telefono</label>
                                                    <input class="form-control" type="text" name="telefono" value="{{$visitas->telefono}}" style=" width: 80%; float: left;">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Distrito</label>
                                                    <select name="distrito_iddistrito" class="form-control" style=" width: 80%; float: left;">
                                                        <option>Seleccione distrito</option>
                                                        @foreach($distritos as $key => $distrito)
                                                        <option value="{{$distrito->iddistrito}}" @if($visitas->distrito_iddistrito==$distrito->iddistrito) selected @endif >{{$distrito->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- 1.2) D¿Como te enteraste?--}}
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3>¿Como te enteraste?</h3>
                                            </div>
                                            <!-- <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Fecha</label>
                                                    <input class="form-control" type="date" name="fecha" id="fecha_reg" value="" style=" width: 80%; float: left;">
                                                </div>
                                            </div> -->
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Origen</label>
                                                    <select name="origen_idorigen" class="form-control" style=" width: 80%; float: left;">
                                                        <option>Seleccione origen</option>
                                                        @foreach($origens as $key => $origen)
                                                        <option @if($visitas->origen_idorigen==$origen->idorigen) selected @endif  value="{{$origen->idorigen}}">{{$origen->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- 1.3) ¿Que buscas?--}}
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3>¿Que buscas?</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Precio</label>
                                                    <input class="form-control" type="text" name="precio" value="{{$visitas->precio}}" style=" width: 80%; float: left;">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Metros cuadrados</label>
                                                    <input class="form-control" type="text" name="m2" value="{{$visitas->m2}}" style=" width: 80%; float: left;">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Nro de dormitorios</label>
                                                    <input class="form-control" type="text" name="nro_dormitorios" value="{{$visitas->nro_dormitorios}}" style=" width: 80%; float: left;">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Nro de baños</label>
                                                    <input class="form-control" type="text" name="nro_banos" value="{{$visitas->nro_banos}}" style=" width: 80%; float: left;">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <span class="tit-1">Cuarto de servicio</span>
                                                    <input type="checkbox" value="1"@if($visitas->cuarto_servicio==1) checked @endif name="cuarto_servicio" class="form-control-file border" >
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">¿Interesado?</label>
                                                    <select name="interesado" class="form-control" style=" width: 80%; float: left;">
                                                        <option>Seleccione</option>
                                                        <option @if($visitas->interesado==1) selected @endif  value="1" >Si</option>
                                                        <option @if($visitas->interesado==2) selected @endif  value="2">No</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Tipo de Financiamiento</label>
                                                    <select name="financiamiento" class="form-control" style=" width: 80%; float: left;">
                                                        <option>Seleccione financiamiento</option>
                                                        <option @if($visitas->financiamiento==1) selected @endif value="1">Credito Hipotecario</option>
                                                        <option @if($visitas->financiamiento==2) selected @endif value="2">Financiamiento Directo</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        {{-- 1.4) ¿Que te ofrecemos?--}}
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3>¿Que te ofrecemos?</h3>
                                            </div>                                            
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Proyecto</label>
                                                    <select name="proyectos_idproyectos" id="proyectos_idproyectos" class="form-control" style=" width: 80%; float: left;">
                                                        <option>Seleccione proyecto</option>
                                                        @foreach($projects as $key => $project)
                                                        <option @if($visitas->proyectos_idproyectos==$project->idproyectos) selected @endif value="{{$project->idproyectos}}">{{$project->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Departamentos</label>
                                                    <select id="departamentos" name="departamentos_iddepartamentos" class="form-control" style=" width: 80%; float: left;">
                                                        <option value="">Seleccione departamento</option>

                                                    </select>
                                                </div>
                                            </div>                                            
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Estacionamiento</label>
                                                    <select  id="estacionamientos" name="estacionamiento_idestacionamiento" class="form-control" style=" width: 80%; float: left;">
                                                        <option value="">Seleccione estacionamiento</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" style="width: 100%;">
                                                    <label style=" width: 20%; float: left;">Deposito</label>
                                                    <select name="depositos_iddepositos" id="depositos" class="form-control" style=" width: 80%; float: left;">
                                                        <option value="">Seleccione depósito</option>
                                                    </select>
                                                </div>
                                            </div>                                                                                     
                                        </div>
                                        {{-- 1.5) Cotización --}}
                                        <div class="row">
                                            <div class="col-md-5">
                                                <span class="tit-1">Seleccione Cotización</span>
                                                <span class="crear_cotizacion" id="agregar">Agregar</span>
                                            </div>
                                        </div>
                                    </form>
                                </div>



                                
                                
                                <ul class="list-inline text-md-center">
                                    <li><span class="btn btn-lg btn-common visitas_button ">Grabar</span></li>
                                </ul>
                            </div>
                            <div class="tab-pane" role="tabpanel" id="step2">
                                {{--2) Acciones de seguimiento--}}
                                <form action="#" id="form_seguimiento">
                                <h1 class="text-md-center">Acciones de seguimiento</h1>
                                <div class="row"  id="seguimiento">
                                    <div class="container">
                                        <div class="row" tabla="departamento" id="departamento">
                                            <div class="col-md-4" style="display: none">
                                                <div class="fila">
                                                    <span class="tit-1">Fecha de registro</span>
                                                    <input class="campo_llenar" type="date" id="fecha_inicio_seg" name="fecha_inicio" value=""> </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="fila">
                                                    <span class="tit-1">Fecha Limite</span>
                                                    <input class="campo_llenar" type="date" name="fecha_limite" value=""> </div>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="fila">
                                                    <span class="tit-1">Detalle</span>
                                                    <input class="campo_llenar" type="text" name="detalle" value=""> </div>
                                            </div>
                                            <div class="col-md-4" style="display: none">
                                                <div class="fila">
                                                    <span class="tit-1">Vendedor</span>
                                                    <input class="campo_llenar" type="text" id="vendedores_idvendedores_seg" name="vendedores_idvendedores" value=""> </div>
                                            </div>
                                            <div class="col-md-4" style="display: none">
                                                <div class="fila">
                                                    <span class="tit-1">Cumple</span>
                                                    <input class="campo_llenar" type="text" id="cumple_seg" name="cumple" value=""> </div>
                                            </div>
                                            <div class="col-md-3">
                                                <span type="#" class="boton-1 agregar_a_tabla">Agregar</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="row" tabla="seguimiento_t" id="seguimiento_t">
                                    <table class="table table-bordered"  id="seguimiento_table">
                                        <thead>
                                        <tr>
                                            <th scope="col">Fecha de registro</th>
                                            <th scope="col">Fecha Limite</th>
                                            <th scope="col">Detalle</th>
                                            <th scope="col">Vendedor</th>
                                            <th scope="col">Cumple</th>                                            
                                            <th scope="col"></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($seguimientos as $key => $seguimiento)
                                            <tr>
                                                <td>{{$visitas->fecha}}</td>
                                                <td>{{$seguimiento->fecha_inicio}}</td>
                                                <td>{{$seguimiento->detalle}}</td>
                                                <td>{{$seguimiento->vendedores_idvendedores}}</td>
                                                <td>@if($seguimiento->estado==1){{"Si"}} @else{{"No"}}@endif</td>
                                                <td><button type="button" class="btn btn-lg btn-common next-step next-button btn_finalizar">Finalizar</button></td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <ul class="list-inline text-md-center">
                                <li><input type="text" placeholder="Create Visitas" class="btn btn-lg btn-common next-step next-button seguimiento_button   "></li>
                                </ul>
                                </form>
                            </div>                                                        
                            <div class="clearfix"></div>
                        </div>

                    </div>
                {{--</form>--}}
            </div>

            <article class="superior">
                <div class="row">

                </div>

            </article>

        </main>
    </section>

@endsection

@section("js")

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script >
        var site_url="{{url("cms")}}";
        var id_usuario="{{Auth::user()->idusuario}}";
        var name_usuario="{{Auth::user()->nombre}}";
        var  visita_id="{{$visitas->idvisitas}}";
    </script>>
    <script src="{{asset("assets/cms/js/validate.js")}}"></script>
    <script src="{{asset("assets/cms/js/download.js")}}"></script>
    <script src="{{asset("assets/cms/js/visitas_cms_editar_update.js")}}"></script>>
    <script src="{{asset("assets/cms/js/visitas_select.js")}}"></script>>
    <script>
            $("#departamentos").empty();
            $("#estacionamientos").empty();
            $("#depositos").empty();
            var idproyectos=$("#proyectos_idproyectos").val();
            console.log(idproyectos);
            var get_departamentos=$.ajax({
                type: "GET",
                url:  site_url+"/proyect_departamentos",
                data: {id:idproyectos},
                // contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                // processData: false, // NEEDED, DON'T OMIT THIS
            });
            var get_estacionamiento=$.ajax({
                type: "GET",
                url:  site_url+"/proyect_estacionamiento",
                data: {id:idproyectos},
                // contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                // processData: false, // NEEDED, DON'T OMIT THIS
            });
            var get_depositos=$.ajax({
                type: "GET",
                url:  site_url+"/proyect_depositos",
                data:  {id:idproyectos},
                // contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                // processData: false, // NEEDED, DON'T OMIT THIS
            });
            $.when( get_departamentos,get_estacionamiento,get_depositos ).done(function( departamentos,estacionamientos,depositos) {
                departamentos=JSON.parse(JSON.stringify(departamentos));
                departamentos=JSON.parse(departamentos[0]);
                console.log(departamentos)

                estacionamientos=JSON.parse(JSON.stringify(estacionamientos));
                estacionamientos=JSON.parse(estacionamientos[0]);
                console.log(estacionamientos);

                depositos=JSON.parse(JSON.stringify(depositos));
                depositos=JSON.parse(depositos[0]);
                console.log(depositos);


                $("#departamentos").append("<option value=''>Seleccione departamento</option>")
                // <option>Seleccione departamento</option>
                $.each(departamentos, function( index, departamento ) {

                    $("#departamentos").append("<option value='"+departamento.iddepartamentos+"'>"+departamento.departamento+"</option>")
                });


                $("#estacionamientos").append("<option value=''>Seleccione estacionamiento</option>")
                $.each(estacionamientos, function( index, estacionamiento ) {

                    $("#estacionamientos").append("<option value='"+estacionamiento.idestacionamiento+"'>"+estacionamiento.estacionamiento+"</option>")
                });


                $("#depositos").append("<option value=''>Seleccione deposito</option>")
                $.each(depositos, function( index, deposito ) {

                    $("#depositos").append("<option value='"+deposito.iddepositos+"'>"+deposito.deposito+"</option>")
                });

                $("#departamentos").val({{$visitas->departamentos_iddepartamentos}});
                $("#depositos").val({{$visitas->depositos_iddepositos}});
                $("#estacionamientos").val({{$visitas->estacionamiento_idestacionamiento}});

            });




    </script>
@endsection

