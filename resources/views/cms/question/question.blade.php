@extends('cms\layouts\master')

@section("css")
    <link rel="stylesheet" href="{{asset("assets/cms/css/home.css")}}" media="screen" title="estilo">
@endsection

@section('title', 'Servicios')


@section('content')

    <section class="pagina">
        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif


            <article class="superior">

                @if($question->id)
                    <h1>Red social : Detalle</h1>
                @else
                    <h1>Creando Red social</h1>
                @endif



            </article>
            <section class="contenedor">


                @if($question->id)
                    {!! Form::open(['url' => 'cms/questions/'.$question->id,'method' => 'put', 'files' => true] ) !!}

                @else
                    {!! Form::open(['url' => 'cms/questions', 'method' => 'post', 'files' => true]) !!}

                @endif


                <div id="contenido">
                    <section class="principal">
                        <div class="atributo#s">
                            <article class="seo">
                                {{ csrf_field() }}
                                <div class="fila">
                                    <span class="tit-1">Autor de la consulta</span>
                                    <input value="{{ ( $question->name )? $question->name : old('name')  }}" id="name" type="text" placeholder="Nombre del servicio" name="name" readonly>
                                </div>

                                <div class="fila">
                                    <span class="tit-1">Telefono</span>
                                    <input value="{{ ( $question->email )? $question->email : old('email')  }}" id="email" type="text" placeholder="Nombre del servicio" name="color_hex"  readonly>
                                </div>

                            <div class="contenidos">    

                            <span class="tit-1">Consulta</span>
                                {{Form::textarea('question', ( $question->question )? $question->question : old('question'),['readonly' => 'true','class' => "mceNonEditable"])}}

                                {{--{{ ( $question->keywords )? $question->keywords : old('keywords')  }}--}}
                            </div>


                                {{--<div class="fila">--}}
                                    {{--<span class="tit-1">Maque las casillas</span>--}}
                                    {{--<div class="radios">--}}
                                        {{--<label>--}}

                                            {{--{!! Form::checkbox('is_active', '1', ( $question->is_active == 1)? true : false  )!!}--}}
                                            {{--<span>Activo</span>--}}
                                        {{--</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}






                            </article>
                        </div>





                    </section>
                    <section class="lateral">

                    </section>
                </div>
                <div id="acciones">
                    <a class="boton-1 boton-2" href="{!! url('cms/questions/') !!}">Cancelar</a>
                    <button type="submit" class="boton-1 ">Actualizar</button>
                </div>
                </form>
            </section>
        </main>
    </section>




    {{--<div class="">--}}
    {{--{{ Form::label('color_hex', 'Descripcion de canvas', ['class' => 'form-label']) }}--}}
    {{--<label class="">--}}
    {{--<input value="{{ ( $question->color_hex )? $question->color_hex : old('color_hex')  }}" type="text" class="" placeholder="Color hexagesimal" name="color_hex" >--}}
    {{--</label>--}}
    {{--</div>--}}



    {{--<div class="">--}}
    {{--{{ Form::label('canvas_front', 'Canvas principal (HTML) ', ['class' => 'form-label']) }}--}}
    {{--<label class="">--}}
    {{--{{Form::textarea('canvas_front',  ( $question->canvas_front )? $question->canvas_front : old('canvas_front')  )}}--}}
    {{--</label>--}}
    {{--</div>--}}


    {{--<div class="">--}}
    {{--{{ Form::label('activity_description', 'Descripccion del servicio', ['class' => 'form-label']) }}--}}
    {{--<label class="">--}}
    {{--{{Form::textarea('activity_description',  ( $question->activity_description )? $question->activity_description : old('activity_description')  )}}--}}
    {{--</label>--}}
    {{--</div>--}}



    {{--<div class="">--}}
    {{--{{ Form::label('Es_activo', 'Activo', ['class' => 'form-label']) }}--}}
    {{--<label class="">--}}
    {{--{!! Form::checkbox('is_active', '1', ( $question->is_active == 1)? true : false  )!!}--}}
    {{--</label>--}}
    {{--</div>--}}

    {{--<div class="">--}}
    {{--<button type="submit" class="" name="">ENVIAR--}}
    {{--<span class="">></span>--}}
    {{--</button>--}}
    {{--</div>--}}

    {{--<div class="">--}}
    {{--<a   href="{!! url('cms/questions') !!}" class="" name="">Regresar--}}
    {{--<span class="">></span>--}}
    {{--</a>--}}
    {{--</div>--}}

@endsection

@section("cloud_base_js")
    @parent
    <script src="https://www.google.com/recaptcha/api.js" async></script>
    <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
    <script src="http://matthew.wagerfield.com/parallax/deploy/jquery.parallax.js"></script>

@endsection


@section("js")
    <script src="{{asset("assets/cms/js/question_cms.js")}}"></script>
@endsection




