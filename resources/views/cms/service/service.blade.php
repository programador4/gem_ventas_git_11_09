@extends('cms\layouts\master')

@section("css")
    <link rel="stylesheet" href="{{asset("assets/cms/css/home.css")}}" media="screen" title="estilo">
@endsection

@section('title', 'Servicios')

@section('content')

    <section class="pagina">
        <main id="cuerpo">
            @if(session()->has("mensaje"))
                <div id="msg-confirmation">
                    <p class="positivo msg">{{  Session::get('mensaje') }}</p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif
            @if ($errors->any())
                <div id="msg-confirmation">
                    <p class="negativo msg">
                        @foreach ($errors->all() as $error)
                            {{ $error  }}
                            <br>
                        @endforeach

                    </p>
                    <!--<p class="negativo msg">Error para guardar registro</p>-->
                </div>
            @endif


            <article class="superior">

                @if($service->id)
                    <h1>Cliente : Detalle</h1>
                @else
                    <h1>Creando Cliente</h1>
                @endif

            </article>
            <section class="contenedor">

                @if($service->id)
                    {!! Form::open(['url' => 'cms/services/'.$service->id,'method' => 'put', 'files' => true] ) !!}

                @else
                    {!! Form::open(['url' => 'cms/services', 'method' => 'post', 'files' => true]) !!}

                @endif

                <div id="contenido">
                    <section class="principal">
                        <div class="atributo#s">
                            <article class="seo">
                                {{ csrf_field() }}
                                <div class="fila">
                                    <span class="tit-1">Nombre del servicio</span>
                                    <input value="{{ ( $service->name )? $service->name : old('name')  }}" id="name" type="text" placeholder="Nombre del servicio" name="name" required="">
                                </div>
                                <div class="fila">
                                    <span class="tit-1">Subtitulo del servicio</span>
                                    <input value="{{ ( $service->subtitle_service )? $service->subtitle_service : old('name')  }}" id="subtitle_service" type="text" placeholder="Subtitulo del servicio" name="subtitle_service" required="">
                                </div>
                                <div class="fila">
                                    <span class="tit-1">Color del servicio</span>
                                    <input maxlength="10" value="{{ ( $service->color_hex )? $service->color_hex : old('color_hex')  }}" id="name" type="text" placeholder="Color asignado al servicio" name="color_hex" required="">
                                </div>
                                <div class="fila">
                                    {{--<span class="tit-1">Maque las casillas</span>--}}
                                    <div class="radios">
                                        <label>

                                            {!! Form::checkbox('is_active', '1', ( $service->is_active == 1)? true : false  )!!}
                                            <span>Activo</span>
                                        </label>
                                    </div>
                                </div>
                            </article>
                        </div>


                        <div class="contenidos">
                            <span class="tit-1">Descripccion del servicio</span>
                            {{Form::textarea('activity_description', ( $service->activity_description )? $service->activity_description : old('activity_description'))}}

                        </div>
                        <div class="contenidos">
                            <span class="tit-1">Canvas de la página principal</span>
                            {{Form::textarea('canvas_front', ( $service->canvas_front )? $service->canvas_front : old('canvas_front') )}}

                        </div>
                        <div class="contenidos">
                            <span class="tit-1">Canvas de la pagina que hacemos</span>
                            {{Form::textarea('canvas_description', ( $service->canvas_description )? $service->canvas_description : old('canvas_description'))}}

                        </div>
                    </section>
                    <section class="lateral">

                    </section>
                </div>
                <div id="acciones">
                    <a class="boton-1 boton-2" href="{!! url('cms/projects/') !!}">Cancelar</a>
                    <button type="submit" class="boton-1 ">Actualizar</button>
                </div>
                </form>
            </section>
        </main>
    </section>


@endsection



@section("js")
    @parent
    <script src="{{asset("assets/cms/js/services_cms.js")}}"></script>
@endsection





