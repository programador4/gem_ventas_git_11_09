<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Distrito extends Model {

    protected $table = 'distrito';
    protected $primaryKey = 'iddistrito';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['created_at','updated_at','deleted_at','nombre','estado'];

}
