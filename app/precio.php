<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Precio extends Model {
    /**
     * The table associated with the model.
     * s
     * @var string
     */
    use SoftDeletes;

    protected $table = 'precios';
    protected $primaryKey = 'idprecios';
    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = [
        'created_at',
        'updated_at',
        'deleted_at',
        'factor_area_libre',
        'factor_desc_preventa',
        'factor_incr_entrega',
        'factor_vista_exterior',
        'moneda',
        'valor_deposito',
        'promocion_idpromocion',
        'proyectos_idproyectos',
        'descuento',
        'fecha_inicio',
        'fecha_fin'];


}

