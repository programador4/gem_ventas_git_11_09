<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promocion extends Model {
	use SoftDeletes;

    protected $table = 'promocion';
    protected $primaryKey = 'idpromocion';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = [
		'created_at',
		'updated_at',
		'deleted_at',
		'nombre',
		'descuento',
		'fecha_inicio',
		'fecha_fin', 
		'estado',
	];
}
