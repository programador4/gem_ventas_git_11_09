<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tipodepartamento extends Model {
	use SoftDeletes;
	//
	protected $table = 'tipo_departamento';
    protected $primaryKey = 'idtipo_departamento';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = [
		'created_at',
		'updated_at',
		'deleted_at',
		'nombre',
		'inicio_area_maxima',
		'fin_area_maxima',
		'area_total',
		'nro_habitacion',
		'nro_banos',
		'vista_parque',
		'url_plano',
		'estado',
        'id_proyecto',
        'valor_venta'
	];
}
