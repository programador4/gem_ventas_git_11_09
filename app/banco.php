<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class banco extends Model {
    protected $table = 'banco';
    protected $primaryKey = 'idbanco';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['created_at','updated_at','deleted_at','nombre','descripccion','estado'];
}
