<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Vendedores extends Model {
    use SoftDeletes;

    protected $table = 'vendedores';
    protected $primaryKey = 'idvendedores';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = [
        'created_at',
        'updated_at',
        'deleted_at',
        'porcentaje_comision',
        'estado',
        'usuario_idusuario',
        'proyectos_idproyectos',
    ];

}
