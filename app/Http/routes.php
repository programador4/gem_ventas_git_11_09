<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');

//Route::controllers([
//	'auth' => 'Login\LoginController@login',
//	'password' => 'Login\LoginController@login',
//]);

Route::get('/auth/login', 'Login\LoginController@login');



Route::get('/', 'Web\Webcontroller@home');
Route::get('/quienes-somos', 'Web\Webcontroller@getQuienesSomos');
Route::get('/que-hacemos', 'Web\WebController@getQueHacemos');
//Route::get('/que-hacemos/{id?}', 'Web\WebController@getQueHacemos');
Route::get('/contacto', 'Web\WebController@getContacto');
Route::post('/contacto', 'Web\WebController@postContacto');
Route::get('/blog', 'Web\WebController@getBlog');
Route::get('/blog/{slug}/', 'Web\WebController@getArticulesBySlug');
Route::get('/blog/tag', 'Web\WebController@getBlog');
Route::get('/blog/tag/{slug}/', 'Web\WebController@getArticulesByTag');
Route::get('/blog/category', 'Web\WebController@getBlog');
Route::get('/blog/category/{slug}/', 'Web\WebController@getArticulesByCategory');


/* Administrador */

Route::get('cms/login', 'Login\LoginController@create');
Route::post('cms/login', 'Login\LoginController@login');
Route::get('cms/logout', 'Login\LoginController@logout');
// 'middleware' => ['auth']
Route::group(
    [ 'prefix' => 'cms/','middleware' => ['auth']],
    function() {
        //Route::get('','Cms\DashboardController@index');
        //Route::get('dashboard','Cms\DashboardController@index');
        Route::get('/','Cms\DashboardController@index');
        Route::resource('articles', 'Cms\ArticleController');
        Route::resource('categories', 'Cms\CategoryController');

        Route::resource('precios'         , 'Cms\PrecioController');

        Route::get('proyect_departamentos', 'Cms\ProjectController@proyect_departamentos');
        Route::get('proyect_estacionamiento', 'Cms\ProjectController@proyect_estacionamiento');
        Route::get('proyect_depositos', 'Cms\ProjectController@proyect_depositos');

        Route::resource('projectos'       , 'Cms\ProjectController');
        /**/

        /**/

        Route::resource('tipodepartamento'       , 'Cms\TipodepartamentoController');
        Route::resource('get_select'       , 'Cms\TipodepartamentoController');

        Route::resource('departamentos'       , 'Cms\DepartamentoController');
        Route::post('calcular_precio_venta'       , 'Cms\ProjectController@calcular_precio_venta');
        Route::post('calcular_precio_venta_estacionamiento'       , 'Cms\ProjectController@calcular_precio_venta_estacionamiento');
        Route::resource('dpto_precios'    , 'Cms\Dpto_precioController');
        Route::resource('estacio_precios'  , 'Cms\Estacio_precioController');
        Route::resource('estacionamientos'  , 'Cms\EstacionamientoController');
        Route::resource('depositos'  , 'Cms\DepositoController');
        Route::resource('documentos'  , 'Cms\DocumentoController');
        Route::resource('vendedores'  , 'Cms\VendedoresController');
        Route::resource('promocion'  , 'Cms\PromocionController');
        Route::resource('precios'  , 'Cms\PreciosController');
        
        Route::resource('visitas', 'Cms\VisitasController');
        Route::post('crear_cotizacion', 'Cms\VisitasController@store_cotizacion');
        Route::get('ver_cotizacion', 'Cms\VisitasController@store_cotizacion_from_html');

//        store_cotizacion_from_html
        Route::put('crear_cotizacion', 'Cms\VisitasController@store_cotizacion');
        Route::resource('seguimiento', 'Cms\SeguimientoController');

        Route::post('visitas_wizard_create', 'Cms\VisitasController@create_visitas');

        Route::post('/create_visitas',function (){
            if(Request::ajax()){
                $visitas = new \App\Http\Controllers\Cms\VisitasController();
                $visitas->store();
            }
        });

        Route::post('projects_wizard_create', 'Cms\ProjectController@create_project');
        Route::post('/create_project',function (){
            if(Request::ajax()){
                $project = new \App\Http\Controllers\Cms\ProjectController();
                $project->store();
            }
        });
//        Route::post('validate_proyecto'       , 'Cms\ProjectController@validate_proyecto');

        Route::resource('clients', 'Cms\ClientController');
        Route::resource('questions', 'Cms\QuestionController');
        Route::resource('services', 'Cms\ServiceController');
        Route::resource('tags', 'Cms\TagController');
        Route::resource('testimonies', 'Cms\TestimonyController');
        Route::resource('socialnetworks', 'Cms\SocialNetworkController');
        Route::resource('tags', 'Cms\TagController');
        Route::resource('users', 'Cms\UserController');
        Route::resource('usertypes' ,'Cms\UsertypeController');
    });


//Clear Cache facade value:
Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    return '<h1>Cache facade value cleared</h1>';
});

//Reoptimized class loader:
Route::get('/optimize', function() {
    $exitCode = Artisan::call('optimize');
    return '<h1>Reoptimized class loader</h1>';
});

//Route cache:
Route::get('/route-cache', function() {
    $exitCode = Artisan::call('route:cache');
    return '<h1>Routes cached</h1>';
});

//Clear Route cache:
Route::get('/route-clear', function() {
    $exitCode = Artisan::call('route:clear');
    return '<h1>Route cache cleared</h1>';
});

//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});

//Clear Config cache:
Route::get('/config-cache', function() {
    $exitCode = Artisan::call('config:cache');
    return '<h1>Clear Config cleared</h1>';
});
