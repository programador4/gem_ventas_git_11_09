<?php

namespace App\Http\Middleware\Cms;

use Closure;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ProjectMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $requestData=$request->all();



        if ($requestData['front_image_t']!=""){
            $rule_img1="";
        }else{
            $rule_img1="required|mimes:jpeg,gif,png|dimensions:max_width=965,max_height=375";
        }

        if ($requestData['back_image_t']!=""){
            $rule_img2="";
        }else{
            $rule_img2="required|mimes:jpeg,gif,png|dimensions:min_width=817,min_height=402";
        }




        $rules = array(
            'name'        => 'required',
            'web'         => 'required|url',
            'keywords'    => 'required',
            'front_image' => $rule_img1,
            'back_image'  => $rule_img2,
            'id_client'   => 'required|min:1',
        );
//
//        var_dump($rules);
//
//        exit();


        $messages = [
            'name.required'          => '-El campo nombre es requerido',
            'web.required'           => '-El campo web  es requerido',
            'web.url'                => '-El campo web  debe ser una url valida',
            'keywords.required'      => '-El campo palabras clave es requerido',
            'front_image.required'   => '-El campo imagen principal es requerido',
            'front_image.dimensions' => '-La imagen principal debe ser de 961px x 372px',
            'front_image.mimes'      => '-El campo imagen principal debe ser una imagen',
            'id_client.required'     => '-El campo cliente es requerido',
            'id_client.min'          => '-El campo cliente es requerido',
            'back_image.required'    => '-El campo imagen de fondo es requerido',
            'back_image.dimensions'  => '-La imagen de fondo debe minimo de ser de 817px x 402px',
            'back_image.mimes'       => '-El campo imagen de fondo debe ser una imagen',

        ];


        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()){

            $validator->validate();

            Redirect::back();

        }else{

            return $next($request);
        }



    }
}
