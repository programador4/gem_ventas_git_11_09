<?php

namespace App\Http\Middleware\Cms;

use Closure;

use Illuminate\Support\Facades\Validator;

class QuestionsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $requestData=$request->all();

        $rules = array(
            'name'     =>'required|alpha',
            'phone'    =>'required|integer|min:9',
            'email'    =>'required|email',
            'question' =>'required'
        );

        $messages = [
            'name.alpha'    => '-Ingrese solo letras en el nombre',
            'phone.integer' => '-Ingrese solo numeros en el telefono',
            'phone.min'     => '-Ingrese un minimo de 9 digitos al telefono',
            'required'      => '-El campo :attribute  es requerido'
        ];


        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()){

            $validator->validate();

            return Redirect::to(URL::previous() . "#contacto");

        }else{

            return $next($request);
        }



    }
}
