<?php

namespace App\Http\Middleware\Cms;

use Closure;
use Illuminate\Support\Facades\Validator;

class TestimonyMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $requestData=$request->all();

        $rules = array(
            'id_client'			 => 'required',
            'author'			 => 'required',
            'position'			 => 'required',
            'testimony'			 => 'required',
            'id_social_network'	 => 'required',
            'url_social_network' => 'required|url',

        );

        $messages = [
        'id_client.required'		  => 'Seleccione un cliente ',
        'author.required'			  => 'El campo autor es requerido ',
        'position.required'			  => 'El campo posicion es requerido ',
        'testimony.required'	      => 'El campo testimonio es requerido ',
        'id_social_network.required'  => 'Seleccione una red social ',
        'url_social_network.required' => 'El campo url de red social es requerido ',
        'url_social_network.url'      => 'Ingrese una url valida en la url de la red social',

        ];


        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()){

            $validator->validate();

            Redirect::back();

        }else{

            return $next($request);
        }



    }
}
