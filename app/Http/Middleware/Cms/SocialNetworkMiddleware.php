<?php

namespace App\Http\Middleware\Cms;

use Closure;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class SocialNetworkMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $requestData=$request->all();

        if ($requestData['logo_t']!=""){
                $rule_img='';
        }else{
            $rule_img='required|mimes:jpeg,gif,png|dimensions:max_width=56,max_height=60';
        }


        $rules = array(

            'name'      => 'required',
            'color_hex' => 'required',
            'logo'      => $rule_img

        );



        $messages = [
            'color_hex.requerido'  => '-El campo color es requerido',
            'name.requerido'       => '-El campo nombre es requerido',
            'logo.required'        => '-El campo logo es requerido',
            'logo.dimensions'      => '-La logo debe ser inferior a 56px x 60px'

        ];


        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()){

            $validator->validate();

            Redirect::back();

        }else{

            return $next($request);
        }



    }
}
