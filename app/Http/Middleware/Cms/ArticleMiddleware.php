<?php

namespace App\Http\Middleware\Cms;

use Closure;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ArticleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $requestData=$request->all();

        if ($requestData['front_image_t']!=""){
            $rule_img="";
        }else{
            $rule_img='';
        }
//'required|mimes:jpeg,gif,png|dimensions:max_width=653,max_height=363'
        /*$slug=strtolower($slug);562*315*/



        $rules = array(

//          'title'        => 'required|regex:/^[\pL\s\-]+$/u',
            'title'        => 'required',
            'front_image'  => $rule_img,
            'description'  => 'required',
            'content'      => 'required',
           // 'keywords'     => 'required',
        );

        //653*363
  // 'metatags_seo' => 'required'
        $messages = [


            'title.required'               => '-El campo titulo es requerido',
            'title.regex'                  => '-El campo titulo debe tener solo espacios y letras',
            'description.required'         => '-El campo descripccion es requerido',
            'front_image.required'         => '-El campo imagen principal es requerido',
            'front_image.dimensions'       => '-La imagen principal debe ser de 653px x 363px',
            'content.required'             => '-El campo contenido es requerido',
            //'keywords.required'            => '-El campo palabras clave es requerido',
           // 'metatags_seo'=> '-El campo metatags-seo es requerido'
 

        ];


        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()){

            $validator->validate();

            Redirect::back();

        }else{

            return $next($request);
        }



    }
}
