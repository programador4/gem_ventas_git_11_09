<?php

namespace App\Http\Middleware\Cms;

use Closure;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ClientMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $requestData=$request->all();

        if ($requestData['logo_t']!=""){
            $rule_img="";
        }else{
            $rule_img="required|mimes:jpeg,gif,png|dimensions:max_width=103,max_height=103";
        }




        $rules = array(
            'name' => 'required',
            'web'  => 'required|url',
            'logo' => $rule_img
        );


        $messages = [
            'name.required'        => '-El campo nombre es requerido',
            'web.required'         => '-El campo web  es requerido',
            'web.url'              => '-El campo web debe ser una url valida',
            'logo.required'        => '-El campo logo es requerido',
            'logo.dimensions'      => '-El  logo debe ser de a 103px x 103px',
            'logo.mimes'           => '-El campo imagen principal debe ser una imagen',

        ];


        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()){

            $validator->validate();

            Redirect::back();

        }else{

            return $next($request);
        }



    }
}
