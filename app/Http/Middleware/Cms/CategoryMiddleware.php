<?php

namespace App\Http\Middleware\Cms;

use Closure;

use Illuminate\Support\Facades\Validator;

class CategoryMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $requestData=$request->all();

        $rules = array(

            'name'        => 'required|regex:/^[\pL\s\-]+$/u'
        
        );


        $messages = [
            'name.required'               => '-El campo nombre de categoria es requerido',

            'name.regex'                  => '-El campo nombre de categoria debe tener solo espacios y letras'
        ];


        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()){

            $validator->validate();

            Redirect::back();

        }else{

            return $next($request);
        }



    }
}
