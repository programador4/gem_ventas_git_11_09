<?php

namespace App\Http\Middleware\Cms;
use App\Http\Controllers\Controller;
use Closure;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
class UserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {



        $requestData=$request->all();





            $rules = array(
                'apellido'     => 'required',
                'password'     => 'required|min:12',
                'nombre'     => 'required',
                'tipo_usuario_idtipo_usuario' => 'required',
                'correo'        => 'required|email'

        );



        $messages = [
            'nombre.required'     => '-El campo nombre es requerido',
            'nombre.required'     => '-El campo apellido es requerido',
            'apellido.required'     => '-El campo apellido es requerido',
            'password.required'     => '-El campo contraseña es requerido',
            'email.required'        => '-El campo correo es requerido',
            'email.email'           => '-El campo correo debe ser un email valido',
            'id_user_type.required' => '-Seleccione el campo tipo de usuario',
            'password.min'          => '-El campo contraseña debe tener un minimo de 12 caracteres'
        ];


        $validator = Validator::make($requestData, $rules, $messages);
        $this->validate($request, [
            'email' => 'required|email',
        ]);
        if ($validator->fails()){

            $validator->validate();

            Redirect::back();

        }else{

            return $next($request);
        }

    }
}
