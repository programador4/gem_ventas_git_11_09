<?php

namespace App\Http\Middleware\Cms;

use Closure;

use Illuminate\Support\Facades\Validator;

class TagMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $requestData=$request->all();

        $rules = array(

            'name'        => 'required'
        
        );

        $messages = [
            'name.required'               => '-El campo nombre del Tag es requerido',
      ];

        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()){

            $validator->validate();

            Redirect::back();

        }else{

            return $next($request);
        }



    }
}
