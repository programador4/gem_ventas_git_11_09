<?php

namespace App\Http\Middleware\Cms;

use Closure;
use Illuminate\Support\Facades\Validator;

class ServiceMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $requestData=$request->all();

        $rules = array(
            'canvas_front'         => 'required',
            'name'                 => 'required',//|min:40
            'subtitle_service'     => 'required',
            'canvas_description'   => 'required',
            'activity_description' => 'required',
            'color_hex'            => 'required|min:7'
// |
        );

        $messages = [

        'canvas_front.required'        => '-El campo canvas frontal es requerido',
        'name.min'                      => '-El campo nombre debe tener al menos 40 caracteres',
        'name.required'                => '-El campo nombre  es requerido',
        'subtitle_service.required'    => '-El campo subtitulo del servicio es requerido',
        'canvas_description.required'  => '-El campo descripccion principal debe ser una imagen',
        'activity_description.required' => '-El campo descripccion de servicio debe ser una imagen',
        'activity_description.min'      => '-El campo descripccion de tner un minimo de 40 caracteres',
        'color_hex.required'            => '-El campo color hexagesimal es requerido',
        'color_hex.min'                 => '-El campo color hexagesimal debe tener 7 caracteres'



        ];


        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()){

            $validator->validate();

            Redirect::back();

        }else{

            return $next($request);
        }



    }
}
