<?php

namespace App\Http\Middleware\Cms;

use Closure;
use Illuminate\Support\Facades\Validator;

class UserTypeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $requestData=$request->all();

        $rules = array(
            'type' => 'required|max:120'
        );

        $messages = [
            'type.required' => '-El campo nombre de tipo de usuario es requerido',

        ];


        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()){

            $validator->validate();

            Redirect::back();

        }else{

            return $next($request);
        }

    }
}
