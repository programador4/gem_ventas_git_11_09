<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
            'apellido'     => 'required',
            'password'     => 'required|min:12',
            'nombre'     => 'required',
            'tipo_usuario_idtipo_usuario' => 'required',
            'correo'        => 'required|email'
		];
	}

}
