<?php namespace App\Http\Controllers\Cms;

use App\Dpto_precio;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class Dpto_precioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $dpto_precio)
	{   $dpto_precios=$dpto_precio->all();
        $idprecio_name="precios_idprecios";
        $idprecio=$dpto_precios["idprecios"];
        $rows=$dpto_precios["row"];

         foreach ($rows as $key => $dpto_precio ) {
             if ($dpto_precio["id"] == "") {
//                 dd($dpto_precio);
                 unset($dpto_precio["id"]);
                 $dpto_precio[$idprecio_name]=$idprecio;
//                 dd($dpto_precio);
                Dpto_precio::create($dpto_precio);
                 echo "dpto_created";
                 //Si esta vacio crea
             }else{
                 $dpto_precio["$idprecio_name"]=$idprecio;
                 Dpto_precio::update($dpto_precio);
                //Si no esta vacio actualiza
             }
         }

	}






	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
