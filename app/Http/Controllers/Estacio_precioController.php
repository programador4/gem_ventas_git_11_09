<?php namespace App\Http\Controllers\Cms;

use App\Estacio_precio;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class Estacio_precioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $estacio_precio)
	{
        $estacio_precios=$estacio_precio->all();
        echo($estacio_precios);
        $idprecio_name="precios_idprecios";
        $idprecio=$estacio_precios["idprecios"];
        $rows=$estacio_precios["row"];

        foreach ($rows as $key => $estacio_precio ) {

            if ($estacio_precio["id"] == "") {

                unset($estacio_precio["id"]);
                $estacio_precio[$idprecio_name]=$idprecio;

                Estacio_precio::create($estacio_precio);

                echo "est_created 2";
                //Si esta vacio crea
            }else{
                $estacio_precio["$idprecio_name"]=$idprecio;
                $model=Estacio_precio::find($estacio_precio["id"] );
                $model->update($estacio_precio);
                //Si no esta vacio actualiza
            }
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
