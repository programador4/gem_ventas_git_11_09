<?php 

namespace App\Http\Controllers\Cms;

use App\Departamento;
use App\Models\Project;
use App\Distrito;
use App\Origen;
use App\Piso;
use App\Seguimiento;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Visitas;

use Illuminate\Http\Request;

class VisitasController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(request $request)
        { $visitas_r=$request;
            $visita = new Visitas();
            if($visitas_r->project){
                $visita->where('proyectos_idproyectos', $visitas_r->project);
            }
            if($visitas_r->fecha_inicio){
                $visita->where('financiamiento', $visitas_r->financiamiento);
            }
            if($visitas_r->fecha_fin){
                $visita->where('financiamiento', '<', $visitas_r->financiamiento);
            }
            if($visitas_r->financiamiento){
                $visita->where('financiamiento', '>', $visitas_r->financiamiento);
            }
            if($request->has('_token')) {

            } else {
                $visita=$visita::all();
            }

            $projects =Project::all();
            $departamentos_visitas=new Departamento();
            $projects_visitas=new Project;
            $seguimientos_visitas=new Seguimiento;

            $pisos=Piso::all();
            $seguimientos=Seguimiento::all();;

            $data = compact('visita', 'pisos','seguimientos', 'projects','projects_visitas','seguimientos_visitas','departamentos_visitas','visitas_r');

            return view('cms/visitas/index', $data);
        }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$visitas=  new Visitas;

		$projects = Project::all();
		$distritos=Distrito::all();
		$origens=Origen::all();
		$pisos=Piso::all();
		$flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
		$flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
		$flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
		$data = compact('visitas', 'distritos', 'origens','pisos','flg_departamentos','flg_estacionmientos','flg_depositos','projects');
		return view('cms/visitas/visitas.php', $data);
	}
    public function store_cotizacion(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('template.docx');

//        $section = $phpWord->addSection();
//        $text = $section->addText($request->get('dni'));
//        $text = $section->addText($request->get('nombres'));
//        $text = $section->addText($request->get('apellidos'));
//        $text = $section->addText($request->get('correo'));
//        $text = $section->addText($request->get('telefono'));
//        $text = $section->addText($request->get('fecha'));
//        $text = $section->addText($request->get('interesado'));
//        $text = $section->addText($request->get('financiamiento'));
//        $text = $section->addText($request->get('cotizacion'));
//        $text = $section->addText($request->get('distrito_iddistrito'));
//        $text = $section->addText($request->get('proyectos_idproyectos'));
//        $text = $section->addText($request->get('departamentos_iddepartamentos'));
//        $text = $section->addText($request->get('estacionamiento_idestacionamiento'));
//        $text = $section->addText($request->get('depositos_iddepositos'));
//        $text = $section->addText($request->get('origen_idorigen'));
//        $text = $section->addText($request->get('nro_banos'));
//        $text = $section->addText($request->get('nro_dormitorios'));
//        $text = $section->addText($request->get('cuarto_servicio'));
//        $text = $section->addText($request->get('m2'));
        $templateProcessor->setValue('dni',$request->get('dni'));
        $templateProcessor->setValue('nombres',$request->get('nombres'));
        $templateProcessor->setValue('apellidos',$request->get('apellidos'));
        $templateProcessor->setValue('correo',$request->get('correo'));
        $templateProcessor->setValue('telefono',$request->get('telefono'));
        $templateProcessor->setValue('fecha',$request->get('fecha'));
        $templateProcessor->setValue('interesado',$request->get('interesado'));
        $templateProcessor->setValue('financiamiento',$request->get('financiamiento'));
        $templateProcessor->setValue('cotizacion',$request->get('cotizacion'));
        $templateProcessor->setValue('distrito_iddistrito',$request->get('distrito_iddistrito'));
        $templateProcessor->setValue('proyectos_idproyectos',$request->get('proyectos_idproyectos'));
        $templateProcessor->setValue('departamentos_iddepartamentos',$request->get('departamentos_iddepartamentos'));
        $templateProcessor->setValue('estacionamiento_idestacionamiento',$request->get('estacionamiento_idestacionamiento'));
        $templateProcessor->setValue('depositos_iddepositos',$request->get('depositos_iddepositos'));
        $templateProcessor->setValue('origen_idorigen',$request->get('origen_idorigen'));
        $templateProcessor->setValue('nro_banos',$request->get('nro_banos'));
        $templateProcessor->setValue('nro_dormitorios',$request->get('nro_dormitorios'));
        $templateProcessor->setValue('cuarto_servicio',$request->get('cuarto_servicio'));
        $templateProcessor->setValue('m2',$request->get('m2'));

        $time = round(microtime(true) * 1000);
//        $text = $section->addText($request->get('number'),array('name'=>'Arial','size' => 20,'bold' => true));
//        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $templateProcessor->saveAs('cotizacion/cotizacion_'. $time.'.docx');
        echo 'cotizacion/cotizacion_'. $time.'.docx';
//        return response()->download(public_path('cotizacion/cotizacion_'. $time.'.docx'));
    }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $visita)
	{
		$visita=Visitas::create($visita->all());
		echo json_encode($visita->idvisitas);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $visitas=  new Visitas;
        $visitas=$visitas::find($id);
        $projects = Project::all();
        $distritos=Distrito::all();
        $origens=Origen::all();
        $pisos=Piso::all();

        $flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
        $data = compact('visitas', 'distritos', 'origens','pisos','flg_departamentos','flg_estacionmientos','flg_depositos','projects');

		return view('cms/visitas/visitas_editar', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $visita,$id)
	{
        $visitas=  new Visitas;

		$visitas=$visitas->find($id);

		$visitas->update($visita->all());
		echo $id;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$this-> ->delete($visitantes->id);
		
		Session::flash("mensaje", "Visitante eliminado");

		return redirect('cms/visitas/');
	}

}

