<?php

namespace App\Http\Controllers\Cms;
use App\banco;
use App\Distrito;
use App\Documento;
//Models
use App\Models\Project;


use App\Models\User;
use App\Piso;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Storage;

use Illuminate\Support\Facades\Session;

class DocumentoController extends Controller
{
//    private $r_p;
//
//    private $c_r;
//    function __construct( ProjectRepository $r_p , ClientRepository $c_r)
//    {
//
//        $this->middleware(ProjectMiddleware::class, ['only' => [ 'store', 'update']]);
//        $this->r_p =  $r_p;
//        $this->c_r =  $c_r;
//
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        $distrito=new Distrito;

        $data = compact('projects','distrito');

        return view('cms/project/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyectos= new Project;


        $distritos=Distrito::all();
        $bancos=banco::all();
        $usuarios_v=User::where('tipo_usuario_idtipo_usuario','=', '2')->get();
        $pisos=Piso::all();
        $flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
        $data = compact('proyectos','distritos','bancos','usuarios_v','pisos','flg_departamentos','flg_estacionmientos','flg_depositos');
        return view('cms/project/project', $data);



    }
    public function create_project(Request $mode)
    {
        $this->store();
        //Para crear el proyecto debemos crear

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $departamentos)
    {
        $departamentos=$departamentos->all();
        $idprecio_name="proyectos_idproyectos";
        $idprecio=$departamentos["proyectos_idproyectos"];
        $rows=$departamentos["row"];

        foreach ($rows as $key => $departamento ) {

            if ($departamento["id"] == "") {

                unset($departamento["id"]);

                $departamento[$idprecio_name]=$idprecio;

                $departamento["name_url_plano"]=$departamento["url_plano"]->getClientOriginalName();

//                dd($departamento["url_plano"]->getClientOriginalName());
//                dd($departamento["url_plano"]);
                //Hasheamos el nombre y guardamos

//                Storage::disk('local')->put('file.txt', 'Contents');
                Storage::disk('local')->put("/proyect_departamento_planos/".$departamento["name_url_plano"],$departamento["url_plano"]);


//                Le colocamos el nombre con la fecha a url_plano_name
                $departamento["url_plano"]=$departamento["url_plano"]->getClientOriginalName();
                $filename = pathinfo($departamento["url_plano"], PATHINFO_FILENAME);
                $extension = pathinfo($departamento["url_plano"], PATHINFO_EXTENSION);
                $departamento["url_plano"]= $filename ."_". date("Y-m-d_H:i:s")."." . $extension; // 'qwe jpg'

                //Le colocamos el nombre original a url plano

                Departamento::create($departamento);

                //Si esta vacio crea
            }else{
                $departamento["$idprecio_name"]=$idprecio;
                Departamento::update($departamento);
                //Si no esta vacio actualiza
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *f
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $proyectos)
    {


//        $clients = $this->c_r->list_select('name','id');
        $distritos=Distrito::all();
        $bancos=banco::all();
        $usuarios_v=User::where('tipo_usuario_idtipo_usuario','=', '2')->get();
        $pisos=Piso::all();
        $flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
        $data = compact('proyectos','distritos','bancos','usuarios_v','pisos','flg_departamentos','flg_estacionmientos','flg_depositos');
        return view('cms/project/project', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $model=Documento::find($id);
        $model->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $this->r_p->delete($project->id);

        Session::flash("mensaje", "Projecto eliminado");

        return redirect('cms/projects/');
    }


    public function validate_project()
    {
//        ProjectRequest $project
        var_dump("project");
    }
    /**
     * Return a sluged version of a wird
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */


}
