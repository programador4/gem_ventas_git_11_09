<?php

namespace App\Http\Controllers\Web;

use App\Http\Middleware\Cms\QuestionsMiddleware;
use App\Repositories\ArticleRepository;
use App\Repositories\CategoryRepository;
use App\Repositories\ProjectRepository;
use App\Repositories\QuestionRepository;
use App\Repositories\ServiceRepository;
use App\Repositories\TagRepository;
use App\Repositories\TestimonyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use App\Models\Project;
use App\Models\Services;
use Validator;

class Webcontroller extends Controller
{

    private $r_a;

    private $r_q;

    private $r_p;

    private $r_s;

    private $r_t;

    private $r_tag;

    private $r_c;


    public function __construct(
        QuestionRepository $r_q,
        ProjectRepository $r_p,
        ServiceRepository $r_s,
        TestimonyRepository $r_t,
        ArticleRepository $r_a,
        CategoryRepository $r_c,
        TagRepository $r_tag)
    {
        $this->middleware(QuestionsMiddleware::class, ['only' => [ 'postContacto']]);
        $this->r_q   = $r_q;
        $this->r_p   = $r_p;
        $this->r_s   = $r_s;
        $this->r_t   = $r_t;
        $this->r_tag = $r_tag;
        $this->r_a   = $r_a;
        $this->r_c   = $r_c;
    }

    public function home()
    {

        $services    = $this->r_s->getAllactived();
        $projects    = $this->r_p->getAllactived();
        $testimonies = $this->r_t->getAllactived();
        $data = compact('projects','services','testimonies');

        return view('paginas/home',$data);
    }

    public function getQuienesSomos(){

        return view('paginas/quienes-somos');

    }

    public function getQueHacemos($id = null){

        $services = $this->r_s->getAllactived();

        if($id==null){

            $id=$this->r_s->getFirstActived()->id;

        }

        $data = compact('services','id');

        return view('paginas/que-hacemos',$data);

    }

    public function getContacto(){
        return view('paginas/contacto');
    }

    public function postContacto(Request $request){

        $data = $request->all();

        $last_project =  $this->r_q->save($data);

        $request->session()->flash('mensaje','!Muchas gracias¡  Pronto nos pondremos en contacto contigo');

        return Redirect::to(URL::previous()."#contacto");

    }
    public function getBlog(){

        $slug="";

        $articles      = $this->r_a->getAllActivedPaginated(3);
        $categories    = $this->r_c->getAllActived();
        $tags          = $this->r_tag->getAllActived()->get();
        $last_articles = $this->r_a->getAllActived()->take(3)->get();

        $data = compact("articles","slug","categories","last_articles","tags");

        return view('paginas/blog',$data);
    }

    public function getArticle($slug){

        $article=$this->r_a->find_or_fail_slug($slug);

        $data=compact('article', $slug);

        return view('paginas/blog-det',$data);
    }


    public function getArticlesByCategory($slug){

        $category = $this->r_c->getBySlug($slug);
        $slug     = $slug."/";
        $articles = $category->articles()->get();
        $data     = compact('articles','slug');

        return view('paginas/blog',$data);
    }

    public function getArticulesBySlug($slug){

        $last_articles = $this->r_a->getAllActived()->take(3)->get();
        $categories=$this->r_c->getAllActived();
        $tags=$this->r_tag->getAllActived()->get();
        $article=$this->r_a->getBySlug($slug);
        //si es un blog redirige a paginas /blog

        $tags = $article->tags->pluck('name', 'id');
        $tag_keys=array();

        foreach ($tags as $key => $tag){
            array_push($tag_keys, $tag);
        }

        $tag_keys = implode (",", $tag_keys);
        $tags=$this->r_tag->getAllActived()->get();
        $data=compact('article','slug','categories','tag_keys','last_articles','tags');

        return view('paginas/blog-det',$data);


        return $slug;
    }

    public function getArticulesByTag($slug){

        //comprobamos si es una categoria o un blog

        $last_articles = $this->r_a->getAllActived()->take(3)->get();
        $categories=$this->r_c->getAllActived();
        $tags=$this->r_tag->getAllActived()->get();
        $article=$this->r_a->getBySlug($slug);

        $tag=$this->r_tag->getBySlugOrFail($slug);

        $slug=$slug."/";

        $articles=$tag->articles()->get();

        $data=compact('articles','slug','categories','last_articles','tags');

        return view('paginas/blog',$data);
        return $slug;
    }

    public function getArticulesByCategory($slug){
        //comprobamos si es una categoria o un blog

        $last_articles = $this->r_a->getAllActived()->take(3)->get();
        $categories=$this->r_c->getAllActived();
        $tags=$this->r_tag->getAllActived()->get();
        $article=$this->r_a->getBySlug($slug);

        $category=$this->r_c->getBySlugOrFail($slug);

        $slug=$slug."/";

        $articles=$category->articles()->get();

        $data=compact('articles','slug','categories','last_articles','tags');

        return view('paginas/blog',$data);
        return $slug;
    }




    public function getArticles_or_Categories($slug){

        //comprobamos si es una categoria o un blog

        $last_articles = $this->r_a->getAllActived()->take(3)->get();
        $categories=$this->r_c->getAllActived();
        $tags=$this->r_tag->getAllActived()->get();
        $article=$this->r_a->getBySlug($slug);

        if ($article){

            //si es un blog redirige a paginas /blog


            $tags = $article->tags->pluck('name', 'id');
            $tag_keys=array();
//            dd($article->tags->pluck('name', 'id'));

            foreach ($tags as $key => $tag){
                array_push($tag_keys, $tag);
            }

            $tag_keys = implode (",", $tag_keys);
            $data=compact('article','slug','categories','tag_keys','last_articles','tags');

            return view('paginas/blog-det',$data);

        }else{
            //if is categoria

            $category=$this->r_c->getBySlugOrFail($slug);

            $slug=$slug."/";

            $articles=$category->articles()->get();

            $data=compact('articles','slug','categories','last_articles','tags');

            return view('paginas/blog',$data);
            //si no es un blog entonces es una categoria, si esta falla entonces es un find or fail
        }


    }

    public function getArticlesBySlug($tag_slug){

        //comprobamos si es una categoria o un blog

        $last_articles = $this->r_a->getAllActived()->take(3)->get();
        $categories=$this->r_c->getAllActived();
//        dd($tag_slug);
        $tag=$this->r_tag->getBySlug($tag_slug);
//        dd($tag_slug);
        $articles=$tag->articles()->get();



        $data=compact('articles','slug','categories','last_articles','tags');

        return view('paginas/blog',$data);
        //si no es un blog entonces es una categoria, si esta falla entonces es un find or fail



    }


//getArticlesBySlug



    public function getBlogDet(){
        return view('paginas/blog-det');
    }
    public function getProfileImage(){

        return Storage::get($this->id.'/'.$this->id.'.jpeg');
    }






}
