<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Auth;

use Validator;
class LoginController extends Controller
{
    public function login(Request $request)
    {

        $requestData=$request->all();

        $rules = array(
            'user'     => 'required',
            'password' => 'required'
        );

        $messages = [
            'user.required'     =>  '-El campo usuario es requerido',
            'password.required' =>  '-El campo contraseña es requerido'
        ];

        $validator = Validator::make($requestData, $rules, $messages);

        if ($validator->fails()) {

            return redirect('cms/login');

        } else {
//            var_dump($request->user);
//            var_dump($request->password);
//            var_dump($request->password);


            if (Auth::attempt(['correo' => $request->user , 'password' => $request->password])) {

                return redirect("cms");

            }else{

                $request->session()->flash('mensaje','Datos incorrectos');
                return redirect('cms/login');

            }

        }

    }

    public function logout()
    {
        Auth::logout();
        return view('paginas/login');
    }


    public function create()
    {
        return view('paginas/login');
    }



}
