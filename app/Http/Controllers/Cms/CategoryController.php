<?php

namespace App\Http\Controllers\Cms;

use App\Models\Category;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


use App\Repositories\CategoryRepository;


use App\Http\Middleware\Cms\CategoryMiddleware;

use Storage;

class CategoryController extends Controller
{

    private $r_a;


    function __construct(CategoryRepository $r_a)
    {

        $this->middleware(CategoryMiddleware::class, ['only' => [ 'store', 'update']]);


        $this->r_a =  $r_a;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->r_a->getAll();



        $data = compact('categories');

        return view('cms/category/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = new Category;

        $data = compact('category');

        return view('cms/category/category', $data);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data["slug"]=$this->create_slug($request->name);

        $last_category =  $this->r_a->save($data);

        $last_id =  $last_category->id;

       $request->session()->flash('mensaje','Articulo creado correctamente');

       return redirect('cms/categories/'.$last_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {


        $data = compact('category');

        return view('cms/category/category', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {

        $data = $request->all();

        $data["slug"]=$this->create_slug($request->name);

        $this->r_a->update($category->id,$data);

        $request->session()->flash('mensaje','Categoria modificada Correctamente');

        $last_id = $category->id;

        return redirect('cms/categories/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $this->r_a->delete($category->id);
//
        Session::flash("mensaje", "Categoria eliminado");


        return redirect('cms/categories/');
    }


    public function create_slug($slug)
    {
        $slug=strtolower($slug);
        $slug = trim($slug);

        $slug = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $slug
        );

        $slug = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $slug
        );

        $slug = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $slug
        );

        $slug = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $slug
        );

        $slug = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $slug
        );

        $slug = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $slug
        );

        $slug = str_replace(
            array(' '),
            array('-'),
            $slug
        );


        $slug = str_replace(array("/","¨", "º", "-", "~",
            "#", "@", "|", "!", "'",
            "·", "$", "%", "&", "/",
            "(", ")", "?", "'", "¡",
            "¿", "[", "^", "<code>", "]",
            "+", "}", "{", "¨", "´",
            ">", "< ", ";", ",", ":",
            ".", " "),
            '',
            $slug
        );


        $slug=$slug.'-'.Carbon::now()->format('d-m-Y');


        return $slug;

    }


}
