<?php 

namespace App\Http\Controllers\Cms;

use App\Models\Project;
use App\Distrito;
use App\Origen;
use App\Piso;
use App\Seguimiento;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Visitas;

use Illuminate\Http\Request;

class SeguimientoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$projects = Project::all();
		$visita = Visitas::all();
		$pisos=Piso::all();
		$seguimientos=Seguimiento::all();;

		$data = compact('visita', 'pisos','seguimientos', 'projects');

		return view('cms/visitas/index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$visitas=  new Visitas;

		$projects = Project::all();
		$distritos=Distrito::all();
		$origens=Origen::all();
		$pisos=Piso::all();
		$flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
		$flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
		$flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
		$data = compact('visitas', 'distritos', 'origens','pisos','flg_departamentos','flg_estacionmientos','flg_depositos','projects');
		return view('cms/visitas/visitas', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $estacio_precio)
	{
        $estacio_precios=$estacio_precio->all();
        $idprecio_name="visitas_idvisitas";
        $idprecio=$estacio_precios["visitas_idvisitas"];
        $rows=$estacio_precios["row"];

        foreach ($rows as $key => $estacio_precio ) {
            if ($estacio_precio["id"] == "") {

                unset($estacio_precio["id"]);
                $estacio_precio[$idprecio_name]=$idprecio;

                Seguimiento::create($estacio_precio);

                echo "est_created ";
                //Si esta vacio crea
            }else{
                $estacio_precio["$idprecio_name"]=$idprecio;
                $model=Estacio_precio::find($estacio_precio["id"] );
                $model->update($estacio_precio);
                //Si no esta vacio actualiza
            }
        }

//		$request=Seguimiento::create($request->all());
//		echo json_encode($request->idseguimiento);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$data = compact('visitas');
		return view('cms/visitas/visitas', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$visitas=$this->model->find($id);
		$visitas->update($request);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$this-> ->delete($visitantes->id);
		
		Session::flash("mensaje", "Visitante eliminado");

		return redirect('cms/visitas/');
	}

}

