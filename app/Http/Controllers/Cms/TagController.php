<?php

namespace App\Http\Controllers\Cms;

use App\Models\Tag;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


use App\Repositories\TagRepository;


use App\Http\Middleware\Cms\TagMiddleware;

use Storage;

class TagController extends Controller
{

    private $r_t;


    function __construct(TagRepository $r_t)
    {

        $this->middleware(TagMiddleware::class, ['only' => [ 'store', 'update']]);


        $this->r_t =  $r_t;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tags = $this->r_t->getAll();

        $data = compact('tags');

        return view('cms/tag/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tag = new Tag;

        $data = compact('tag');

        return view('cms/tag/tag', $data);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data["slug"]=$this->create_slug($request->name);

        $last_tag =  $this->r_t->save($data);

        $last_id =  $last_tag->id;

       $request->session()->flash('mensaje','Tag creado correctamente');

       return redirect('cms/tags/'.$last_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {


        $data = compact('tag');

        return view('cms/tag/tag', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {

        $data = $request->all();

        $data["slug"]=$this->create_slug($request->name);



        $this->r_t->update($tag->id,$data);

        $request->session()->flash('mensaje','Tag modificado Correctamente');

        $last_id = $tag->id;

        return redirect('cms/tags/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tag  $tag
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        $this->r_t->delete($tag->id);
//
        Session::flash("mensaje", "Tag eliminado");


        return redirect('cms/tags/');
    }


    public function create_slug($slug)
    {
        $slug=strtolower($slug);
        $slug = trim($slug);

        $slug = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $slug
        );

        $slug = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $slug
        );

        $slug = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $slug
        );

        $slug = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $slug
        );

        $slug = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $slug
        );

        $slug = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $slug
        );

        $slug = str_replace(
            array(' '),
            array('-'),
            $slug
        );


        $slug = str_replace(array("/","¨", "º", "-", "~",
            "#", "@", "|", "!", "'",
            "·", "$", "%", "&", "/",
            "(", ")", "?", "'", "¡",
            "¿", "[", "^", "<code>", "]",
            "+", "}", "{", "¨", "´",
            ">", "< ", ";", ",", ":",
            ".", " "),
            '',
            $slug
        );


        //$slug=$slug.'-'.Carbon::now()->format('d-m-Y');


        return $slug;

    }


}
