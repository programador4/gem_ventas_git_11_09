<?php

namespace App\Http\Controllers\Cms;

use App\Models\SocialNetwork;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;


use App\Repositories\SocialNetworkRepository;


use App\Http\Middleware\Cms\SocialNetworkMiddleware;

use Storage;

class SocialNetworkController extends Controller
{

    private $r_sn;


    function __construct(SocialNetworkRepository $r_sn)
    {

        $this->middleware(SocialNetworkMiddleware::class, ['only' => [ 'store', 'update']]);


        $this->r_sn =  $r_sn;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $socialnetworks = $this->r_sn->getAll();



        $data = compact('socialnetworks');

        return view('cms/socialnetwork/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $socialnetwork = new SocialNetwork;

        $data = compact('socialnetwork');

        return view('cms/socialnetwork/socialnetwork', $data);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $last_socialnetwork =  $this->r_sn->save($data);

        $last_id =  $last_socialnetwork->id;

        $img_socialnetwork_name= $last_id.$request->file('logo')->hashName();

        if( $request->hasFile('logo') ) {

            $path = $request->file('logo')->storeAs(

            'socialnetworks', $img_socialnetwork_name

            );

        }

        $data["logo"]=$img_socialnetwork_name;

        $this->r_sn->update($last_id,$data);

        $request->session()->flash('mensaje','Red social creada correctamente');


        return redirect('cms/socialnetworks/'.$last_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SocialNetwork  $socialnetwork
     * @return \Illuminate\Http\Response
     */
    public function show(SocialNetwork $socialnetwork)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SocialNetwork  $socialnetwork
     * @return \Illuminate\Http\Response
     */
    public function edit(SocialNetwork $socialnetwork)
    {


        $data = compact('socialnetwork');

        return view('cms/socialnetwork/socialnetwork', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SocialNetwork  $socialnetwork
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SocialNetwork $socialnetwork)
    {

        $data = $request->all();

        $last_id =  $socialnetwork->id;

        if(!Storage::disk('local')->exists('socialnetworks/'.$request->logo)){

        $img_socialnetwork_name = $request->file('logo')->hashName();

        if( $request->hasFile('logo') ) {



            Storage::delete("socialnetworks/".$socialnetwork->logo);


            $request->file('logo')->storeAs(
                'socialnetworks', $img_socialnetwork_name
            );

        }

        $data["logo"] = $img_socialnetwork_name;

        }else{

            $data["logo"] = $request->logo_t;

        }

        $this->r_sn->update($socialnetwork->id,$data);

        $request->session()->flash('mensaje','Red social modificada Correctamente');

        $last_id = $socialnetwork->id;

        return redirect('cms/socialnetworks/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SocialNetwork  $socialnetwork
     * @return \Illuminate\Http\Response
     */
    public function destroy(SocialNetwork $socialnetwork)
    {
        $this->r_sn->delete($socialnetwork->id);
//
        Session::flash("mensaje", "Red social eliminada");


        return redirect('cms/socialnetworks/');
    }
}
