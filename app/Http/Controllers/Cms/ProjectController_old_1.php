<?php

namespace App\Http\Controllers\Cms;
use App\banco;
use App\Departamento;
use App\Deposito;
use App\Distrito;
use App\Estacionamiento;
use App\Http\Middleware\Cms\ProjectMiddleware;
//Models
use App\Models\Project;


use App\Http\Requests\ProjectRequest;
use App\Models\User;
use App\Piso;
use App\Repositories\ClientRepository;
use App\Repositories\ProjectRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Storage;

use Illuminate\Support\Facades\Session;

class ProjectController extends Controller
{
//    private $r_p;
//
//    private $c_r;
//    function __construct( ProjectRepository $r_p , ClientRepository $c_r)
//    {
//
//        $this->middleware(ProjectMiddleware::class, ['only' => [ 'store', 'update']]);
//        $this->r_p =  $r_p;
//        $this->c_r =  $c_r;
//
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        $distrito=new Distrito;

        $data = compact('projects','distrito');

        return view('cms/project/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyectos= new Project;


        $distritos=Distrito::all();
        $bancos=banco::all();
        $usuarios_v=User::where('tipo_usuario_idtipo_usuario','=', '2')->get();
        $pisos=Piso::all();
        $departamentos=Departamento::all();
        $flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
        $data = compact('proyectos','distritos','departamentos','bancos','usuarios_v','pisos','flg_departamentos','flg_estacionmientos','flg_depositos');
        return view('cms/project/project', $data);



    }
    public function create_project(Request $proyecto)
    {
        $this->store();
        //Para crear el proyecto debemos crear

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $proyecto)
    {
//        Project::create($proyecto->all());
        $project=Project::create($proyecto->all());
        echo json_encode($project->idproyectos);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *f
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $proyectos)
    {


//        $clients = $this->c_r->list_select('name','id');
        $distritos=Distrito::all();
        $bancos=banco::all();
        $usuarios_v=User::where('tipo_usuario_idtipo_usuario','=', '2')->get();
        $pisos=Piso::all();
        $flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
        $data = compact('proyectos','distritos','bancos','usuarios_v','pisos','flg_departamentos','flg_estacionmientos','flg_depositos');
        return view('cms/project/project', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $project=$this->model->find($id);
        $project->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $this->r_p->delete($project->id);

        Session::flash("mensaje", "Projecto eliminado");

        return redirect('cms/projects/');
    }


    public function validate_project()
    {
//        ProjectRequest $project
        var_dump("project");
    }
    /**
     * Return a sluged version of a wird
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function proyect_departamentos(Request $request)
    {

        $option=Departamento::where("proyectos_idproyectos",$request->id)->get()->toJson();

        return $option;
    }
    public function proyect_estacionamiento(Request $request)
    {
        $option=Estacionamiento::where("proyectos_idproyectos",$request->id)->get()->toJson();
        echo $option;
    }
    public function proyect_depositos(Request $request)
    {
        $option=Deposito::where("proyectos_idproyectos",$request->id)->get()->toJson();
        echo $option;
    }
    /*
     *   Departamento::create($departamento);
                $departamento_option=Departamento::where("proyectos_idproyectos",$departamento["proyectos_idproyectos"])->get()->toJson();

     *
     * */


}
