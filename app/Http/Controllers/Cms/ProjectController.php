<?php

namespace App\Http\Controllers\Cms;
use App\banco;
use App\Departamento;
use App\Deposito;
use App\Distrito;
use App\Estacionamiento;
use App\Http\Middleware\Cms\ProjectMiddleware;
use App\Tipodepartamento;
use App\Promocion;
//Models
use App\Models\Project;


use App\Http\Requests\ProjectRequest;
use App\Models\User;
use App\Piso;
use App\Repositories\ClientRepository;
use App\Repositories\ProjectRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Storage;

use Illuminate\Support\Facades\Session;

class ProjectController extends Controller
{
//    private $r_p;
//
//    private $c_r;
//    function __construct( ProjectRepository $r_p , ClientRepository $c_r)
//    {
//
//        $this->middleware(ProjectMiddleware::class, ['only' => [ 'store', 'update']]);
//        $this->r_p =  $r_p;
//        $this->c_r =  $c_r;
//
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        $distrito=new Distrito;

        $data = compact('projects','distrito');

        return view('cms/project/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function calcular_precio_venta(Request $request){
        /*********************/
//        Obteniendo ids del ajax
            $id_tipo_departamento=$request->id_td;
            $id_precio=$request->id_precio;
            $id_proyecto=$request->id_proyecto;
            $id_piso=$request->idpiso;

            if($id_piso==0){$id_piso=1;}
        /*********************/
//        Buscando los modelos de td,precios y departamentos
        $departamentos=new \App\Departamento();
        $dpto_precios=new \App\Dpto_precio();
        $dpto_precios=$dpto_precios::where('precios_idprecios','=', $id_precio)->get();
        $tipo_departamento=new \App\Tipodepartamento();
        $tipo_departamento=$tipo_departamento::find($id_tipo_departamento);
        $precio= new \App\Precio();
        $precio=$precio::find($id_precio);

        /*********************/
//        Variables
        $precio_area=           $tipo_departamento->inicio_area_maxima;
        $area_libre=            $tipo_departamento->inicio_area_maxima;
        $area_techada=          $tipo_departamento->fin_area_maxima;
        $area_total=$area_libre*$area_techada;
        $factor_desc_preventa=  $precio->factor_desc_preventa;
        /********************/
        /*********Calculando precio espacio de matriz***********/

        foreach ($dpto_precios as $dpto_precio){
            $piso_a=      $dpto_precio->piso_inicio;
            $precio_area= $dpto_precio->precio_area;
            $piso_b=      $dpto_precio->piso_fin;
//            a>=$id_piso<=b

            if($id_piso>=$piso_a){
                if($id_piso<=$piso_b){
                    $precio=($area_libre*$precio_area*$factor_desc_preventa)+($area_techada*$precio_area);
//                    echo json_encode($precio);
                    echo json_encode($precio);
                    exit();
                }
            }
        }
    }


    public function calcular_precio_venta_estacionamiento(Request $request){
        /*********************/
//        Obteniendo ids del ajax
//        dd($request->all());
        $tipo=$request->tipo;
        $piso_idpiso=$request->piso_idpiso;
        $id_proyecto=$request->id_proyecto;
        /*********************/
        $estacio_precio=new \App\Estacio_precio();
        $estacio_precio=$estacio_precio::where('tipo','=', $tipo)->where('id_proyecto','=', $id_proyecto)->where('piso_idpiso','=', $piso_idpiso)->get()->last();
//        $estacio_precio=$estacio_precio::whereRaw('tipo = '.$tipo.' and id_proyecto = '.$id_proyecto.' and piso_idpiso = '.$piso_idpiso, [25])->get()->last();

        if($estacio_precio==null){
            echo json_encode(0);
        }else{
            echo json_encode($estacio_precio->precio);
        }
        /*********************/
//        dd($tipo);
//        dd('tipo = '.$tipo.' and id_proyecto = '.$id_proyecto.' and piso_idpiso = '.$piso_idpiso);

    }



    public function create()
    {
        $proyectos= new Project;

        $tipodepartamentos=Tipodepartamento::all();
        $promociones=Promocion::all();
        $distritos=Distrito::all();
        $bancos=banco::all();
        $usuarios_v=User::where('tipo_usuario_idtipo_usuario','=', '2')->get();
        $pisos=Piso::all();
        $departamentos=Departamento::all();
        $flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
        
        $data = compact(
            'proyectos',
            'distritos',
            'departamentos',
            'bancos',
            'usuarios_v',
            'pisos',
            'flg_departamentos',
            'flg_estacionmientos',
            'flg_depositos',
            'tipodepartamentos',
            'promociones'
        );
        return view('cms/project/project', $data);



    }
    public function create_project(Request $proyecto)
    {
        $this->store();
        //Para crear el proyecto debemos crear

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $proyecto)
    {
        $project=$proyecto->all();
        /********************Save image**************************/
  //1)Guardando en disk local la imagen

       if($proyecto->hasFile('url_plano')) {
             // a)Colocando nombre unico
           $project["url_plano"]=$project["url_plano"]->getClientOriginalName();
           $filename = pathinfo($project["url_plano"], PATHINFO_FILENAME);
           $extension = pathinfo($project["url_plano"], PATHINFO_EXTENSION);
           $project["name_url_plano"]= $filename ."_". date("Y-m-d_H:i:s")."." . $extension; // 'qwe jpg'
           $project["name_url_plano"]=str_replace(":","_",$project["name_url_plano"]);
           //B)Guardando la imagen temporal
            $file = $proyecto->file('url_plano');
            $file->move("proyect_logos",$project["name_url_plano"]);
            $project["url_plano"]=$project["name_url_plano"];
         }



       //2)Guardando en bd

        /********************Save image**************************/
        if($project["banco_idbanco"]==""){unset($project["banco_idbanco"]);}
        $project=Project::create($project);
        echo json_encode($project->idproyectos);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *f
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $proyectos)
    {


//        $clients = $this->c_r->list_select('name','id');
        $distritos=Distrito::all();
        $bancos=banco::all();
        $usuarios_v=User::where('tipo_usuario_idtipo_usuario','=', '2')->get();
        $pisos=Piso::all();
        $flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
        $data = compact('proyectos','distritos','bancos','usuarios_v','pisos','flg_departamentos','flg_estacionmientos','flg_depositos');
        return view('cms/project/project', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $project=$this->model->find($id);
        $project->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $this->r_p->delete($project->id);

        Session::flash("mensaje", "Projecto eliminado");

        return redirect('cms/projects/');
    }


    public function validate_project()
    {
//        ProjectRequest $project
        var_dump("project");
    }
    /**
     * Return a sluged version of a wird
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function proyect_departamentos(Request $request)
    {

        $option=Departamento::where("proyectos_idproyectos",$request->id)->get()->toJson();

        return $option;
    }
    public function proyect_estacionamiento(Request $request)
    {
        $option=Estacionamiento::where("proyectos_idproyectos",$request->id)->get()->toJson();
        echo $option;
    }
    public function proyect_depositos(Request $request)
    {
        $option=Deposito::where("proyectos_idproyectos",$request->id)->get()->toJson();
        echo $option;
    }
    /*
     *   Departamento::create($departamento);
                $departamento_option=Departamento::where("proyectos_idproyectos",$departamento["proyectos_idproyectos"])->get()->toJson();

     *
     * */


}
