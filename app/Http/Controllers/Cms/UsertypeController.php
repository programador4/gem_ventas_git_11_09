<?php

namespace App\Http\Controllers\Cms;

use App\Http\Middleware\Cms\UserTypeMiddleware;
use App\Models\Usertype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\UserTypeRepository;
use Illuminate\Support\Facades\Session;


class UserTypeController extends Controller
{

    private $r_ut;

    private $model;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(UserTypeRepository $r_ut)
    {

       $this->middleware(UserTypeMiddleware::class, ['only' => [ 'store', 'Update']]);

        $this->r_ut = $r_ut;
    }

    public function index()
    {
        $usertypes = $this->r_ut->getAll();

        $data = compact('usertypes');

        return view('cms/userType/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $usertype = new Usertype;

        $data = compact('usertype');

        return view('cms/userType/userType', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $user_type =  $this->r_ut->save($data);

        return redirect('cms/usertypes/'.$user_type->id.'/edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Usertype  $usertype
     * @return \Illuminate\Http\Response
     */
    public function show(Usertype $usertype)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Usertype  $usertype
     * @return \Illuminate\Http\Response
     */
    public function edit(Usertype $usertype)
    {

        $data = compact('usertype');

        return view('cms/userType/userType', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Usertype  $usertype
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Usertype $usertype)
    {
        $data = $request->all();

        $this->r_ut->update($usertype->id,$data);

        $request->session()->flash('mensaje','Tipo de Usuario Modificado Correctamente');

        $last_id = $usertype->id;

        return redirect('cms/usertypes/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usertype  $usertype
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usertype $usertype)
    {
        $usertype->delete();
//
        Session::flash("mensaje", "Tipo de usuario eliminado");


        return redirect('cms/usertypes/');
    }


    /**
     * Obtiene la lista de clientes
     *
     * @param  \App\Models\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function list_select($name,$id)
    {
        return $this->model->pluck($name, $id);

    }




}
