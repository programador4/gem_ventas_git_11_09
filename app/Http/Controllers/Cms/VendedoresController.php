<?php

namespace App\Http\Controllers\Cms;

//Models
use App\Departamento;

use App\Vendedores;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Storage;

use Illuminate\Support\Facades\Session;

class VendedoresController extends Controller
{

    function __construct( )
    {


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = $this->r_p->getAll();

        $data = compact('projects');
        var_dump($projects);


        return view('cms/project/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = $this->c_r->list_select('name','id');

        $project = new Departamento;

        $data = compact('project', 'clients');

        return view('cms/project/project', $data);



    }
    public function create_project(Request $proyecto)
    {
        $this->store();
//Para crear el proyecto debemos crear
        /*
         *
         *
         *
         *
         *
         *
         * */
        Departamento::create($proyecto->all());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $estacio_precio)
    {

        $estacio_precios=$estacio_precio->all();
        $idprecio_name="proyectos_idproyectos";
        $idprecio=$estacio_precios["proyectos_idproyectos"];
        $rows=$estacio_precios["row"];

        foreach ($rows as $key => $estacio_precio ) {
            if ($estacio_precio["id"] == "") {

                unset($estacio_precio["id"]);
                $estacio_precio[$idprecio_name]=$idprecio;

                Vendedores::create($estacio_precio);

                echo "est_created ";
                //Si esta vacio crea
            }else{
                $estacio_precio["$idprecio_name"]=$idprecio;
                $model=Vendedores::find($estacio_precio["id"] );
                $model->update($estacio_precio);
                //Si no esta vacio actualiza
            }
        }
        $_SESSION['mensaje']='Proyecto creado correctamente';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Departamento  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Departamento $project)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Departamento  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Departamento $project)
    {
        $clients = $this->c_r->list_select('name','id');

        $data = compact('clients','project');

        return view('cms/project/project', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Departamento  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Departamento $project)
    {
        $data = $request->all();

        $last_id =  $project->id;

        //Setting things for front_image

        if(!Storage::disk('local')->exists('projects/'.$request->front_image)) {

            $img_project_name = $request->file('front_image')->hashName();

            if ($request->hasFile('front_image')) {

                Storage::delete("projects/" . $project->front_image);


                $request->file('front_image')->storeAs(

                    'projects', $img_project_name

                );

            }

            $data["front_image"] = $img_project_name;

        }else{

            $data["front_image"] = $request->front_image_t;

        }



        //Setting things for back_image

        if(!Storage::disk('local')->exists('projects_bg/'.$request->back_image)){

            $img_project_name= $last_id.$request->file('back_image')->hashName();

        if( $request->hasFile('back_image') ) {

            Storage::delete("projects_bg/" . $project->back_image);

            $path = $request->file('back_image')->storeAs(

                'projects_bg', $img_project_name

            );

          }
            $data["back_image"]=$img_project_name;
        }else{

            $data["back_image"] = $request->back_image_t;

        }





        $this->r_p->update($project->id,$data);

        $request->session()->flash('mensaje','Departamentoo Modificado Correctamente');

        $last_id = $project->id;

        return redirect('cms/projects/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Departamento  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Departamento $project)
    {
        $this->r_p->delete($project->id);

        Session::flash("mensaje", "Departamentoo eliminado");

        return redirect('cms/projects/');
    }



    /**
     * Return a sluged version of a wird
     *
     * @param  \App\Departamento  $project
     * @return \Illuminate\Http\Response
     */


}
