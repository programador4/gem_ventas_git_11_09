<?php

namespace App\Http\Controllers\Cms;

use App\Questions;
use App\Http\Middleware\Cms\QuestionMiddleware;
use App\Models\Question;
use App\Repositories\QuestionRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Storage;

class QuestionController extends Controller
{

    private $r_q;

    function __construct( QuestionRepository $r_q)
    {

        $this->middleware(QuestionMiddleware::class, ['only' => [ 'store', 'update']]);

        $this->r_q =  $r_q;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = $this->r_q->getAll();



        $data = compact('questions');

        return view('cms/question/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $question = new Question;

        $data = compact('question');

        return view('cms/question/question', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = $request->all();

       $last_id =  $this->r_q->save($data);

       $request->session()->flash('mensaje','Consulta creada correctamente');

       return redirect('cms/questions/'.$last_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {

        $data = compact('question');

        return view('cms/question/question', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        $questions = $this->r_q->list_select('name','id');

        $data = compact('questions','question');

        return view('cms/question/question', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {


        $data = $request->all();

        $this->r_q->update($question->id,$data);

        $request->session()->flash('mensaje','Consulta creada correctamente');

        $last_id = $question->id;

        return redirect('cms/questions/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        $this->r_q->delete($question->id);

        Session::flash("mensaje", "Consulta eliminada");

        return redirect('cms/questions/');
    }
}
