<?php

namespace App\Http\Controllers\Cms;

use App\Models\Service;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


use App\Repositories\ServiceRepository;


use App\Http\Middleware\Cms\ServiceMiddleware;

use Validator;

class ServiceController extends Controller
{
    private $r_s;


    function __construct( ServiceRepository $r_s )
    {

        $this->middleware(ServiceMiddleware::class, ['only' => [ 'store', 'update']]);

        $this->r_s =  $r_s;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = $this->r_s->getAll();



        $data = compact('services');

        return view('cms/service/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $service = new Service;

        $data = compact('service');

        return view('cms/service/service', $data);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = $request->all();
        //dd($data);

       $data["slug"]=$this->create_slug($request->name);

       $last_id =  $this->r_s->save($data)->id;



       $request->session()->flash('mensaje','Servicio creado correctamente');

       return redirect('cms/services/'.$last_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {

        $data = compact('service');

        return view('cms/service/service', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {

        $data = $request->all();

        $data["slug"]=$this->create_slug($request->name);

        $this->r_s->update($service->id,$data);

        $request->session()->flash('mensaje','Servicio Modificado Correctamente');

        $last_id = $service->id;

        return redirect('cms/services/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $this->r_s->delete($service->id);
//
        Session::flash("mensaje", "Servicio eliminado");


        return redirect('cms/services/');
    }

    public function create_slug($slug)
    {
        $slug=strtolower($slug);
        $slug = trim($slug);

        $slug = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $slug
        );

        $slug = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $slug
        );

        $slug = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $slug
        );

        $slug = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $slug
        );

        $slug = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $slug
        );

        $slug = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $slug
        );

        $slug = str_replace(
            array(' '),
            array('-'),
            $slug
        );


        $slug = str_replace(array("/","¨", "º", "-", "~",
            "#", "@", "|", "!", "'",
            "·", "$", "%", "&", "/",
            "(", ")", "?", "'", "¡",
            "¿", "[", "^", "<code>", "]",
            "+", "}", "{", "¨", "´",
            ">", "< ", ";", ",", ":",
            ".", " "),
            '',
            $slug
        );


        $slug=$slug.'-'.Carbon::now()->format('d-m-Y');


        return $slug;

    }

}
