<?php

namespace App\Http\Controllers\Cms;

use App\Http\Requests\UserRequest;
use App\User;
use App\Models\Usertype;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;


use App\Repositories\UserRepository;
use App\Repositories\UserTypeRepository;



use App\Http\Middleware\Cms\UserMiddleware;

use Illuminate\Support\Facades\Storage;
use Validator;

class UserController extends Controller

{
    private $r_u;

    private $r_ut;

    function __construct( UserRepository $r_u ,UserTypeRepository $r_ut)
    {

//        $this->middleware(UserMiddleware::class, ['only' => [ 'store', 'update']]);

        $this->r_u =  $r_u;

        $this->r_ut = $r_ut;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->r_u->getAll();



        $data = compact('users');

        return view('cms/user/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user_types =Usertype::all();

        $user = new User;

        $data = compact('user_types', 'user');

        return view('cms/user/user', $data);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
       $data = $request->all();

       $data['password'] = bcrypt($data['password']);

       $last_user =  $this->r_u->save($data);

       $last_id=DB::getPdo()->lastInsertId();

      $request->session()->flash('mensaje','Usuario creado correctamente');

       return redirect('cms/users/'.$last_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        $user_types = Usertype::all();
        $user=User::find($user);

        $data = compact('user_types','user');

        return view('cms/user/user', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $user)
    {
        $user=User::find($user);
      ;
        $data = $request->all();
//        var_dump($data['password']);
//        var_dump($user->password);
        $data['password']=$this->r_u->checkpassword($data['password'],$user);

//        if($data['password']!=$user->password){unset($data['password']);}
//        dd($data['password']);


        $this->r_u->update($user->idusuario,$data);


        $request->session()->flash('mensaje','Usuario Modificado Correctamente');

        $last_id = $user->idusuario;

        return redirect('cms/users/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        var_dump($user);

        $user=User::find($user);
        $user->delete();

        Session::flash("mensaje", "Usuario Eliminado");
        return redirect('cms/users/');
    }



}
