<?php

namespace App\Http\Controllers\Cms;

use App\Clients;
use App\Http\Middleware\Cms\ClientMiddleware;
use App\Models\Client;
use App\Repositories\ClientRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Storage;

class ClientController extends Controller
{

    private $r_c;

    function __construct( ClientRepository $r_c)
    {

        $this->middleware(ClientMiddleware::class, ['only' => [ 'store', 'update']]);

        $this->r_c =  $r_c;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = $this->r_c->getAll();
//        var_dump($clients);
//        exit();


        $data = compact('clients');

        return view('cms/client/index' , $data);

    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = $this->r_c->list_select('name','id');

        $client = new Client;

        $data = compact('client', 'clients');

        return view('cms/client/client', $data);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $last_id =  $this->r_c->save($data)->id;

        $img_client_name= $request->file('logo')->hashName();

        if( $request->hasFile('logo') ) {

            $path = $request->file('logo')->storeAs(

            'clients', $img_client_name

            );

        }

        $data["logo"]=$img_client_name;

        $this->r_c->update($last_id,$data);


       $request->session()->flash('mensaje','Cliente creado correctamente');

       return redirect('cms/clients/'.$last_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        $clients = $this->r_c->list_select('name','id');

        $data = compact('clients','client');

        return view('cms/client/client', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $data = $request->all();

        $last_id =  $client->id;

        if(!Storage::disk('local')->exists('clients/'.$request->logo)){


            $img_client_name = $request->file('logo')->hashName();



        if( $request->hasFile('logo') ) {


            var_dump($client->logo);

            Storage::delete("clients/".$client->logo);


            $request->file('logo')->storeAs(
                'clients', $img_client_name
            );

        }

        $data["logo"] = $img_client_name;

        }else{

        $data["logo"] = $request->logo_t;

        }

        $this->r_c->update($client->id,$data);

        $request->session()->flash('mensaje','Cliente Modificado Correctamente');

        $last_id = $client->id;

        return redirect('cms/clients/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        $this->r_c->delete($client->id);

        Session::flash("mensaje", "Cliente eliminado");

        return redirect('cms/clients/');
    }
}
