<?php

namespace App\Http\Controllers\Cms;

use App\Models\Article;

use App\Repositories\CategoryRepository;
use App\Repositories\TagRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


use App\Repositories\ArticleRepository;


use App\Http\Middleware\Cms\ArticleMiddleware;


use Storage;

class ArticleController extends Controller
{

    private $r_a;

    private $r_c;

    private $r_t;
    function __construct(ArticleRepository $r_a,CategoryRepository $r_c,TagRepository $r_t)
    {

        $this->middleware(ArticleMiddleware::class, ['only' => [ 'store', 'update']]);


        $this->r_a =  $r_a;
        $this->r_c =  $r_c;
        $this->r_t =  $r_t;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = $this->r_a->getAll();
        $tags = $this->r_t->getAll();
        $data = compact('articles','tags');

        return view('cms/article/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = new Article;

        $categories= $this->r_c->getAll()->pluck('name','id');

        $tags= $this->r_t->getAll()->pluck('name','id');

        $cat_pub="";

        $art_tag="";

        $data = compact('article','categories','cat_pub','tags','art_tag');

        return view('cms/article/article', $data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $data["slug"]=$this->create_slug($request->title);

        $last_article =  $this->r_a->save($data);

        $last_id =  $last_article->id;

        $img_article_name= $request->file('front_image')->hashName();

        if( $request->hasFile('front_image') ) {

            $path = $request->file('front_image')->storeAs(

            'articles', $img_article_name

            );

        }

        $data["front_image"]=$img_article_name;

        $article=$this->r_a->update($last_id,$data);

        //anillando categorias
        if(!empty($request->categorias)){

            $categorias = explode(",", $request->categorias);

            $article->tags()->attach($request->tags);

        }

        //anillando tags
        if(!empty($request->tags)){

            $categorias = explode(",", $request->categorias);

            $article->categories()->attach($request->categorias);

        }



        $request->session()->flash('mensaje','Articulo creado correctamente');

       return redirect('cms/articles/'.$last_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //Revisando Categorias

        $categories= $this->r_c->list_select('name','id');
        $cat_pub=$article->cat_pub;

        foreach ($article->categories as $cat_sel){
            $cat_pub .= ",".$cat_sel["id"];
        }
        $cat_pub = ltrim($cat_pub, ',');


        //Revisando tags

        $tags= $this->r_t->list_select('name','id');

//        dd($tags);
        $art_tag="";

        foreach ($article->tags as $tag_sel){
            $art_tag .= ",".$tag_sel["id"];
        }
        $art_tag = ltrim($art_tag, ',');


        $data = compact('article','categories','cat_pub','tags','art_tag');
        return view('cms/article/article', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {

        $data = $request->all();

        $data["slug"]=$this->create_slug($request->title);

        $last_id =  $article->id;


        if(!Storage::disk('local')->exists('articles/'.$request->front_image)){


        $img_article_name = $request->file('front_image')->hashName();

        if( $request->hasFile('front_image') ) {


            Storage::delete("articles/".$article->front_image);


            $request->file('front_image')->storeAs(

                'articles', $img_article_name

            );

        }

        $data["front_image"] = $img_article_name;

        }else{

        $data["front_image"] = $request->front_image_t;

        }

        $article=$this->r_a->update($article->id,$data);

        //Leemos el  array del categorias y lo syncronizamos
        $categorias = explode(",",$request->categorias);


        if ($request->categorias!=null){
            $article->categories()->sync($categorias);


        }else{
            $article->categories()->sync([]);
        }


        //Leemos el  array del tags y lo syncronizamos
        $tags = explode(",",$request->tags);


        if ($request->tags!=null){
            $article->tags()->sync($tags);


        }else{
            $article->tags()->sync([]);
        }

        $request->session()->flash('mensaje','Articulo modificado Correctamente');

        $last_id = $article->id;

        return redirect('cms/articles/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        $this->r_a->delete($article->id);
//
        Session::flash("mensaje", "Articulo eliminado");


        return redirect('cms/articles/');
    }



    public function create_slug($slug)
    {
        $slug=strtolower($slug);
        $slug = trim($slug);

        $slug = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $slug
        );

        $slug = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $slug
        );

        $slug = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $slug
        );

        $slug = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $slug
        );

        $slug = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $slug
        );

        $slug = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $slug
        );

        $slug = str_replace(
            array(' '),
            array('-'),
            $slug
        );


        $slug = str_replace(array("/","¨", "º", "-", "~",
             "#", "@", "|", "!", "'",
             "·", "$", "%", "&", "/",
             "(", ")", "?", "'", "¡",
             "¿", "[", "^", "<code>", "]",
             "+", "}", "{", "¨", "´",
             ">", "< ", ";", ",", ":",
             ".", " "),
        '',
        $slug
    );


        $slug=$slug.'-'.Carbon::now()->format('d-m-Y');


        return $slug;

    }

}
