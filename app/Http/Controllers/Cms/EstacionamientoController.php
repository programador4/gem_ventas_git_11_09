<?php

namespace App\Http\Controllers\Cms;
use App\banco;
use App\Distrito;
use App\Estacionamiento;
//Models
use App\Models\Project;


use App\Models\User;
use App\Piso;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Storage;

use Illuminate\Support\Facades\Session;

class EstacionamientoController extends Controller
{
//    private $r_p;
//
//    private $c_r;
//    function __construct( ProjectRepository $r_p , ClientRepository $c_r)
//    {
//
//        $this->middleware(ProjectMiddleware::class, ['only' => [ 'store', 'update']]);
//        $this->r_p =  $r_p;
//        $this->c_r =  $c_r;
//
//    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        $distrito=new Distrito;

        $data = compact('projects','distrito');

        return view('cms/project/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $proyectos= new Project;


        $distritos=Distrito::all();
        $bancos=banco::all();
        $usuarios_v=User::where('tipo_usuario_idtipo_usuario','=', '2')->get();
        $pisos=Piso::all();
        $flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
        $data = compact('proyectos','distritos','bancos','usuarios_v','pisos','flg_departamentos','flg_estacionmientos','flg_depositos');
        return view('cms/project/project', $data);



    }
    public function create_project(Request $mode)
    {
        $this->store();
        //Para crear el proyecto debemos crear

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $estacio_precio)
    {

        $estacio_precios=$estacio_precio->all();
        $idprecio_name="proyectos_idproyectos";
        $idprecio=$estacio_precios["proyectos_idproyectos"];
        $rows=$estacio_precios["row"];
        foreach ($rows as $key => $estacio_precio ) {
            if ($estacio_precio["id"] == "") {

                unset($estacio_precio["id"]);
                $estacio_precio[$idprecio_name]=$idprecio;

                /***************Imagen**************/
                if($estacio_precio['url_plano']!=null) {
                    $file = $estacio_precio['url_plano'];

                    // a)Colocando nombre unico
                    $estacio_precio["url_plano"]=$estacio_precio["url_plano"]->getClientOriginalName();
                    $filename = pathinfo($estacio_precio["url_plano"], PATHINFO_FILENAME);
                    $extension = pathinfo($estacio_precio["url_plano"], PATHINFO_EXTENSION);
                    $estacio_precio["name_url_plano"]= $filename ."_". date("Y-m-d_H:i:s")."." . $extension; // 'qwe jpg'
                    $estacio_precio["name_url_plano"]=str_replace(":","_",$estacio_precio["name_url_plano"]);
                    //B)Guardando la imagen temporal
                    $file->move("proyect_estacionamiento_plano",$estacio_precio["name_url_plano"]);
                    $estacio_precio["url_plano"]=$estacio_precio["name_url_plano"];
                }
                /***************Imagen*************/

                Estacionamiento::create($estacio_precio);

                echo "est_created ";
                //Si esta vacio crea
            }else{
                $estacio_precio["$idprecio_name"]=$idprecio;
                $model=Estacio_precio::find($estacio_precio["id"] );
                $model->update($estacio_precio);
                //Si no esta vacio actualiza
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *f
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $proyectos)
    {


//        $clients = $this->c_r->list_select('name','id');
        $distritos=Distrito::all();
        $bancos=banco::all();
        $usuarios_v=User::where('tipo_usuario_idtipo_usuario','=', '2')->get();
        $pisos=Piso::all();
        $flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
        $data = compact('proyectos','distritos','bancos','usuarios_v','pisos','flg_departamentos','flg_estacionmientos','flg_depositos');
        return view('cms/project/project', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {

        $model=Estacionamiento::find($id);
        $model->update($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $this->r_p->delete($project->id);

        Session::flash("mensaje", "Projecto eliminado");

        return redirect('cms/projects/');
    }


    public function validate_project()
    {
//        ProjectRequest $project
        var_dump("project");
    }
    /**
     * Return a sluged version of a wird
     *
     * @param  \App\Project  $project
     * @return \Illuminate\Http\Response
     */


}
