<?php 

namespace App\Http\Controllers\Cms;

use App\Departamento;
use App\Deposito;
use App\Estacionamiento;
use App\Models\Project;
use App\Distrito;
use App\Models\User;
use App\Origen;
use App\Piso;
use App\Precio;
use App\Promocion;
use App\Seguimiento;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use Barryvdh\DomPDF\Facade as PDF;
use App\Models\Visitas;
use PDF;
use App\Tipodepartamento;
use Illuminate\Http\Request;
class VisitasController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(request $request)
        { $visitas_r=$request;
            $visita = new Visitas();
            if($visitas_r->project){
                $visita->where('proyectos_idproyectos', $visitas_r->project);
            }
            if($visitas_r->fecha_inicio){
                $visita->where('financiamiento', $visitas_r->financiamiento);
            }
            if($visitas_r->fecha_fin){
                $visita->where('financiamiento', '<', $visitas_r->financiamiento);
            }
            if($visitas_r->financiamiento){
                $visita->where('financiamiento', '>', $visitas_r->financiamiento);
            }
            if($request->has('_token')) {

            } else {
                $visita=$visita::all();
            }

            $projects =Project::all();
            $departamentos_visitas=new Departamento();
            $projects_visitas=new Project;
            $seguimientos_visitas=new Seguimiento;

            $pisos=Piso::all();
            $seguimientos=Seguimiento::all();;

            $data = compact('visita', 'pisos','seguimientos', 'projects','projects_visitas','seguimientos_visitas','departamentos_visitas','visitas_r');

            return view('cms/visitas/index', $data);
        }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{

		$visitas=  new Visitas;

		$projects = Project::all();
		$distritos=Distrito::all();
		$origens=Origen::all();
		$pisos=Piso::all();
		$flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
		$flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
		$flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();
		$data = compact('visitas', 'distritos', 'origens','pisos','flg_departamentos','flg_estacionmientos','flg_depositos','projects');

		return view('cms/visitas/visitas', $data);

	}
    public function store_cotizacion_old(Request $request)
    {
//        dd("test");
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('template.docx');
        /***************/
        $idusuario=$request->idusuario;
//        idusuario
        /***************/
         /***************/
        $idproyectos = $request->proyectos_idproyectos;
        $idpromociomn = $request->proyectos_idproyectos;
        $iddepartamentos = $request->departamentos_iddepartamentos;

        $idestacionamiento = $request->estacionamiento_idestacionamiento;

        $iddepositos = $request->depositos_iddepositos;
        /***************/
        /*Proyectos*/

        $proyecto=Project::where('idproyectos','=', $idproyectos)->get()->last();
//        $templateProcessor->setImageValue('logo','image1.jpg', 'my_image.jpg');
//        dd($request->proyectos_idproyectos);

        $url_logo=$proyecto->url_plano;

        $templateProcessor->setValue('legales',$proyecto->legales);
        /**/
        $precio=Precio::where('proyectos_idproyectos','=', $idproyectos)->get()->last();
        /*Promocion*/
        $promocion=Promocion::where('idpromocion','=', $precio->promocion_idpromocion)->get()->last();
        /*Departamento*/
        $departamento=Departamento::where('iddepartamentos','=', $iddepartamentos)->get()->last();

        $tipo_departamento=Tipodepartamento::where('idtipo_departamento','=', $departamento->tipo_departamento_idtipo_departamento)->get()->last();

            $templateProcessor->setValue('departamento',$departamento->departamento);
            $templateProcessor->setValue('departamento_at',$tipo_departamento->inicio_area_maxima);
            $templateProcessor->setValue('departamento_al',$tipo_departamento->fin_area_maxima);
            $templateProcessor->setValue('departamento_precio',$tipo_departamento->departamento);
            $templateProcessor->setValue('departamento_promocion',$promocion->nombre);
            $templateProcessor->setValue('departamento_total',($tipo_departamento->inicio_area_maxima* $tipo_departamento->fin_area_maxima));
       ;
            //inicio_area_maxima=area libre
        //fin_area_maxima=area techad
            /*Estacionamiento*/

        $estacionamiento=Estacionamiento::where('idestacionamiento','=', $idestacionamiento)->get()->last();

            $templateProcessor->setValue('estacionamiento',$estacionamiento->estacionamiento);
            $templateProcessor->setValue('estacionamiento_at',$estacionamiento->area_techada);
            $templateProcessor->setValue('estacionamiento_al',$estacionamiento->area_libre);
            $templateProcessor->setValue('estacionamiento_precio',$estacionamiento->precio_venta);

            $templateProcessor->setValue('estacionamiento_promocion',$estacionamiento->dscto_estacionamiento);//dscto_estacionamiento
//        dd("test");
        /*Departamento*/

        $deposito=Deposito::where('iddepositos','=', $iddepositos)->get()->last();
            $templateProcessor->setValue('deposito',$deposito->deposito);
            $templateProcessor->setValue('deposito_at',$deposito->area_techada);
            $templateProcessor->setValue('deposito_promocion',$promocion->nombre);
            $templateProcessor->setValue('deposito_precio',$deposito->precio_venta);
        /*Usuario*/

        $usuario=User::where('idusuario','=', $idusuario)->get()->last();
        $templateProcessor->setValue('nombre_v',$usuario->nombre);
        $templateProcessor->setValue('teléfono_v',$usuario->telefono);
        $templateProcessor->setValue('correo_v',$usuario->correo);
    /***************/
        $templateProcessor->setValue('dni',$request->get('dni'));
        $templateProcessor->setValue('nombres',$request->get('nombres'));
        $templateProcessor->setValue('apellidos',$request->get('apellidos'));
        $templateProcessor->setValue('correo',$request->get('correo'));
        $templateProcessor->setValue('telefono',$request->get('telefono'));
        $templateProcessor->setValue('fecha',$request->get('fecha'));
        $templateProcessor->setValue('interesado',$request->get('interesado'));
        $templateProcessor->setValue('financiamiento',$request->get('financiamiento'));
        $templateProcessor->setValue('cotizacion',$request->get('cotizacion'));
        $templateProcessor->setValue('distrito_iddistrito',$request->get('distrito_iddistrito'));
        $templateProcessor->setValue('proyectos_idproyectos',$request->get('proyectos_idproyectos'));
        $templateProcessor->setValue('departamentos_iddepartamentos',$request->get('departamentos_iddepartamentos'));
        $templateProcessor->setValue('estacionamiento_idestacionamiento',$request->get('estacionamiento_idestacionamiento'));
        $templateProcessor->setValue('depositos_iddepositos',$request->get('depositos_iddepositos'));
        $templateProcessor->setValue('origen_idorigen',$request->get('origen_idorigen'));
        $templateProcessor->setValue('nro_banos',$request->get('nro_banos'));
        $templateProcessor->setValue('nro_dormitorios',$request->get('nro_dormitorios'));
        $templateProcessor->setValue('cuarto_servicio',$request->get('cuarto_servicio'));
		$templateProcessor->setValue('m2',$request->get('m2'));
		$templateProcessor->setValue('precio',$request->get('precio'));



        $time = round(microtime(true) * 1000);
//        $text = $section->addText($request->get('number'),array('name'=>'Arial','size' => 20,'bold' => true));
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
//        $sTableText = $objWriter->getWriterPart('document')->getObjectAsText($table);
        $templateProcessor->saveAs('cotizacion/cotizacion_'. $time.'.docx');
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor('cotizacion/cotizacion_'. $time.'.docx');
        /******************/
        //Remplazar la imagen
        $fileindocx = "word/media/logo.png";
        $newimage = file_get_contents("proyect_logos/$url_logo");


//        $templateProcessor->zipClass->AddFromString('"word/media/logo.png"' ,$newimage );
        dd($url_logo);
        $templateProcessor->saveAs('cotizacion/cotizacion_'. $time.'.docx');
        /******************/
        echo '/cotizacion/cotizacion_'. $time.'.docx';
//        return response()->download(public_path('cotizacion/cotizacion_'. $time.'.docx'));
    }
    public function store_cotizacion_from_html(Request $request)
    {
//        dd("a");
        $pw = new \PhpOffice\PhpWord\PhpWord();

        /* [THE HTML] */
        $section = $pw->addSection();
        $html = "<h1 style='text-align: left'>HELLO WORLD!</h1>";
        $html .= "<p>This is a paragraph of random text</p>";
        $html .= "<table><tr><td style='background-color: red'>A table</td><td>Cell</td></tr></table>";
        \PhpOffice\PhpWord\Shared\Html::addHtml($section, $html, false, false);

        /* [SAVE FILE ON THE SERVER] */
// $pw->save("html-to-doc.docx", "Word2007");

        /* [OR FORCE DOWNLOAD] */
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment;filename="convert.docx"');
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($pw, 'Word2007');
        $objWriter->save('php://output');

    }

    public function store_cotizacion(Request $request)
    {
        /***************/
        $idusuario=$request->idusuario;
//        idusuario
        /***************/
        /***************/
        $idproyectos = $request->proyectos_idproyectos;
        $idpromocion = $request->proyectos_idproyectos;
        $iddepartamentos = $request->departamentos_iddepartamentos;
        $departamento=Departamento::where('iddepartamentos','=', $iddepartamentos)->get()->last();
        $tipo_departamento=Tipodepartamento::where('idtipo_departamento','=', $departamento->tipo_departamento_idtipo_departamento)->get()->last();
        $idestacionamiento = $request->estacionamiento_idestacionamiento;

        $iddepositos = $request->depositos_iddepositos;
        /***************/
        $precio=Precio::where('proyectos_idproyectos','=', $idproyectos)->get()->last();
        /*Promocion*/
        $promocion=Promocion::where('idpromocion','=', $precio->promocion_idpromocion)->get()->last();
        /******************************/

        /*Proyecto*/
        $projecto=Project::where('idproyectos','=', $idproyectos)->get()->last();

        $estacionamiento=Estacionamiento::where('idestacionamiento','=', $idestacionamiento)->get()->last();
        $deposito=Deposito::where('iddepositos','=', $iddepositos)->get()->last();
        $precio=Precio::where('proyectos_idproyectos','=', $idproyectos)->get()->last();
        $usuario=User::where('idusuario','=', $idusuario)->get()->last();
        $cliente=$request;
//        dd($estacionamiento->estacionamiento);
       $fecha=date("now");

        $data = compact('tipo_departamento','promocion','projecto', 'departamento','estacionamiento', 'deposito','precio','usuario','cliente','fecha');

        $time = round(microtime(true) * 1000);
        // Fetch all customers from database
        // Send data to the view using loadView function of PDF facade
//        dd($data);
        $pdf = PDF::loadView('cms/visitas/reporte_cotizacion', $data);
        $time = round(microtime(true) * 1000);
//        $templateProcessor->saveAs('cotizacion/cotizacion_'. $time.'.docx');
        // If you want to store the generated pdf to the server then you can use the store function
//        $templateProcessor->saveAs('cotizacion/cotizacion_'. $time.'.docx');
        $pdf->save(storage_path().'\cotizacion\cotizacion_'.$time.'.pdf');
        // Finally, you can download the file using download function
        echo '\cotizacion\cotizacion_'.$time.'.pdf';
//        return $pdf->download('customers.pdf');
    }


	public function store(Request $visita)
	{
		$visita=Visitas::create($visita->all());
		echo json_encode($visita->idvisitas);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $visitas=  new Visitas;
        $visitas=$visitas::find($id);
        $projects = Project::all();
        $distritos=Distrito::all();
        $seguimientos=Seguimiento::where("visitas_idvisitas",$id)->get();

        $origens=Origen::all();
        $pisos=Piso::all();
        $flg_departamentos=Piso::where('flg_departamento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_estacionmientos=Piso::where('flg_estacionmiento','=', '1')->orderBy('orden', 'ASC')->get();
        $flg_depositos=Piso::where('flg_deposito','=', '1')->orderBy('orden', 'ASC')->get();

        $data = compact('visitas','seguimientos', 'distritos', 'origens','pisos','flg_departamentos','flg_estacionmientos','flg_depositos','projects');
		return view('cms/visitas/visitas_editar', $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $visita,$id)
	{
        $visitas=  new Visitas;

		$visitas=$visitas->find($id);

		$visitas->update($visita->all());
		echo $id;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//$this-> ->delete($visitantes->id);
		
		Session::flash("mensaje", "Visitante eliminado");

		return redirect('cms/visitas/');
	}

}

