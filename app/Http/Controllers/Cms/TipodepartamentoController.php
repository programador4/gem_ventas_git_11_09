<?php namespace App\Http\Controllers\cms;

//Models
use App\Tipodepartamento;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class TipodepartamentoController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        $td=new Tipodepartamento();
//        dd($request->id_projecto);
        $td=$td::where("id_proyecto",$request->id_projecto)->get();

        echo json_encode($td);
	}

    public function get_select(Request $request)
    {
//        $td=new App\Tipodepartamento();
//        $td=$td::where("",$request->id_proyecto);
//        echo json_encode();
    }

    /**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		$tipodepartamentos= new tipodepartamento;

		$data = compact('tipodepartamentos');
		return view('cms/project/tipodepartamento', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $estacio_precio)
	{
        $estacio_precios=$estacio_precio->all();
        $idprecio_name="id_proyecto";
        $idprecio=$estacio_precios["id_proyecto"];
        $rows=$estacio_precios["row"];

        foreach ($rows as $key => $estacio_precio ) {

            if ($estacio_precio["id"] == "") {

                unset($estacio_precio["id"]);
                $estacio_precio[$idprecio_name]=$idprecio;


                /*****************************/
                if($estacio_precio['url_plano']!=null) {
                    $file = $estacio_precio['url_plano'];

                    // a)Colocando nombre unico
                    $estacio_precio["url_plano"]=$estacio_precio["url_plano"]->getClientOriginalName();
                    $filename = pathinfo($estacio_precio["url_plano"], PATHINFO_FILENAME);
                    $extension = pathinfo($estacio_precio["url_plano"], PATHINFO_EXTENSION);
                    $estacio_precio["name_url_plano"]= $filename ."_". date("Y-m-d_H:i:s")."." . $extension; // 'qwe jpg'
                    $estacio_precio["name_url_plano"]=str_replace(":","_",$estacio_precio["name_url_plano"]);
                    //B)Guardando la imagen temporal
                    $file->move("proyect_tipo_departamento_plano",$estacio_precio["name_url_plano"]);
                    $estacio_precio["url_plano"]=$estacio_precio["name_url_plano"];
                }
                /*****************************/

                Tipodepartamento::create($estacio_precio);



                echo "est_created ";
                //Si esta vacio crea
            }else{
                $estacio_precio["$idprecio_name"]=$idprecio;
                $model=Tipodepartamento::find($estacio_precio["id"] );
                $model->update($estacio_precio);
                //Si no esta vacio actualiza
            }
        }

//		echo json_encode($tipodepartamento->idtipodepartamento);
		
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
		
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
