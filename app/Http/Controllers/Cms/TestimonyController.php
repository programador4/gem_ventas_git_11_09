<?php

namespace App\Http\Controllers\Cms;

use App\Models\Testimony;

use App\Repositories\ClientRepository;
use App\Repositories\SocialNetworkRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


use App\Repositories\TestimonyRepository;


use App\Http\Middleware\Cms\TestimonyMiddleware;

use Validator;

class TestimonyController extends Controller
{
    private $r_c;

    private $r_t;

    private $r_sn;


    function __construct( TestimonyRepository $r_t,ClientRepository $r_c,SocialNetworkRepository $r_sn)
    {

        $this->middleware(TestimonyMiddleware::class, ['only' => [ 'store', 'update']]);

        $this->r_c =  $r_c;

        $this->r_t =  $r_t;

        $this->r_sn =  $r_sn;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonies = $this->r_t->getAll();



        $data = compact('testimonies');

        return view('cms/testimony/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $social_networks = $this->r_sn->list_select('name','id');

        $clients = $this->r_c->list_select('name','id');

        $testimony = new Testimony;

        $data = compact('testimony','social_networks','clients');

        return view('cms/testimony/testimony', $data);



    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = $request->all();

       $last_id =  $this->r_t->save($data)->id;

       $request->session()->flash('mensaje','Testimonio creado correctamente');

       return redirect('cms/testimonies/'.$last_id.'/edit');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Testimony  $testimony
     * @return \Illuminate\Http\Response
     */
    public function show(Testimony $testimony)
    {

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Testimony  $testimony
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimony $testimony)
    {
        $social_networks = $this->r_sn->list_select('name','id');

        $clients = $this->r_c->list_select('name','id');

        $data = compact('testimony','social_networks','clients');

        return view('cms/testimony/testimony', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Testimony  $testimony
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimony $testimony)
    {

        $data = $request->all();

        $this->r_t->update($testimony->id,$data);

        $request->session()->flash('mensaje','Testimonio modificado Correctamente');

        $last_id = $testimony->id;

        return redirect('cms/testimonies/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Testimony  $testimony
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimony $testimony)
    {
        $this->r_t->delete($testimony->id);
//
        Session::flash("mensaje", "Testimonio eliminado");


        return redirect('cms/testimonies/');
    }
}
