<?php

namespace App\Http\Controllers\Cms;

//Models
use App\Vendedores;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Storage;

use Illuminate\Support\Facades\Session;

class VendedoresController extends Controller
{

    function __construct( )
    {


    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = $this->r_p->getAll();

        $data = compact('projects');
        var_dump($projects);


        return view('cms/project/index' , $data);
    }

    /**
     * Show the form for creating a new resource.
     * q
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clients = $this->c_r->list_select('name','id');

        $project = new Vendedores;

        $data = compact('project', 'clients');

        return view('cms/project/project', $data);



    }
    public function create_project(Request $proyecto)
    {
        $this->store();
//Para crear el proyecto debemos crear
        /*
         *
         *
         *
         *
         *
         *
         * */
        Vendedores::create($proyecto->all());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $departamentos)
    {

        $departamentos=$departamentos->all();
        $idprecio_name="proyectos_idproyectos";
        $idprecio=$departamentos["proyectos_idproyectos"];
        $rows=$departamentos["row"];

        foreach ($rows as $key => $departamento ) {

            if ($departamento["id"] == "") {

                unset($departamento["id"]);

                $departamento[$idprecio_name]=$idprecio;

                $departamento["name_url_plano"]=$departamento["url_plano"]->getClientOriginalName();

//                dd($departamento["url_plano"]->getClientOriginalName());
//                dd($departamento["url_plano"]);
                //Hasheamos el nombre y guardamos

//                Storage::disk('local')->put('file.txt', 'Contents');
                Storage::disk('local')->put("/proyect_departamento_planos/".$departamento["name_url_plano"],$departamento["url_plano"]);


//                Le colocamos el nombre con la fecha a url_plano_name
                $departamento["url_plano"]=$departamento["url_plano"]->getClientOriginalName();
                $filename = pathinfo($departamento["url_plano"], PATHINFO_FILENAME);
                $extension = pathinfo($departamento["url_plano"], PATHINFO_EXTENSION);
                $departamento["url_plano"]= $filename ."_". date("Y-m-d_H:i:s")."." . $extension; // 'qwe jpg'

                //Le colocamos el nombre original a url plano

                Vendedores::create($departamento);

                //Si esta vacio crea
            }else{
                $departamento["$idprecio_name"]=$idprecio;
                Vendedores::update($departamento);
                //Si no esta vacio actualiza
            }
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vendedores  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Vendedores $project)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vendedores  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Vendedores $project)
    {
        $clients = $this->c_r->list_select('name','id');

        $data = compact('clients','project');

        return view('cms/project/project', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vendedores  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vendedores $project)
    {
        $data = $request->all();

        $last_id =  $project->id;

        //Setting things for front_image

        if(!Storage::disk('local')->exists('projects/'.$request->front_image)) {

            $img_project_name = $request->file('front_image')->hashName();

            if ($request->hasFile('front_image')) {

                Storage::delete("projects/" . $project->front_image);


                $request->file('front_image')->storeAs(

                    'projects', $img_project_name

                );

            }

            $data["front_image"] = $img_project_name;

        }else{

            $data["front_image"] = $request->front_image_t;

        }



        //Setting things for back_image

        if(!Storage::disk('local')->exists('projects_bg/'.$request->back_image)){

            $img_project_name= $last_id.$request->file('back_image')->hashName();

        if( $request->hasFile('back_image') ) {

            Storage::delete("projects_bg/" . $project->back_image);

            $path = $request->file('back_image')->storeAs(

                'projects_bg', $img_project_name

            );

          }
            $data["back_image"]=$img_project_name;
        }else{

            $data["back_image"] = $request->back_image_t;

        }





        $this->r_p->update($project->id,$data);

        $request->session()->flash('mensaje','Vendedoreso Modificado Correctamente');

        $last_id = $project->id;

        return redirect('cms/projects/'.$last_id.'/edit');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vendedores  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendedores $project)
    {
        $this->r_p->delete($project->id);

        Session::flash("mensaje", "Vendedoreso eliminado");

        return redirect('cms/projects/');
    }



    /**
     * Return a sluged version of a wird
     *
     * @param  \App\Vendedores  $project
     * @return \Illuminate\Http\Response
     */


}
