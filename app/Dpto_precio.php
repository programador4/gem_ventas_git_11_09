<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Dpto_precio extends Model {
    use SoftDeletes;

    protected $table = 'dpto_precio';
    protected $primaryKey = 'iddpto_precio';
    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = [
        'created_at',
        'updated_at',
        'deleted_at',
        'area_inicio',
        'area_fin',
        'piso_inicio',
        'piso_fin',
        'precio_area',
        'estado',
        'precios_idprecios',
        'tipo_departamento_idtipo_departamento',
        'orden'

    ];


}
