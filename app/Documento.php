<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Documento extends Model {
    use SoftDeletes;

    protected $table = 'documentos';
    protected $primaryKey = 'iddocumentos';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = [
        'created_at',
        'updated_at',
        'deleted_at',
        'tipo',
        'nombre',
        'url_plano',
        'estado',
        'proyectos_idproyectos',
        'name_url_plano'
    ];

}
