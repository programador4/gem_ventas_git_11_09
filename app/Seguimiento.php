<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Seguimiento extends Model {

	protected $table = 'seguimiento';
	protected $primaryKey = 'idseguimiento';

	protected $dates = ['deleted_at'];
	/**
	 * @var array
	 */
	protected $fillable = [
		'created_at',
		'updated_at',
		'detalle',
        'fecha_inicio',
		'fecha_atencion',
		'estado',
		'visitas_idvisitas',
		'vendedores_idvendedores',
	];

}
