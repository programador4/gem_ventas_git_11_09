<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Deposito extends Model {
    use SoftDeletes;

    protected $table = 'depositos';
    protected $primaryKey = 'iddepositos';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['created_at','updated_at','deleted_at','deposito','area_techada','dpto_asociado','precio_venta','estado','piso_idpiso','departamentos_iddepartamentos','proyectos_idproyectos'];
}
