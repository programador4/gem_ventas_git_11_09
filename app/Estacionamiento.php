<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Estacionamiento extends Model {
    protected $table = 'estacionamiento';
    protected $primaryKey = 'idestacionamiento';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['created_at','updated_at','deleted_at','estacionamiento','area_libre','area_techada','tipo_estacionamiento','estacionamientocol','area_total','precio_venta','estado','piso_idpiso','proyectos_idproyectos'
    ,'disponibilidad','dscto_estacionamiento','url_plano'];

}
