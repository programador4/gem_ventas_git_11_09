<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Piso extends Model {
    use SoftDeletes;

    protected $table = 'piso';
    protected $primaryKey = 'idpiso';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['created_at','updated_at','deleted_at','nombre','descripcion','estado','flg_departamento', 'flg_estacionmiento', 'flg_deposito', 'orden'];

}
