<?php
namespace App\Repositories;

use App\Models\Project;

use Illuminate\Http\Request;

class ProjectRepository implements RepositoryInterface
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentProject constructor.
     *
     * param App\Models\Project $model
     */
    public function __construct(Project $project)
    {
        $this->model = $project;
    }

    /**
     * Get all projects.
     *
     *  Illuminate\Database\Eloquent\Collection
*/
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get all actived projects.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getAllactived()
    {
        return $this->model->where('is_active', 1)->get();
    }




    /**
     * Get project by id.
     *
     * @param integer $id
     *
     * return App\Project
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new project.
     *
     * @param array $attributes
     *
     * return App\Project
     */
    public function save(array $attributes)
    {



        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }

        return $this->model->create($attributes);


    }

    /**
     * Update a project.
     *
     * @param integer $id
     * @param array $attributes
     *
     * return App\Project
     */
    public function update($id, array $attributes)
    {

        $project=$this->model->find($id);


        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }


        $project->update($attributes);

        return $project;
    }


    /**
     * Delete a project.
     *
     * @param integer $id
     *
     * return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }
}