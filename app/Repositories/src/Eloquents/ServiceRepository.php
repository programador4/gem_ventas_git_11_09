<?php
namespace App\Repositories;

use App\Models\Service;

use Illuminate\Http\Request;

class ServiceRepository implements RepositoryInterface
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentService constructor.
     *
     * param App\Models\Service $model
     */
    public function __construct(Service $service)
    {
        $this->model = $service;
    }

    /**
     * Get all projects.
     *
     *  Illuminate\Database\Eloquent\Collection
     */


    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get all actived projects.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getAllActived()
    {
        return $this->model->all()->where('is_active', 1);
    }

    /**
     * Get first activated proyect.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getFirstActived()
    {
        return $this->model->first()->where('is_active', 1)->first();
    }


    /**
     * Get project by id.
     *
     * @param integer $id
     *
     * return App\Service
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new project.
     *
     * @param array $attributes
     *
     * return App\Service
     */
    public function save(array $attributes)
    {



        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }

        return $this->model->create($attributes);


    }

    /**
     * Update a project.
     *
     * @param integer $id
     * @param array $attributes
     *
     * return App\Service
     */
    public function update($id, array $attributes)
    {

        $service=$this->model->find($id);


        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }


        $service->update($attributes);

        return $service;
    }


    /**
     * Detecta si una contresa ya existe o si esta vacio
     * Caso sea correcto te devuelve el password sino devuelve el anterior
     * @param string $password
     *
     *
     * return App\Service
     */

    /**
     * Delete a project.
     *
     * @param integer $id
     *
     * return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }


    public function list_select($name,$id)
    {
        return $this->model->pluck($name, $id);

    }

}