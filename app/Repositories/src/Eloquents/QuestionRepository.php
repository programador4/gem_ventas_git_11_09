<?php
namespace App\Repositories;

use App\Models\Question;

use Illuminate\Http\Request;

class QuestionRepository implements RepositoryInterface
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentQuestion constructor.
     *
     * param App\Models\Question $model
     */
    public function __construct(Question $question)
    {
        $this->model = $question;
    }

    /**
     * Get all projects.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get project by id.
     *
     * @param integer $id
     *
     * return App\Question
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new project.
     *
     * @param array $attributes
     *
     * return App\Question
     */
    public function save(array $attributes)
    {



        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }

        return $this->model->create($attributes);


    }

    /**
     * Update a project.
     *
     * @param integer $id
     * @param array $attributes
     *
     * return App\Question
     */
    public function update($id, array $attributes)
    {

        $question=$this->model->find($id);


        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }


        $question->update($attributes);

        return $question;
    }


    /**
     * Detecta si una contresa ya existe o si esta vacio
     * Caso sea correcto te devuelve el password sino devuelve el anterior
     * @param string $password
     *
     *
     * return App\Question
     */

    /**
     * Delete a project.
     *
     * @param integer $id
     *
     * return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }


    public function list_select($name,$id)
    {
        return $this->model->pluck($name, $id);

    }

}