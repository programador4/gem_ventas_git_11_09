<?php
namespace App\Repositories;

use App\Models\Category;

use Illuminate\Http\Request;

class CategoryRepository implements RepositoryInterface
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentCategory constructor.
     *
     * param App\Models\Category $model
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }

    /**
     * Get all categories.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get all actived categories.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getAllActived()
    {
        return $this->model->where("is_active",1)->get();
    }


    /**
     * Get project by id.
     *
     * @param integer $id
     *
     * return App\Category
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Get project by id.
     *
     * @param integer $id
     *
     * return App\Category
     */
    public function getBySlug($slug)
    {

        return $this->model->where("slug",$slug)->first();
    }
    /**
     * Get projects by id or fail.
     *
     * @param integer $id
     *
     * return App\Category
     */
    public function getBySlugOrFail($slug)
    {

        return $this->model->where("slug",$slug)->firstOrFail();
    }


    /**
     * Create a new project.
     *
     * @param array $attributes
     *
     * return App\Category
     */
    public function save(array $attributes)
    {



        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }

        return $this->model->create($attributes);


    }

    /**
     * Update a project.
     *
     * @param integer $id
     * @param array $attributes
     *
     * return App\Category
     */
    public function update($id, array $attributes)
    {

        $category=$this->model->find($id);


        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }


        $category->update($attributes);

        return $category;
    }


    /**
     * Detecta si una contresa ya existe o si esta vacio
     * Caso sea correcto te devuelve el password sino devuelve el anterior
     * @param string $password
     *
     *
     * return App\Category
     */

    /**
     * Delete a project.
     *
     * @param integer $id
     *
     * return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }

    public function list_select($name,$id)
    {
        return $this->model->pluck($name, $id);

    }

//        public function getArticlesFromCategory($page,$category_slug)
//    {
//        return $this->model->articles()->find($category_slug);
//
//    }

}