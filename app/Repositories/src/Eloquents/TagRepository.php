<?php
namespace App\Repositories;

use App\Models\Tag;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TagRepository implements RepositoryInterface
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentTag constructor.
     *
     * param App\Models\Tag $model
     */
    public function __construct(Tag $tag)
    {
        $this->model = $tag;
    }

    /**
     * Get all projects.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get project by id.
     *
     * @param integer $id
     *
     * return App\Tag
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new project.
     *
     * @param array $attributes
     *
     * return App\Tag
     */
    public function save(array $attributes)
    {


        $attributes['id_user']=Auth::id();

        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }

        return $this->model->create($attributes);


    }

    /**
     * Update a project.
     *
     * @param integer $id
     * @param array $attributes
     *
     * return App\Tag
     */
    public function update($id, array $attributes)
    {

        $attributes['id_user']=Auth::id();

        $tag=$this->model->find($id);


        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }


        $tag->update($attributes);

        return $tag;
    }


    /**
     * Detecta si una contresa ya existe o si esta vacio
     * Caso sea correcto te devuelve el password sino devuelve el anterior
     * @param string $password
     *
     *
     * return App\Tag
     */

    /**
     * Delete a project.
     *
     * @param integer $id
     *
     * return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }


    public function list_select($name,$id)
    {
        return $this->model->pluck($name, $id);

    }

    public function getBySlugOrFail($slug)
    {    //dd($slug);
        return $this->model->where(['slug' => $slug, 'is_active' => 1])->firstOrFail();
        /*   ['slug', '=', $slug],
    ['is_active', '=', 1],
         ['slug' => $slug, 'is_active' => 1*/

    }

    public function getBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();

    }

    public function getAllActivedPaginated($page)
    {
        return $this->model->where('is_active',1)->paginate($page);

    }

    public function getAllActived()
    {
        return $this->model->where('is_active',1);

    }

//    public function getbyCategoryActivedPaginated($page,$category_id)
//    {
//        return $this->model->where(([
//            ['is_active', '=', '1']
//        ]))->paginate($page);
//
//    }


}