<?php
namespace App\Repositories;

use App\Models\SocialNetwork;

use Illuminate\Http\Request;

class SocialNetworkRepository implements RepositoryInterface
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentSocialNetwork constructor.
     *
     * param App\Models\SocialNetwork $model
     */
    public function __construct(SocialNetwork $socialNetwork)
    {
        $this->model = $socialNetwork;
    }

    /**
     * Get all projects.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get project by id.
     *
     * @param integer $id
     *
     * return App\SocialNetwork
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new project.
     *
     * @param array $attributes
     *
     * return App\SocialNetwork
     */
    public function save(array $attributes)
    {



        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }

        return $this->model->create($attributes);


    }

    /**
     * Update a project.
     *
     * @param integer $id
     * @param array $attributes
     *
     * return App\SocialNetwork
     */
    public function update($id, array $attributes)
    {

        $socialNetwork=$this->model->find($id);


        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }


        $socialNetwork->update($attributes);

        return $socialNetwork;
    }


    /**
     * Detecta si una contresa ya existe o si esta vacio
     * Caso sea correcto te devuelve el password sino devuelve el anterior
     * @param string $password
     *
     *
     * return App\SocialNetwork
     */

    /**
     * Delete a project.
     *
     * @param integer $id
     *
     * return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }


    public function list_select($name,$id)
    {
        return $this->model->pluck($name, $id);

    }

}