<?php
namespace App\Repositories;

use App\Models\Article;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleRepository implements RepositoryInterface
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentArticle constructor.
     *
     * param App\Models\Article $model
     */
    public function __construct(Article $article)
    {
        $this->model = $article;
    }

    /**
     * Get all projects.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get project by id.
     *
     * @param integer $id
     *
     * return App\Article
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new project.
     *
     * @param array $attributes
     *
     * return App\Article
     */
    public function save(array $attributes)
    {


        $attributes['id_user']=Auth::id();

        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }

        return $this->model->create($attributes);


    }

    /**
     * Update a project.
     *
     * @param integer $id
     * @param array $attributes
     *
     * return App\Article
     */
    public function update($id, array $attributes)
    {

        $attributes['id_user']=Auth::id();

        $article=$this->model->find($id);


        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }


        $article->update($attributes);

        return $article;
    }


    /**
     * Detecta si una contresa ya existe o si esta vacio
     * Caso sea correcto te devuelve el password sino devuelve el anterior
     * @param string $password
     *
     *
     * return App\Article
     */

    /**
     * Delete a project.
     *
     * @param integer $id
     *
     * return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }


    public function list_select($name,$id)
    {
        return $this->model->pluck($name, $id);

    }

    public function find_or_fail_slug($slug)
    {
        return $this->model->where('slug', $slug)->firstOrFail();

    }

    public function getBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();

    }
    public function getAllActived()
    {
        return $this->model->where('is_active',1);

    }

    public function getAllActivedPaginated($page)
    {
        return $this->model->where('is_active',1)->paginate($page);

    }

//    public function getbyCategoryActivedPaginated($page,$category_id)
//    {
//        return $this->model->where(([
//            ['is_active', '=', '1']
//        ]))->paginate($page);
//
//    }


}