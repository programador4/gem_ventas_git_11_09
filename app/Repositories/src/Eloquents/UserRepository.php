<?php
namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;

class UserRepository implements RepositoryInterface
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentTask constructor.
     *
     * param App\Models\User $model
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * Get all tasks.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get task by id.
     *
     * @param integer $id
     *
     * return App\Task
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new task.
     *
     * @param array $attributes
     *
     * return App\Task
     */
    public function save(array $attributes)
    {



        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }

        return $this->model->create($attributes);


    }

    /**
     * Update a task.
     *
     * @param integer $id
     * @param array $attributes
     *
     * return App\Task
     */
    public function update($id, array $attributes)
    {

        $user=$this->model->find($id);


        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }


        $user->update($attributes);

        return $user;
    }


    /**
     * Detecta si una contresa ya existe o si esta vacio
     * Caso sea correcto te devuelve el password sino devuelve el anterior
     * @param string $password
     *
     *
     * return App\Task
     */
    public function checkPassword($password,$user)
    {

        $last_password=$user->password;

        if($password == ""){
            return $last_password;
        }else if(Hash::check($password, $last_password)){
            return $last_password;
        }else{
            return bcrypt($password);
        }

    }

    /**
     * Delete a task.
     *
     * @param integer $id
     *
     * return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }
}