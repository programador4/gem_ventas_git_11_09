<?php
namespace App\Repositories;

use App\Models\Usertype;
use Illuminate\Http\Request;

class UserTypeRepository implements RepositoryInterface
{
    /**
     * @var $model
     */
    private $model;

    /**
     * EloquentTask constructor.
     *
     * param App\Models\User $model
     */
    public function __construct(Usertype $usertype)
    {
        $this->model = $usertype;
    }

    /**
     * Get all tasks.
     *
     *  Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * Get task by id.
     *
     * @param integer $id
     *
     * return App\Task
     */
    public function getById($id)
    {
        return $this->model->find($id);
    }

    /**
     * Create a new task.
     *
     * @param array $attributes
     *
     * return App\Task
     */
    public function save(array $attributes)
    {
        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }

        return $this->model->create($attributes);
    }

    /**
     * Update a task.
     *
     * @param integer $id
     * @param array $attributes
     *
     * return App\Task
     */
    public function update($id, array $attributes)
    {
        $user=$this->model->find($id);

        if (!(array_key_exists("is_active", $attributes))) {

            $attributes["is_active"] = '';

        }else{

            $attributes["is_active"] = 1;

        }

        $user->update($attributes);

        return $user;

    }

    /**
     * Delete a task.
     *
     * @param integer $id
     *
     * return boolean
     */
    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Usertype  $usertype
     * @return \Illuminate\Http\Response
     */
    public function list_select($name,$id)
    {
        return $this->model->pluck($name, $id);

    }
}