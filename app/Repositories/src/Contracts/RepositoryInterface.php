<?php

namespace App\Repositories;

interface RepositoryInterface
{
    function getAll();

    function getById($id);

    function save(array $attributes);

    function update($id, array $attributes);

    function delete($id);

}