<?php

namespace App\Models;

use App\Http\Controllers\Controller;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Model
{

        use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="usuario";
    protected $primaryKey = 'idusuario';
    protected $fillable = [
        'created_at',
        'updated_at',
        'deleted_at',
        'correo',
        'nombre',
        'apellido',
        'password',
        'estado',
        'tipo_usuario_idtipo_usuario',
        'telefono'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    protected $dates = ['deleted_at'];

    public function UserType()
    {
        return $this->belongsTo('App\Models\Usertype', 'id_user_type');
    }
}
