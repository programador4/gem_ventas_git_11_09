<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visitas_detalle extends Model {

		use SoftDeletes;
		
		protected $table = 'visitas_detalle';
		protected $primaryKey = 'idvisitas';
		protected $fillable = [
			'created_at',
			'updated_at',
			'deleted_at',
			'dni',
			'nombres',
			'apellidos',
			'correo',
			'telefono',
			'fecha',
			'interesado',
			'financiamiento',
			'cotizacion',
			'distrito_iddistrito',
            'proyectos_idproyectos',
			'departamentos_iddepartamentos',
			'estacionamiento_idestacionamiento',
			'depositos_iddepositos',
			'origen_idorigen',
            'nro_banos',
            'nro_dormitorios',
            'cuarto_servicio',
            'm2'
		];

		protected $dates = ['deleted_at'];
}
