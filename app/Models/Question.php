<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $query
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class Question extends Model
{
    /**
     * @var array
     */

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'phone', 'email', 'question'];

}
