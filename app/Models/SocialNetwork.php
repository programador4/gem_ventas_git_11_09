<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

/**
 * @property User $user
 * @property Testimony[] $testimonies
 * @property User[] $users
 * @property int $id
 * @property int $id_user
 * @property string $name
 * @property string $logo
 * @property boolean $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class SocialNetwork extends Model
{


    /**
     * The table associated with the model.
     * 
     * @var string
     */
    use SoftDeletes;

    protected $table = 'social_networks';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['id_user', 'name', 'logo', 'is_active', 'created_at', 'updated_at', 'deleted_at','color_hex'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testimonies()
    {
        return $this->hasMany('App\Models\Testimony', 'id_social_network');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany('App\User', 'id_social_network');
    }
}
