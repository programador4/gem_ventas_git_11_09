<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Usertype extends Model
{
    use SoftDeletes;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tipo_usuario';

    protected $primaryKey= 'idtipo_usuario';

    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $fillable = [
        'created_at',
        'updated_at',
        'deleted_at',
        'nombre',
        'descripcion',
        'estado',
    ];


}
