<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property User $user
 * @property Article[] $articles
 * @property int $id
 * @property int $id_user
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class Tag extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    use SoftDeletes;

    protected $table = 'tag';

    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $fillable = ['id_user','name','is_active','slug'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articles()
    {
        return $this->belongsToMany('App\Models\Article', 'tag_article');
    }
}
