<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property Client $client
 * @property SocialNetwork $socialNetwork
 * @property int $id
 * @property int $id_client
 * @property int $id_social_network
 * @property string $author
 * @property string $position
 * @property string $testimony
 * @property string $ur_social_network
 * @property boolean $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class Testimony extends Model
{

    use SoftDeletes;
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'testimony';

    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $fillable = ['id_client', 'id_social_network', 'author', 'position', 'testimony', 'url_social_network', 'deleted_at','is_active'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function socialNetwork()
    {
        return $this->belongsTo('App\Models\SocialNetwork', 'id_social_network');
    }
}
