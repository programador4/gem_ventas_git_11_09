<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property User $user
 * @property Project[] $projects
 * @property int $id
 * @property int $user_id
 * @property string $canvas_front
 * @property string $name
 * @property string $subtitle_activity
 * @property string $canvas_description
 * @property string $activity_description
 * @property boolean $is_actived
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class Service extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'canvas_front', 'name', 'subtitle_service', 'subtitle_activity', 'canvas_description',
        'activity_description', 'is_active','color_hex','slug'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects()
    {
        return $this->belongsToMany('App\Project', 'services_projects', 'activities_id');
    }
}
