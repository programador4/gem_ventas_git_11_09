<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property User $user
 * @property Project[] $projects
 * @property Testimony[] $testimonies
 * @property int $id
 * @property int $id_user
 * @property string $web
 * @property string $name
 * @property string $logo
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property boolean $is_active
 */
class Client extends Model
{
    /**
     * @var array
     */
    use SoftDeletes;

    protected $fillable = ['id_user', 'web', 'name', 'logo', 'is_active'];


    protected $dates = ['deleted_at'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany('App\Project', 'id_client');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function testimonies()
    {
        return $this->hasMany('App\Testimony', 'id_client');
    }
}
