<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property User $user
 * @property Category[] $categories
 * @property Tag[] $tags
 * @property int $id
 * @property int $id_user
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property boolean $is_active
 * @property string $principal_image
 */
class Article extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */

    use SoftDeletes;

    protected $table = 'article';

    protected $dates = ['deleted_at'];

    /**
     * @var array
     */
    protected $fillable = ['id_user', 'title', 'description', 'content', 'is_active', 'keywords','front_image','slug'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
//
//    public function roles()
//    {
//        return $this->belongsToMany('App\Role');
//    }
    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }



    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('App\Models\Tag', 'tag_article');
    }
}
