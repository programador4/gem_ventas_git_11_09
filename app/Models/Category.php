<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property User $user
 * @property Article[] $articles
 * @property int $id
 * @property int $id_user
 * @property string $name
 * @property string $created_at
 * @property string $deleted_at
 * @property string $updated_at
 */
class Category extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    use SoftDeletes;

    protected $table = 'category';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['id_user', 'name','is_active','slug'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'id_user');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function articles()
    {
        return $this->belongsToMany('App\Models\Article','article_category');
    }
}
