<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * @property Client $client
 * @property User $user
 * @property Service[] $services
 * @property int $id
 * @property int $id_client
 * @property int $id_user
 * @property string $name
 * @property boolean $is_active
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $front_image
 * @property string $web
 * @property string $keywords
 * @property string $metatags_seo
 */
class Project extends Model
{
    /**
     * The table associated with the model.
     * s
     * @var string
     */
    use SoftDeletes;

    protected $table = 'proyectos';
    protected $primaryKey = 'idproyectos';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['created_at','updated_at','deleted_at','nombre','direccion','estado_proyecto','estado','banco_idbanco','distrito_iddistrito','flg_departamento','flg_estacionmiento','flg_deposito','url_plano','legales'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */




    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
//    public function user()
//    {
//        return $this->belongsTo('App\User', 'id_user');
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */

}
