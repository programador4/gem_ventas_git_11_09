<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estacio_precio extends Model {
    use SoftDeletes;

    protected $table = 'estacio_precio';
    protected $primaryKey = 'idestacio_precio';
    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = [
        'idestacio_precio',
        'created_at',
        'updated_at',
        'deleted_at',
        'piso_id',
        'tipo',
        'precio',
        'precios_idprecios',
        'piso_idpiso',
        'id_proyecto'
    ];

}
