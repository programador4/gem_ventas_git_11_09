<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;
    use SoftDeletes;
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
    protected $table="usuario";
    protected $primaryKey = 'idusuario';
    protected $fillable = [
        'created_at',
        'updated_at',
        'deleted_at',
        'correo',
        'nombre',
        'apellido',
        'password',
        'estado',
        'tipo_usuario_idtipo_usuario',
        'telefono'
    ];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

}
