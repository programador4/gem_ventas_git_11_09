<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Departamento extends Model {
    use SoftDeletes;

    protected $table = 'departamentos';
    protected $primaryKey = 'iddepartamentos';

    protected $dates = ['deleted_at'];
    /**
     * @var array
     */
    protected $fillable = ['created_at','updated_at','deleted_at','departamento',
        'area_libre','area_techada','nro_habitacio','nro_banos','area_total',
        'precio_venta','estado','proyectos_idproyectos','piso','tipo_departamento_idtipo_departamento','url_plano'];

}
