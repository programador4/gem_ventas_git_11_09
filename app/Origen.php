<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class origen extends Model {

	protected $table = 'origen';
	protected $primaryKey = 'idorigen';

	protected $dates = ['deleted_at'];
	/**
	 * @var array
	 */
	protected $fillable = ['created_at','nombre'];

}
