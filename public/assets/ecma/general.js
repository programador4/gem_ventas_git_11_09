$( document ).ready( function(){
  
    $('.icon').on('click', function(){
        $(this).toggleClass('active');
    });
    $(".control").click(function() {
        $(".lista").toggleClass("menu-abrir");
    });
    
    $( ".menu .enlace" ).on("click", function( evt ){
        $( "#control-menu" ).prop( "checked", false);
    });


	$( ".formulario" )
        .find( "input[type=text], input[type=tel], input[type=tel]" )
        .on( "keypress", function( evt ) {
            var $c = $(evt.currentTarget);
            var caracter, anterior, futuro, patron;
            var chc = evt.charCode;
            if ( chc > 0 ) {
                caracter = String.fromCharCode( chc );
                anterior = $c.val();
                patron = new RegExp( $c.data("restriccion") );
                futuro = anterior + caracter;
                if ( patron.test( futuro ) != true ) {
                    evt.preventDefault();
                }
            }
        });
 
  var dom = dom || document;
var datos;

window.addEventListener( "load", evt => {
    datos = datos || dom.forms[0];

    var quill = new Quill( '.editor', {
        theme: 'snow',
        modules: {
            toolbar: [
                [{ header: [1, 2, false] }],
                ['bold', 'italic', 'underline'],
                ['link', 'blockquote', 'image'],
                [{ list: 'ordered' }, { list: 'bullet' }]
            ]
        },
        scrollingContainer: '.contenidos', 
        placeholder: "Texto de la noticia"
    });

    datos.addEventListener( "submit", evt =>{
        var editor = datos.querySelector( ".ql-editor" );
        datos["contenido"].value = editor.innerHTML;
    });
});


   
});


