$("#proyectos_idproyectos").on("change",function () {
    $("#departamentos").empty();
    $("#estacionamientos").empty();
    $("#depositos").empty();
        var idproyectos=$("#proyectos_idproyectos").val();
        console.log(idproyectos);
        var get_departamentos=$.ajax({
            type: "GET",
            url:  site_url+"/proyect_departamentos",
            data: {id:idproyectos},
            // contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            // processData: false, // NEEDED, DON'T OMIT THIS
        });
        var get_estacionamiento=$.ajax({
        type: "GET",
        url:  site_url+"/proyect_estacionamiento",
        data: {id:idproyectos},
        // contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        // processData: false, // NEEDED, DON'T OMIT THIS
    });
        var get_depositos=$.ajax({
            type: "GET",
            url:  site_url+"/proyect_depositos",
            data:  {id:idproyectos},
            // contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            // processData: false, // NEEDED, DON'T OMIT THIS
        });
        $.when( get_departamentos,get_estacionamiento,get_depositos ).done(function( departamentos,estacionamientos,depositos) {
            departamentos=JSON.parse(JSON.stringify(departamentos));
            departamentos=JSON.parse(departamentos[0]);
            console.log(departamentos)

            estacionamientos=JSON.parse(JSON.stringify(estacionamientos));
            estacionamientos=JSON.parse(estacionamientos[0]);
            console.log(estacionamientos);

            depositos=JSON.parse(JSON.stringify(depositos));
            depositos=JSON.parse(depositos[0]);
            console.log(depositos);


            $("#departamentos").append("<option value=''>Seleccione departamento</option>")
            // <option>Seleccione departamento</option>
            $.each(departamentos, function( index, departamento ) {

                $("#departamentos").append("<option value='"+departamento.iddepartamentos+"'>"+departamento.departamento+"</option>")
            });


            $("#estacionamientos").append("<option value=''>Seleccione estacionamiento</option>")
            $.each(estacionamientos, function( index, estacionamiento ) {

                $("#estacionamientos").append("<option value='"+estacionamiento.idestacionamiento+"'>"+estacionamiento.estacionamiento+"</option>")
            });


            $("#depositos").append("<option value=''>Seleccione deposito</option>")
            $.each(depositos, function( index, deposito ) {

                $("#depositos").append("<option value='"+deposito.iddepositos+"'>"+deposito.deposito+"</option>")
            });


            });

})

$("#seguimiento_table").on("click",".btn_finalizar",function () {
$cumple     =$(this).closest("tr").find("[row_name=cumple]");
console.log($cumple);
$cumple_span=$cumple.closest("td").find("span");
console.log($cumple_span.val());
    console.log($cumple_span.text());
console.log("Edit");
if($cumple.val()==""){
    $cumple.val(1);
    $cumple_span.text("Si");
}
if($cumple.val()==1){
    $cumple.val("");
    $cumple_span.text("No");
}
})