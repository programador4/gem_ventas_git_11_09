$( document ).ready( function(){

    tinymce.init({
        selector:'textarea.content',
        height: 250,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });

    tinymce.init({
        selector:'textarea.description',
        height: 250,
        theme: 'modern',
        plugins: [
            'advlist autolink lists link image charmap print preview hr anchor pagebreak',
            'searchreplace wordcount visualblocks visualchars code fullscreen',
            'insertdatetime media nonbreaking save table contextmenu directionality',
            'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
        ],
        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        toolbar2: 'print preview media | forecolor backcolor emoticons | codesample',
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '//www.tinymce.com/css/codepen.min.css'
        ]
    });






    $('input[name=front_image]').change(function(){
        $("#front_image_t").val("");
    });





    /*Para categorias*/
    $('#group-categoria').multipleSelect({
        placeholder: "Categorias",
        selectAll: "Seleccionados todos",
        selectAllText: "Seleccionados todos",
        allSelected: "Todos seleccionados" ,
        countSelected: "# de % seleccionados",
        onClick: function(view) {

            var getselects = $('#group-categoria').multipleSelect('getSelects');

            $('#categorias').val(getselects).attr("value", getselects);
        },
        onCheckAll:function(view) {

            var getselects = $('#group-categoria').multipleSelect('getSelects');

            $('#categorias').val(getselects).attr("value", getselects);
        },
        onUncheckAll:function(view) {
            var getselects = $('#group-categoria').multipleSelect('getSelects');

            $('#categorias').val(getselects).attr("value", getselects);
        }


    })


    var categorias_array="";

    var categorias= $('#categorias').val();

    if(categorias=="/"){

        $('#categorias').val("");

    }

    if(categorias.length > 0){

        if(categorias=="/"){
            $('#group-categoria').multipleSelect("uncheckAll");
            $('#categorias').val("");
        }else{
            // console.log(categorias);
            var categorias_array = JSON.parse("[" + categorias + "]");

            $("#group-categoria").multipleSelect("setSelects", categorias_array);
        }


    }else{

    }
/*Para categorias*/



/*Para Tags*/
    $('#group-tag').multipleSelect({
        placeholder: "Tags",
        selectAll: "Seleccionados todos",
        selectAllText: "Seleccionados todos",
        allSelected: "Todos seleccionados" ,
        countSelected: "# de % seleccionados",
        onClick: function(view) {

            var getselects = $('#group-tag').multipleSelect('getSelects');

            $('#tags').val(getselects).attr("value", getselects);
        },
        onCheckAll:function(view) {

            var getselects = $('#group-tag').multipleSelect('getSelects');

            $('#tags').val(getselects).attr("value", getselects);
        },
        onUncheckAll:function(view) {
            var getselects = $('#group-tag').multipleSelect('getSelects');

            $('#tags').val(getselects).attr("value", getselects);
        }


    })


    var tags_array="";

    var tags= $('#tags').val();

    if(tags=="/"){

        $('#tags').val("");

    }

    if(tags.length > 0){

        if(tags=="/"){
            $('#group-tag').multipleSelect("uncheckAll");
            $('#tags').val("");
        }else{
            // console.log(tags);
            var tags_array = JSON.parse("[" + tags + "]");

            $("#group-tag").multipleSelect("setSelects", tags_array);
        }


    }else{

    }
/*Para Tags*/


});

