// START Variables Globales
var id_visita="";
var id_vendedor="";
// END Variables Globales

//START Validate de los formularios
$("#visitas_form_container").validate({
  rules: {
    dni:{
      required:true,
      number:true
    },
    nombres:{
      required:true
    },
    apellidos:{
      required:true
    },
    correo:{
      required:true,
      email:true
    },
    telefono:{
      required:true,
      number:true
    },    
    distrito_iddistrito:{
      required:true,
      number:true
    },
    fecha:{
      required:true
    },
    origen_idorigen:{
      required:true,
      number:true
    },
    interesado:{
      required:true,
      number:true
    },
    proyectos_idproyectos:{
      required:true,
      number:true
    },
    departamentos_iddepartamentos:{
      required:true,
      number:true
    },
    estacionamiento_idestacionamiento:{
      required:true,
      number:true
    },
    depositos_iddepositos:{
      required:true,
      number:true
    },
    financiamiento:{
      required:true,
      number:true
    }
  },
  messages: {
    dni:{
      required:""      
    },
    nombres:{
      required:""
    },
    apellidos:{
      required:""
    },
    correo:{
      required:""
    },
    telefono:{
      required:""
    },    
    distrito_iddistrito:{
      required:""
    },
    fecha:{
      required:""
    },
    origen_idorigen:{
      required:""
    },
    interesado:{
      required:""
    },
    proyectos_idproyectos:{
      required:""
    },
    departamentos_iddepartamentos:{
      required:""
    },
    estacionamiento_idestacionamiento:{
      required:""
    },
    depositos_iddepositos:{
      required:""
    },
    financiamiento:{
      required:""
    },
    cotizacion:{
      required:""
    }
  }
});
//END Validate de los formularios

$("#form_seguimiento").validate({

    rules: {
        fecha_limite:{
            required:true,
        },
        detalle:{
            required:true
        }
    }
});


//JS de los tabs
//Start js tabs
//Initialize tooltips
$('.nav-tabs > li a[title]').tooltip();

//Wizard
$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

  var $target = $(e.target);
  console.log($target);
  if ($target.hasClass('disabled')) {
      return false;
  }
});

$(".next-step").click(function (e) {
  var $active = $('.wizard .nav-tabs .nav-item .active');
  var $activeli = $active.parent("li");

  $($activeli).next().find('a[data-toggle="tab"]').removeClass("disabled");
  $($activeli).next().find('a[data-toggle="tab"]').click();
});


$(".prev-step").click(function (e) {

  var $active = $('.wizard .nav-tabs .nav-item .active');
  var $activeli = $active.parent("li");

  $($activeli).prev().find('a[data-toggle="tab"]').removeClass("disabled");
  $($activeli).prev().find('a[data-toggle="tab"]').click();

});




//Validaciones
$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

$(".visitas_button").on("click",function (e) {
  e.preventDefault();
  VisitasValdiate();
})
function next_button() {
  console.log();
  var $active = $('.wizard .nav-tabs .nav-item .active');
  var $activeli = $active.parent("li");
  $activeli.find('a[data-toggle="tab"]').addClass("disabled");
  $($activeli).next().find('a[data-toggle="tab"]').removeClass("disabled");
  $($activeli).next().find('a[data-toggle="tab"]').click();
}
function VisitasValdiate() {
  if ($('#visitas_form_container').valid()) {
      var visitas_form_container = new FormData($('#visitas_form_container')[0]);
      visitas_form_container.append('idusuario', id_usuario);
          $.ajax({
              type: "POST",
              url:  site_url+"/visitas/"+visita_id,
              data: visitas_form_container,
              contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
              processData: false, // NEEDED, DON'T OMIT THIS,
          }).done(function(id) {
              id_visita=id;

              $("#fecha_inicio_seg").val($("#fecha_reg").val());
              $("#vendedores_idvendedores_seg").val(id_usuario);
              $("#cumple_seg").val("0");
              next_button();

          });

  }
}
$(".crear_cotizacion").on("click",function () {
    crear_cotizacion();
})
function crear_cotizacion() {
    if ($('#visitas_form_container').valid()) {
        var visitas_form_container = new FormData($('#visitas_form_container')[0]);
        visitas_form_container.append('idusuario', id_usuario);
        $.ajax({
            type: "POST",
            url:  site_url+"/crear_cotizacion",
            data: visitas_form_container,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS,
        }).done(function(cotizacion) {
            // site_url+cotizacion;
            // data=data;

            site_url = site_url.replace('public/cms','storage');
            console.log(site_url);
            // window.location=site_url+cotizacion;
            var wnd = window.open(site_url+cotizacion);
            // download.bind(true, "<FILENAME_TO_SAVE_WITH_EXTENSION>", "<FILE_MIME_TYPE>")
            // setTimeout(function() {
            //     wnd.close();
            // }, 5000);
            // return false;
        });

    }
}

$(".precios_button").on("click",function (e) {
    e.preventDefault();
    if(precios_button()){
        create_precio();
        // next_button();
    }
})

//Validaciones generales
  function detect_if_empty(string) {
      if(string==""){
      return false;
      }else{
          return true;
      }
  }


$("table").on("click",".btn_borrar",function () {
    $(this).closest("tr").remove();
});

function adding_tr_td(row_of_inputs,table) {
    $(row_of_inputs+"_t tbody").append("<tr></tr>");
    var $last_tr=$(row_of_inputs+"_t tbody tr").last();
    var last_tr_index=$last_tr.index();
    $last_td=null;
    // $.each($(row_of_inputs+" .campo_llenar"),function (key, value) {
    //Agregamos el texto
    $.each($(row_of_inputs+" .campo_llenar"),function (key, value) {
        $last_td=null;
        $last_tr.append("<td></td>");

        var name=$(value).attr("name");
        var old_name=$(value).attr("name");
        name="row["+last_tr_index+"]["+name+"]";
        $last_td=$last_tr.find("td").last();

        var input=$(value).clone();
        if(input.is( "select" )){
            var $originalSelects=$(value);
            var select=$(value).clone();
            select.each(function(index, item) {
                //set new select to value of old select
                $(item).val( $originalSelects.eq(index).val() );

            });
            input=select;
        }
        input.attr("disabled",true);
        input.addClass(name);
        input.attr("row_name",$(value).attr("name"));
        input.hide();
        input.attr("name",name);
        input.attr("id",old_name+"_"+last_tr_index);

        $last_td.append(input);
        var input_val=input.val();
        if(input.attr("row_name")=="cumple"){
            if(input.val()==0){
                input_val="No"
            }else{
                input_val="Si"
            }
        }
        if(input.attr("row_name")=="vendedores_idvendedores"){
            input_val=name_usuario;
        }
        $last_td.append("<span>"+input_val+"</span>");
        $last_td.find("input,select").removeClass("campo_llenar");

    });
    $last_tr.append("<input type='text' name='row["+last_tr_index+"][id]' style='display: none;'>");
    var html=html+"<td>";
    html=html+"<a class='btn btn_finalizar' href='#'>Finalizar</a>";
    html=html+"</td>";
    $last_tr.append(html);
}



  Add_tr_to_table_pre("#seguimiento","seguimiento");

  function Add_tr_to_table_pre(row_of_inputs,table) {

      $(row_of_inputs+" .agregar_a_tabla").on("click",function () {

          if ($('#form_seguimiento').valid()){
              adding_tr_td(row_of_inputs,table);
          }
      });

  }

        $(".seguimiento_button").on("click",function (e) {
            if ($("#form_seguimiento tbody tr").length > 0) {
                $(this).attr("disabled",true);
                create_seguimiento();
            }else{
                $("#form_seguimiento table").addClass("error");
                console.log("validating ");
            }
        });

        function create_seguimiento(){
            $('#form_seguimiento').find(':input:disabled').removeAttr('disabled');
            $('#form_seguimiento').find('select:disabled').removeAttr('disabled');
            console.log("Enviando Boton");
            var departamentos_form = new FormData($('#form_seguimiento')[0]);
            departamentos_form.append('visitas_idvisitas', id_visita);
            var create_precio_dep=$.ajax({
                type: "POST",
                url:  site_url+"/seguimiento",
                data: departamentos_form,
                contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                processData: false, // NEEDED, DON'T OMIT THIS
            }).done(function(id) {
                console.log("seguimiento form created");

                window.location.replace(site_url+"/visitas");
            });
        }