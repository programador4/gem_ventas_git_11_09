//Variables globales
var test;
var id_projecto="";
var id_precio="";
var rules_departamento;
var depositos;
/*********/
//Valor de venta de departamentos
var depa_valor_venta=0;
var estacionamiento_valor_venta=0;
var deposito_valor_venta=0;

/********/
    //Usadas en la funcion calcular_venta_depa
    //Esta funcion calcula el valor de ventas
    //Calculo de precios
    /********/
    //factor_desc_preventa en formulario tipo de precio
    var factor_desc_preventa;
    //Area libre y area techada esta en tipo de departamento
    var a_libre;
    var a_techada;
    //El array que tienen los tipos de departamento
    var arr_tipo_departamento_lp_matriz=[];
    var arr_tipo_departamento_matriz=[];
    //=C15*$K$18*K21+D15*$K$18


    /********/
    //Calculo de precios
    /********/


//START Validate de los formularios
//Aca empezamos las validaciones de formularios abajo lo llamaremos conla funcion validate();
//form 1)Crear Proyecto
$("#project_form_container").validate({
    rules: {
        nombre:{
            required:true
        },
        distrito_iddistrito:{
            required:true,
            number:true,
        },
        direccion:{
            required:true
        },
        estado_proyecto:{
            required:true,
            number:true
        },
        url_plano:{
            extension: "png|jpeg|jpg|PNG|JPEG|JPG"
        }
    },
    messages: {

        nombre:{
            required:""
        },
        distrito_iddistrito:{
            required:""
        },
        direccion:{
            required:""
        },
        estado_proyecto:{
            required:""
        }
    }
});
//form 2)Tipo Departamentos
$("#form_tipodepartamento").validate({
    rules: {
        nombre:{
            required:true
        },
        inicio_area_maxima:{
            required:true,
            number:true
        },
        fin_area_maxima:{
            required:true,
            number:true
        },
        nro_habitacion:{
            required:true,
            number:true
        },
        nro_banos:{
            required:true,
            number:true
        },
        vista_parque:{
            required:true,
            number:true
        },
        url_plano:{
            required:true,
            extension: "png|jpeg|jpg|PNG|JPEG|JPG"
        }
    },
    messages: {
        url_plano:{
            extension: "Extension erronea ",
        }
    }
})
//Form3 Lista de ventas
    //lp1 general
    $("#lp_general").validate({
        rules: {
            factor_area_libre:{
                required:true,
                number:true
            },
            factor_desc_preventa:{
                required:true,
                number:true,
            },
            factor_incr_entrega:{
                required:true,
                number:true,
            },
            factor_vista_exterior:{
                required:true,
                number:true
            },
            moneda:{
                required:true,
                number:true
            }
        },
        messages: {}
    });
    //lp2 promciones
    $("#lp_promociones").validate({
        rules: {
            promocion_idpromocion:{
                required:true
            }
        },
        messages: {}
    });
    //lp3 departamentos
    $("#lp_departamentos").validate({
        rules: {
            area_inicio:{
                required:true,
                number:true
            },
            area_fin:{
                required:true
            },
            piso_inicio:{
                required:true
            },
            piso_fin:{
                required:true
            },
            precio_area:{
                required:true
            }
        },
        messages: {}
    });
    $("#lp_departamentos").validate({
        rules: {
            piso_id:{
                required:true,
                number:true
            },
            tipo:{
                required:true,
                number:true
            },
            precio:{
                required:true,
                number:true
            }
        },
        messages: {}
    });
    //lp4 depositos
    $("#lp_depositos").validate({
    rules: {
        valor_deposito:{
            required:true
        }
    },
    messages: {}
});
    //     {{--lp4 Estacionamiento--}}
$("#form_departamentos").validate({
    rules: {
        piso_idpiso:{
            required:true
        },
        departamento:{
            required:true
        },
        area_libre:{
            required:true,
            number:true,
        },
        area_techada:{
            required:true,
            number:true
        },
        nro_habitacion:{
            required:true,
            number:true
        },
        nro_banos:{
            required:true,
            number:true
        },
        valor_venta:{
            required:true,
            number:true
        },
        url_plano:{
            required:true,
            extension: "png|jpeg|jpg|PNG|JPEG|JPG"
        }

    },
    messages: {
        url_plano:{
            extension: "Extension erronea ",
        }
    }
});
/*
    extension: "png|jpeg|jpg|PNG|JPEG|JPG",
 filesize: 8
 */
//Form 4) Estacionamientos
$("#form_estacionamiento").validate({
    rules: {
        piso_idpiso:{
            required:true
        },
        estacionamiento:{
            required:true
        },
        area_libre:{
            required:true,
            number:true,
        },
        area_techada:{
            required:true,
            number:true
        },
        tipo_estacionamiento:{
            required:true
        },
        valor_venta:{
            required:true,
            number:true
        }

    },
    messages: {

    }
});
//Form 5) Depositos
$("#form_depositos").validate({
    rules: {
        piso_idpiso:{
            required:true
        },
        deposito:{
            required:true
        },
        area_techada:{
            required:true,
            number:true,
        },
        dpto_asociado:{
            required:true
        },
        precio_venta:{
            required:true,
            number:true
        }

    },
    messages: {

    }
});
//Form 6) Documentos
$("#form_documentos").validate({
    rules: {
        tipo:{
            required:true
        },
        nombre_doc:{
            required:true
        },
        url_plano:{
            required:true,
            extension: "png|jpeg|jpg|PNG|JPEG|JPG|doc|docx|DOC|DOCX|xls|xlsx|XLSX|XLSX"
        }

    },
    messages: {

    }
});
//Form 7) Documentos
$("#form_vendedores").validate({
    rules: {
        tipo:{
            required:true
        },
        nombre_doc:{
            required:true
        },
        archivo:{
            required:true,
            extension: "png|jpeg|jpg|PNG|JPEG|JPG|doc|docx|DOC|DOCX|xls|xlsx|XLSX|XLSX",
            filesize: 8
        }

    },
    messages: {

    }
});
//form_departamentos
//END Validate de los formularios




//JS de los tabs
//Start js tabs
//Initialize tooltips
$('.nav-tabs > li a[title]').tooltip();

//Wizard
$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

    var $target = $(e.target);
    console.log($target);
    if ($target.hasClass('disabled')) {
        return false;
    }
});

$(".next-step").click(function (e) {
    var $active = $('.wizard .nav-tabs .nav-item .active');
    var $activeli = $active.parent("li");

    $($activeli).next().find('a[data-toggle="tab"]').removeClass("disabled");
    $($activeli).next().find('a[data-toggle="tab"]').click();
});


$(".prev-step").click(function (e) {

    var $active = $('.wizard .nav-tabs .nav-item .active');
    var $activeli = $active.parent("li");

    $($activeli).prev().find('a[data-toggle="tab"]').removeClass("disabled");
    $($activeli).prev().find('a[data-toggle="tab"]').click();

});


//Fin js Tabas

//adding tr adding to tables
function adding_tr_td(row_of_inputs,table) {
    $(row_of_inputs+"_t tbody").append("<tr></tr>");
    var $last_tr=$(row_of_inputs+"_t tbody tr").last();
    var last_tr_index=$last_tr.index();
    $last_td=null;
    // $.each($(row_of_inputs+" .campo_llenar"),function (key, value) {
    //Agregamos el texto
    $.each($(row_of_inputs+" .campo_llenar"),function (key, value) {
        $last_td=null;
        $last_tr.append("<td></td>");

        var name=$(value).attr("name");
        var old_name=$(value).attr("name");
        name="row["+last_tr_index+"]["+name+"]";
        $last_td=$last_tr.find("td").last();

        var input=$(value).clone();
        if(input.is( "select" )){
            var $originalSelects=$(value);
            var select=$(value).clone();
            select.each(function(index, item) {
                //set new select to value of old select
                $(item).val( $originalSelects.eq(index).val() );

            });
            input=select;
        }
        input.attr("disabled",true);
        input.attr("name",name);
        // input.attr("id",old_name+"_"+last_tr_index);

        $last_td.append(input);
        $last_td.find("input,select").removeClass("campo_llenar");

    });
    $last_tr.append("<input type='text' name='row["+last_tr_index+"][id]' style='display: none;'>");
    var html=html+"<td>";
    html=html+"<a class='btn btn_editar' href='#'>Editar</a>";
    html=html+"<a class='btn btn_borrar' href='#'>Borrar</a>";
    html=html+"</td>";
    $last_tr.append(html);
}


//adding tr adding to tables
function adding_tr_td__(row_of_inputs,table) {
    $(row_of_inputs+"_t tbody").append("<tr></tr>");
    var $p_ino=document.getElementsByName("piso_inicio");
    var $p_fin=document.getElementsByName("piso_fin");
    var $last_tr=$(row_of_inputs+"_t tbody tr").last();
    var last_tr_index=$last_tr.index();
    $last_td=null;
    // $.each($(row_of_inputs+" .campo_llenar"),function (key, value) {
    //Agregamos el texto
    $.each($(row_of_inputs+" .campo_llenar"),function (key, value) {
        $last_td=null;
        $last_tr.append("<td></td>");

        var name=$(value).attr("name");
        var old_name=$(value).attr("name");
        name="row["+last_tr_index+"]["+name+"]";
        $last_td=$last_tr.find("td").last();

        var input=$(value).clone();
        if(input.is( "select" )){
            var $originalSelects=$(value);
            var select=$(value).clone();
            select.each(function(index, item) {
                //set new select to value of old select
                $(item).val( $originalSelects.eq(index).val() );

            });
            input=select;
        }
        input.attr("disabled",true);
        input.attr("name",name);
        input.attr("id",old_name+"_"+last_tr_index);

        $last_td.append(input);
        $last_td.find("input,select").removeClass("campo_llenar");

    });
    $last_tr.append("<input type='text' name='row["+last_tr_index+"][id]' style='display: none;'>");
    var html=html+"<td>";
    html=html+"<a class='btn btn_editar' href='#'>Editar</a>";
    html=html+"<a class='btn btn_borrar' href='#'>Borrar</a>";
    html=html+"</td>";
    $last_tr.append(html);
}


$("table").on("click",".btn_editar",function () {
    $(this).removeClass("btn_editar");
    $(this).addClass("btn_editando");
    var $tr=$(this).closest("tr");
    $.each($tr.find("input,select"),function (key, value) {
        $(this).rules('add', {required: true, messages: {required: jQuery.validator.format("")}});
    });

    $tr.find("td input").attr("disabled",false);
    $tr.find("td select").attr("disabled",false);
});

$("table").on("click",".btn_editando",function () {
    if ($(this).closest("form").valid()) {
        $(this).removeClass("btn_editando");
        $(this).addClass("btn_editar");

        var $tr = $(this).closest("tr");
        $.each($tr.find("input,select"), function (key, value) {
            $(this).rules('remove', 'required')
        });

        $tr.find("td input").attr("disabled", true);
        $tr.find("td select").attr("disabled", true);
    }
});
$("table").on("click",".btn_borrar",function () {
    $(this).closest("tr").remove();
});



        Add_tr_to_table_tipodepa("#tipodepartamento","tipodepartamento");

        function Add_tr_to_table_tipodepa(row_of_inputs, table){
            $(row_of_inputs+" .agregar_a_tabla").on("click", function() {
        if ($('#form_tipodepartamento').valid()){
            adding_tr_td(row_of_inputs, table);
        }
    });
}



Add_tr_to_table_pre("#preciodepa","preciodepa");

function Add_tr_to_table_pre(row_of_inputs,table) {

        $(row_of_inputs+" .agregar_a_tabla").on("click",function () {

            if ($('#lp_departamentos').valid()){
                adding_tr_td(row_of_inputs,table);
            }
        });

}


Add_tr_to_table_pre_est("#precioesta","precioesta");

function Add_tr_to_table_pre_est(row_of_inputs,table)  {

    $(row_of_inputs+" .agregar_a_tabla").on("click",function () {

        if ($('#lp_estacionamientos').valid()){
            adding_tr_td(row_of_inputs,table);
        }
    });

}


Add_tr_to_table_depa("#departamento","departamento");
//Formulario departamento
//Calculo Interno de venta
/***********************/
function  calcular_venta_depa() {
   var factor_desc_preventa=$("#factor_desc_preventa");
   var arr_departamento_lp_matriz=$("#form_tipodepartamento").serializeArray() ;
   var arr_tipo_departamento_matriz=$("#lp_departamentos").serializeArray() ;
   console.log(factor_desc_preventa);
   console.log(arr_departamento_lp_matriz);
   console.log(arr_tipo_departamento_matriz);

}
/***********************/
function Add_tr_to_table_depa(row_of_inputs,table) {
        $(row_of_inputs+" .agregar_a_tabla").on("click",function () {

            if ($('#form_departamentos').valid()){//start_valid
            /******************/
            var id_td   =$("#tipo_departamento_idtipo_departamento_2").val();
            var piso_fin = $("#piso_fin").val();
            var piso_inicio = $("#piso_inicio").val();
            var array_ventas=[];
            console.log(piso_inicio);
            console.log(piso_fin);
                for (i = piso_inicio; i <= piso_fin; i++) {
                    console.log(i);
            //id_piso
            /******************/
            /******************/
            //AJAXES CALCULO DE VENTA
                var last_tr_length_i=$(row_of_inputs+"_t tbody tr").length;
            //last_tr_length_i
            var ajax_venta_depa=$.ajax({
                method: "POST",
                type:"application/json",
                url:  site_url+"/calcular_precio_venta",
                async:false,
                data:{
                    id_td :       id_td,
                    id_precio :   id_precio,
                    id_proyecto : id_projecto,
                    idpiso :      i
                }
            })
            array_ventas.push(ajax_venta_depa);

            $.when(ajax_venta_depa ).done(function(valor_venta) {

                    // adding_tr_td(row_of_inputs,table);
                    $(row_of_inputs+"_t tbody").append("<tr></tr>");
                    var $last_tr=$(row_of_inputs+"_t tbody tr").last();
                    var last_tr_length=$(row_of_inputs+"_t tbody tr").length;
                    var last_tr_index=$last_tr.index();
                    /*****************************************/
                    $last_td=null;

                    $tipo_departamento=$("#tipo_departamento_idtipo_departamento_2").clone().prop("disabled",true);
                    $tipo_departamento.attr("name","row["+last_tr_index+"][tipo_departamento_idtipo_departamento]");
                    $tipo_departamento.each(function(index, item) {
                        //set new select to value of old select
                        $(item).val( $("#tipo_departamento_idtipo_departamento_2").eq(index).val() );
                    });
                    /*****************************/
                    $last_tr.append($tipo_departamento);
                    $last_tr.append("<input type='text' name='row["+last_tr_index+"][id]' style='display: none;'>");
                    var html="";
                    html=html+"<td>";
                    html=html+"<input type='text' value='"+i+"' name='row["+last_tr_index+"][piso]' disabled >";
                    html=html+"</td>";
                    html=html+"<td>";
                    html=html+"<input type='text' value='"+$("#tipo_departamento_idtipo_departamento_2 option:selected").text()+"_"+($("#departamento_t tbody tr").length)+"'" +
                    "name='row["+last_tr_index+"][departamento]' disabled >";
                    html=html+"</td>";
                    html=html+"<td>";
                    html=html+"<input type='text' value='"+valor_venta+"' name='row["+last_tr_index+"][valor_venta]' disabled >";
                    html=html+"</td>";
                    html=html+"<td>";
                    html=html+"<input type='text' value='"+($("#departamento_t tbody tr").length)+"' name='row["+last_tr_index+"][orden]' disabled >";
                    html=html+"</td>";
                    html=html+"<td>";
                    html=html+"<a class='btn btn_editar' href='#'>Editar</a>";
                    html=html+"<a class='btn btn_borrar' href='#'>Borrar</a>";
                    html=html+"</td>";
                    $last_tr.append(html);


                  });
                  /******************/

                }//End for

                $.when.apply(this,array_ventas).done(function() {

                    // console.log(arguments); //it is an array like object which can be looped
                    var total = 0;
                    $.each(array_ventas, function (i, data) {
                        console.log(data.responseText); //data is the value returned by each of the ajax requests

                        // total += data[0]; //if the result of the ajax request is a int value then
                    });

                    // console.log(total)
                })


            }//end valid
        });

}

Add_tr_to_table_est("#estacionamiento","estacionamiento");

function Add_tr_to_table_est(row_of_inputs,table) {
    $(row_of_inputs+" .agregar_a_tabla").on("click",function () {

        if ($('#form_estacionamiento').valid()){
            /*********************************/
            $.ajax({
                method: "POST",
                type:"application/json",
                url:  site_url+"/calcular_precio_venta_estacionamiento",
                async:false,
                data:{
                    tipo :          $("#tipo_estacionamiento").val(),
                    piso_idpiso :   $("#piso_estacionamiento").val(),
                    id_proyecto :   id_projecto,
                }
            }).done(function (precio) {//Start done
            if($("#is_dscto_estacionamiento:checked").length > 0){
                precio=precio-$("#dscto_estacionamiento").val();
            }

            /**************************/
            $(row_of_inputs+"_t tbody").append("<tr></tr>");
            var $last_tr=$(row_of_inputs+"_t tbody tr").last();
            var last_tr_index=$last_tr.index();
            $last_td=null;
            // $.each($(row_of_inputs+" .campo_llenar"),function (key, value) {
            //Agregamos el texto
            $.each($(row_of_inputs+" .campo_llenar"),function (key, value) {
                $last_td=null;
                $last_tr.append("<td></td>");

                var name=$(value).attr("name");
                var old_name=$(value).attr("name");
                name="row["+last_tr_index+"]["+name+"]";
                $last_td=$last_tr.find("td").last();

                var input=$(value).clone();
                if(input.is( "select" )){
                    var $originalSelects=$(value);
                    var select=$(value).clone();
                    select.each(function(index, item) {
                        //set new select to value of old select
                        $(item).val( $originalSelects.eq(index).val() );

                    });
                    input=select;
                }
                input.attr("disabled",true);
                input.attr("name",name);
                input.attr("id",old_name+"_"+last_tr_index);

                $last_td.append(input);
                $last_td.find("input,select").removeClass("campo_llenar");

            });
            $last_tr.append("<input type='text' name='row["+last_tr_index+"][id]' style='display: none;'>");
            var html=html+"<td>";
            html=html+"<input type='text' name='row["+last_tr_index+"][precio_venta]' value='"+precio+"' >";
            html=html+"</td>";
            html=html+"<td>";
            html=html+"<a class='btn btn_editar' href='#'>Editar</a>";
            html=html+"<a class='btn btn_borrar' href='#'>Borrar</a>";
            html=html+"</td>";
            $last_tr.append(html);
            /********************************/
            });//End  done
        } //End valid
        /***********************************/
    });
}

Add_tr_to_table_depo("#depositos","depositos");

function Add_tr_to_table_depo(row_of_inputs,table) {
    $(row_of_inputs+" .agregar_a_tabla").on("click",function () {
        if ($('#form_depositos').valid()){
            $(row_of_inputs+"_t tbody").append("<tr></tr>");
            var $last_tr=$(row_of_inputs+"_t tbody tr").last();
            var last_tr_index=$last_tr.index();
            $last_td=null;
            // $.each($(row_of_inputs+" .campo_llenar"),function (key, value) {
            //Agregamos el texto
            $.each($(row_of_inputs+" .campo_llenar"),function (key, value) {
                $last_td=null;
                $last_tr.append("<td></td>");

                var name=$(value).attr("name");
                var old_name=$(value).attr("name");
                name="row["+last_tr_index+"]["+name+"]";
                $last_td=$last_tr.find("td").last();

                var input=$(value).clone();
                if(input.is( "select" )){
                    var $originalSelects=$(value);
                    var select=$(value).clone();
                    select.each(function(index, item) {
                        //set new select to value of old select
                        $(item).val( $originalSelects.eq(index).val() );

                    });
                    input=select;
                }
                input.attr("disabled",true);
                input.attr("name",name);
                input.attr("id",old_name+"_"+last_tr_index);

                $last_td.append(input);
                $last_td.find("input,select").removeClass("campo_llenar");

            });
            $last_tr.append("<input type='text' name='row["+last_tr_index+"][id]' style='display: none;'>");
            var html=html+"<td>";
            html=html+"<input type='text' name='row["+last_tr_index+"][precio_venta]' value='"+($("#area_techada_depa").val()*$("#valor_deposito_lp").val())+"' >";
            html=html+"</td>";
            html=html+"<td>";
            html=html+"<a class='btn btn_editar' href='#'>Editar</a>";
            html=html+"<a class='btn btn_borrar' href='#'>Borrar</a>";
            html=html+"</td>";
            $last_tr.append(html);
        }
    });
}

Add_tr_to_table_doc("#documento","documento");

function Add_tr_to_table_doc(row_of_inputs,table) {
    $(row_of_inputs+" .agregar_a_tabla").on("click",function () {
        if ($('#form_documentos').valid()){
            adding_tr_td(row_of_inputs,table);
        }
    });
}



Add_tr_to_table_ven("#vendedores","vendedores");

function Add_tr_to_table_ven(row_of_inputs,table) {
    $(row_of_inputs+" .agregar_a_tabla").on("click",function () {
        if ($('#form_vendedores').valid()){
            adding_tr_td(row_of_inputs,table);
        }
    });
}



















//Validaciones
//Configuracion del csrf de laravel no tocar
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//Funcion para cambiar por el tab
function next_button() {
    console.log();
    var $active = $('.wizard .nav-tabs .nav-item .active');
    var $activeli = $active.parent("li");
    $activeli.find('a[data-toggle="tab"]').addClass("disabled");
    $($activeli).next().find('a[data-toggle="tab"]').removeClass("disabled");
    $($activeli).next().find('a[data-toggle="tab"]').click();
}

//Funciones Ajax

//End funciones ajax


function projectValdiate() {
    console.log("Submit project");
    var proyect_form =new FormData($('#project_form_container')[0]);
    $.ajax({
        type: "POST",
        url:  site_url+"/projectos",
        data: proyect_form,
        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        processData: false
    }).done(function(id) {
        id_projecto=id;
        next_button();
    });
}

function create_tipodepartamento(){

    $('#form_tipodepartamento select,#form_tipodepartamento input').removeAttr('disabled');

    console.log("Enviando tipo_departamento");
    var tipodepartamento_form =new FormData($('#form_tipodepartamento')[0]);
    console.log(tipodepartamento_form);
    tipodepartamento_form.append('id_proyecto', id_projecto);
    var create_tipodepartamento=$.ajax({
        type: "POST",
        url:  site_url+"/tipodepartamento",
        data: tipodepartamento_form,
        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        processData: false, // NEEDED, DON'T OMIT THIS
    }).done(function(tipodepartamento) {
        // console.log(JSON.parse(departamentos));
        // departamentos=JSON.parse(JSON.stringify(departamentos));
        // console.log(departamentos);
        // tipodepartamento=JSON.parse(tipodepartamento);
        // $.each(tipodepartamento, function( index, tipodepartamento ) {
        //     $("#deposito_dpo").append("<option value='"+tipodepartamento.idtipodepartamento+"'>"+tipodepartamento.tipodepartamento+"</option>")
        // });
        console.log("tipodepartamento form created");
        next_button();
    });
    $.when( create_tipodepartamento ).done(function(id) {
        $.ajax({
            type: "GET",
            url:  site_url+"/tipodepartamento?id_projecto="+id_projecto,
        }).done(function(tipodepartamento) {
            console.log(tipodepartamento);
            departamentos=JSON.parse(tipodepartamento);
            $.each(departamentos, function( index, departamento ) {
                $("#tipo_departamento_idtipo_departamento").append("<option value='"+departamento.idtipo_departamento+"'>"+departamento.nombre+"</option>")
                $("#tipo_departamento_idtipo_departamento_2").append("<option value='"+departamento.idtipo_departamento+"'>"+departamento.nombre+"</option>")
            });
            console.log("departamento form created");
        });
    });
}

function create_precio() {    
    $('#lp_departamentos').find(':input:disabled').removeAttr('disabled');
    $('#lp_departamentos').find('select:disabled').removeAttr('disabled');
    $('#lp_estacionamientos').find(':input:disabled').removeAttr('disabled');
    $('#lp_estacionamientos').find('select:disabled').removeAttr('disabled');
        var create_precio=$.ajax({
            type: "POST",
            url:  site_url+"/precios",
            data: $('#lp_general').serialize()+"&"+$('#lp_promociones').serialize()
            +"&"+$('#lp_depositos').serialize()+"&proyectos_idproyectos="+id_projecto
        }).done(function(id) {
            id_precio=id;
            console.log(id_precio);
        });
    $.when( create_precio ).done(function(id) {
        idprecios=id;

        var lp_departamentos = new FormData($('#lp_departamentos')[0]);
        lp_departamentos.append('idprecios', idprecios);
        lp_departamentos.append('precios_idprecios', idprecios);
        var lp_estacionamientos = new FormData($('#lp_estacionamientos')[0]);
        lp_estacionamientos.append('idprecios', idprecios);
        lp_estacionamientos.append('precios_idprecios', idprecios);
        console.log(id_projecto);
        lp_estacionamientos.append('id_proyecto', id_projecto);
        var create_precio_dep=$.ajax({
            type: "POST",
            url:  site_url+"/dpto_precios",
            data: lp_departamentos,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
        }).done(function(id) {

        });
        //$('#lp_estacionamientos').serialize()+"&precios_idprecios="+idprecios
        var create_precio_est=$.ajax({
            type: "POST",
            url:  site_url+"/estacio_precios",
            data: lp_estacionamientos,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
        }).done(function(id) {
        });
        $.when( create_precio_dep , create_precio_est).done(function( precio_dep,precio_est ) {
            console.log("Departamentos creado");
                next_button();
        });
    });
}

function create_departamento(){
    $('#form_departamentos').find(':input:disabled').removeAttr('disabled');
    $('#form_departamentos').find('select:disabled').removeAttr('disabled');
    console.log("Enviando Boton");
    var departamentos_form = new FormData($('#form_departamentos')[0]);
    departamentos_form.append('proyectos_idproyectos', id_projecto);
    var create_precio_dep=$.ajax({
        type: "POST",
        url:  site_url+"/departamentos",
        data: departamentos_form,
        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        processData: false, // NEEDED, DON'T OMIT THIS
    }).done(function(departamentos) {
        // console.log(JSON.parse(departamentos));
        // departamentos=JSON.parse(JSON.stringify(departamentos));
        // console.log(departamentos);
        departamentos=JSON.parse(departamentos);
        $.each(departamentos, function( index, departamento ) {
            $("#deposito_dpo").append("<option value='"+departamento.iddepartamentos+"'>"+departamento.departamento+"</option>")
        });
        console.log("departamento form created");
        next_button();
    });
}

function create_estacionamiento(){
    $('#form_estacionamiento').find(':input:disabled').removeAttr('disabled');
    $('#form_estacionamiento').find('select:disabled').removeAttr('disabled');
    console.log("Enviando Boton");
    var departamentos_form = new FormData($('#form_estacionamiento')[0]);
    departamentos_form.append('proyectos_idproyectos', id_projecto);
    var create_precio_dep=$.ajax({
        type: "POST",
        url:  site_url+"/estacionamientos",
        data: departamentos_form,
        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        processData: false, // NEEDED, DON'T OMIT THIS
    }).done(function(id) {
        console.log("estacionamiento form created");
        next_button();
    });
}

function create_depositos(){
    $('#form_depositos').find(':input:disabled').removeAttr('disabled');
    $('#form_depositos').find('select:disabled').removeAttr('disabled');
    console.log("Enviando Boton");
    var departamentos_form = new FormData($('#form_depositos')[0]);
    departamentos_form.append('proyectos_idproyectos', id_projecto);
    var create_precio_dep=$.ajax({
        type: "POST",
        url:  site_url+"/depositos",
        data: departamentos_form,
        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        processData: false, // NEEDED, DON'T OMIT THIS
    }).done(function(id) {
        console.log("estacionamiento form created");
        next_button();
    });
}

function create_documentos(){
    $('#form_documentos').find(':input:disabled').removeAttr('disabled');
    $('#form_documentos').find('select:disabled').removeAttr('disabled');
    console.log("Enviando Boton");
    var departamentos_form = new FormData($('#form_documentos')[0]);
    departamentos_form.append('proyectos_idproyectos', id_projecto);
    var create_precio_dep=$.ajax({
        type: "POST",
        url:  site_url+"/documentos",
        data: departamentos_form,
        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        processData: false, // NEEDED, DON'T OMIT THIS
    }).done(function(id) {
        console.log("documentos form created");
        next_button();
    });
}

function create_vendedores(){
    $('#form_vendedores').find(':input:disabled').removeAttr('disabled');
    $('#form_vendedores').find('select:disabled').removeAttr('disabled');
    console.log("Enviando Boton");
    var departamentos_form = new FormData($('#form_vendedores')[0]);
    departamentos_form.append('proyectos_idproyectos', id_projecto);
    var create_precio_dep=$.ajax({
        type: "POST",
        url:  site_url+"/vendedores",
        data: departamentos_form,
        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
        processData: false, // NEEDED, DON'T OMIT THIS
    }).done(function(id) {
        console.log("documentos form created");
        window.location.replace(site_url+"/projectos");
    });
}



//Aca se declaaran los botones project button por ejemplo crea el boton basico


$(".project_button").on("click",function (e) {
    $(this).attr("disabled",true);
    if ($('#project_form_container').valid()) {
    e.preventDefault();
    projectValdiate();
    } else {
    }
});

$(".tipodepartamento_button").on("click",function (e) {

    if ($("#form_tipodepartamento tbody tr").length > 0) {
        $(this).attr("disabled",true);
        create_tipodepartamento();
    }else{
        $("#form_tipodepartamento table").addClass("error");
        console.log("validating ");
    }
});


$(".precios_button").on("click",function (e) {
    e.preventDefault();
    if(precios_button()){
       create_precio();
       // next_button();
    }
})

    function precios_button() {

        var preciodepa_t=true;
        var precioesta_t=true;

            //Ahora validamos que se hayan crado trs en las 2 tablas
            $("#preciodepa_t").removeClass("error");
            $("#precioesta_t").removeClass("error");
            if ($("#preciodepa_t table tbody tr").length==0) {
                $("#preciodepa_t").addClass("error");
                preciodepa_t= false;
            }

            if ($("#precioesta_t table tbody tr").length==0) {
                $("#precioesta_t").addClass("error");
                precioesta_t= false;
            }


            //colocar en rojo los forms

            $('#lp_general').valid();
            $('#lp_promociones').valid();
            $('#lp_depositos').valid();

            //Checar que todos los forms sean validos

            if ($('#lp_general').valid() &&
                $('#lp_promociones').valid() &&
                $('#lp_departamentos').valid() &&
                $('#lp_depositos').valid() &&
                preciodepa_t &&
                precioesta_t
                 ) {
              return true;
            } else {
                return false;
            }



}



$(".departamento_button").on("click",function (e) {

    if ($("#form_departamentos tbody tr").length > 0) {
        $(this).attr("disabled",true);
        create_departamento();
    }else{
        $("#form_departamentos table").addClass("error");
        console.log("validating ");
    }


});

$(".estacionamiento_button").on("click",function (e) {
    if ($("#form_estacionamiento tbody tr").length > 0) {
        $(this).attr("disabled",true);
        create_estacionamiento();
    }else{
        $("#form_estacionamiento table").addClass("error");
        console.log("validating ");
    }
});



$(".deposito_button").on("click",function (e) {
    if ($("#form_depositos tbody tr").length > 0) {
        $(this).attr("disabled",true);
        create_depositos();
    }else{
        $("#form_depositos table").addClass("error");
        console.log("validating ");
    }
});


$(".documento_button").on("click",function (e) {
    if ($("#form_documentos tbody tr").length > 0) {
        $(this).attr("disabled",true);
        create_documentos();
    }else{
        $("#form_documentos table").addClass("error");
        console.log("validating ");
    }
});


$(".vendedor_button").on("click",function (e) {
    if ($("#form_vendedores tbody tr").length > 0) {
        $(this).attr("disabled",true);
        create_vendedores();
    }else{
        $("#form_vendedores table").addClass("error");
        console.log("validating ");
    }
});


    /*************************************************************/
//Validaciones generales
    function detect_if_empty(string) {
        if(string==""){
        return false;
        }else{
            return true;
        }
    }