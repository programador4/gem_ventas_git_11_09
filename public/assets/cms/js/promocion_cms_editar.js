//START Validate de los formularios
//Aca empezamos las validaciones de formularios abajo lo llamaremos conla funcion validate();
//form 1)Crear Proyecto
$("#promocion_form_container").validate({
    rules: {
        nombre:{
            required: true
        },
        descuento:{
            required:true,
            number:true
        },
        fecha_inicio:{
            required:true            
        },
        fecha_fin:{
            required:true
        }
    },
    menssajes: {
        nombre:{
            required: ""
        },
        descuento:{
            required:""
        },
        fecha_inicio:{
            required:""
        },
        fecha_fin:{
            required:""
        }
    }
});

//form_departamentos
//END Validate de los formularios




//JS de los tabs
//Start js tabs
//Initialize tooltips
$('.nav-tabs > li a[title]').tooltip();

//Wizard
$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

    var $target = $(e.target);
    console.log($target);
    if ($target.hasClass('disabled')) {
        return false;
    }
});

$(".next-step").click(function (e) {
    var $active = $('.wizard .nav-tabs .nav-item .active');
    var $activeli = $active.parent("li");

    $($activeli).next().find('a[data-toggle="tab"]').removeClass("disabled");
    $($activeli).next().find('a[data-toggle="tab"]').click();
});


$(".prev-step").click(function (e) {

    var $active = $('.wizard .nav-tabs .nav-item .active');
    var $activeli = $active.parent("li");

    $($activeli).prev().find('a[data-toggle="tab"]').removeClass("disabled");
    $($activeli).prev().find('a[data-toggle="tab"]').click();

});



//Validaciones
//Configuracion del csrf de laravel no tocar
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//Funcion para cambiar por el tab
function next_button() {
    console.log();
    var $active = $('.wizard .nav-tabs .nav-item .active');
    var $activeli = $active.parent("li");
    $activeli.find('a[data-toggle="tab"]').addClass("disabled");
    $($activeli).next().find('a[data-toggle="tab"]').removeClass("disabled");
    $($activeli).next().find('a[data-toggle="tab"]').click();
}

//Funciones Ajax

//End funciones ajax


function promocionValdiate() {
    $.ajax({
        type: "POST",
        url:  site_url+"/promocion",
        data: $('#promocion_form_container').serialize()+"&estado=1"
    }).done(function(id) {
        id_promocion=id;
    });
}


//Aca se declaaran los botones project button por ejemplo crea el boton basico


$(".promocion_button").on("click",function (e) {
    $(this).attr("disabled",true);
    if ($('#promocion_form_container').valid()) {
    e.preventDefault();
    promocionValdiate();
    } else {
    }
});


    /*************************************************************/
//Validaciones generales
    function detect_if_empty(string) {
        if(string==""){
        return false;
        }else{
            return true;
        }
    }