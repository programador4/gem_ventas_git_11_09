$( document ).ready( function(){
  
    $('.icon').on('click', function(){
        $(this).toggleClass('active');
    });
    $(".control").click(function() {
        $(".lista").toggleClass("menu-abrir");
    });
    
    $( ".menu .enlace" ).on("click", function( evt ){
        $( "#control-menu" ).prop( "checked", false);
    });


	$( ".formulario" )
        .find( "input[type=text], input[type=tel], input[type=tel]" )
        .on( "keypress", function( evt ) {
            var $c = $(evt.currentTarget);
            var caracter, anterior, futuro, patron;
            var chc = evt.charCode;
            if ( chc > 0 ) {
                caracter = String.fromCharCode( chc );
                anterior = $c.val();
                patron = new RegExp( $c.data("restriccion") );
                futuro = anterior + caracter;
                if ( patron.test( futuro ) != true ) {
                    evt.preventDefault();
                }
            }
        });
 
  var dom = dom || document;
var datos;

window.addEventListener( "load", evt => {
    datos = datos || dom.forms[0];

    var quill = new Quill( '.editor', {
        theme: 'snow',
        modules: {
            toolbar: [
                [{ header: [1, 2, false] }],
                ['bold', 'italic', 'underline'],
                ['link', 'blockquote', 'image'],
                [{ list: 'ordered' }, { list: 'bullet' }]
            ]
        },
        scrollingContainer: '.contenidos', 
        placeholder: "Texto de la noticia"
    });

    datos.addEventListener( "submit", evt =>{
        var editor = datos.querySelector( ".ql-editor" );
        datos["contenido"].value = editor.innerHTML;
    });
});


   
});



//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJnZW5lcmFsLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiQoIGRvY3VtZW50ICkucmVhZHkoIGZ1bmN0aW9uKCl7XHJcbiAgXHJcbiAgICAkKCcuaWNvbicpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XHJcbiAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICB9KTtcclxuICAgICQoXCIuY29udHJvbFwiKS5jbGljayhmdW5jdGlvbigpIHtcclxuICAgICAgICAkKFwiLmxpc3RhXCIpLnRvZ2dsZUNsYXNzKFwibWVudS1hYnJpclwiKTtcclxuICAgIH0pO1xyXG4gICAgXHJcbiAgICAkKCBcIi5tZW51IC5lbmxhY2VcIiApLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oIGV2dCApe1xyXG4gICAgICAgICQoIFwiI2NvbnRyb2wtbWVudVwiICkucHJvcCggXCJjaGVja2VkXCIsIGZhbHNlKTtcclxuICAgIH0pO1xyXG5cclxuXHJcblx0JCggXCIuZm9ybXVsYXJpb1wiIClcclxuICAgICAgICAuZmluZCggXCJpbnB1dFt0eXBlPXRleHRdLCBpbnB1dFt0eXBlPXRlbF0sIGlucHV0W3R5cGU9dGVsXVwiIClcclxuICAgICAgICAub24oIFwia2V5cHJlc3NcIiwgZnVuY3Rpb24oIGV2dCApIHtcclxuICAgICAgICAgICAgdmFyICRjID0gJChldnQuY3VycmVudFRhcmdldCk7XHJcbiAgICAgICAgICAgIHZhciBjYXJhY3RlciwgYW50ZXJpb3IsIGZ1dHVybywgcGF0cm9uO1xyXG4gICAgICAgICAgICB2YXIgY2hjID0gZXZ0LmNoYXJDb2RlO1xyXG4gICAgICAgICAgICBpZiAoIGNoYyA+IDAgKSB7XHJcbiAgICAgICAgICAgICAgICBjYXJhY3RlciA9IFN0cmluZy5mcm9tQ2hhckNvZGUoIGNoYyApO1xyXG4gICAgICAgICAgICAgICAgYW50ZXJpb3IgPSAkYy52YWwoKTtcclxuICAgICAgICAgICAgICAgIHBhdHJvbiA9IG5ldyBSZWdFeHAoICRjLmRhdGEoXCJyZXN0cmljY2lvblwiKSApO1xyXG4gICAgICAgICAgICAgICAgZnV0dXJvID0gYW50ZXJpb3IgKyBjYXJhY3RlcjtcclxuICAgICAgICAgICAgICAgIGlmICggcGF0cm9uLnRlc3QoIGZ1dHVybyApICE9IHRydWUgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZXZ0LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuIFxyXG4gIHZhciBkb20gPSBkb20gfHwgZG9jdW1lbnQ7XHJcbnZhciBkYXRvcztcclxuXHJcbndpbmRvdy5hZGRFdmVudExpc3RlbmVyKCBcImxvYWRcIiwgZXZ0ID0+IHtcclxuICAgIGRhdG9zID0gZGF0b3MgfHwgZG9tLmZvcm1zWzBdO1xyXG5cclxuICAgIHZhciBxdWlsbCA9IG5ldyBRdWlsbCggJy5lZGl0b3InLCB7XHJcbiAgICAgICAgdGhlbWU6ICdzbm93JyxcclxuICAgICAgICBtb2R1bGVzOiB7XHJcbiAgICAgICAgICAgIHRvb2xiYXI6IFtcclxuICAgICAgICAgICAgICAgIFt7IGhlYWRlcjogWzEsIDIsIGZhbHNlXSB9XSxcclxuICAgICAgICAgICAgICAgIFsnYm9sZCcsICdpdGFsaWMnLCAndW5kZXJsaW5lJ10sXHJcbiAgICAgICAgICAgICAgICBbJ2xpbmsnLCAnYmxvY2txdW90ZScsICdpbWFnZSddLFxyXG4gICAgICAgICAgICAgICAgW3sgbGlzdDogJ29yZGVyZWQnIH0sIHsgbGlzdDogJ2J1bGxldCcgfV1cclxuICAgICAgICAgICAgXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc2Nyb2xsaW5nQ29udGFpbmVyOiAnLmNvbnRlbmlkb3MnLCBcclxuICAgICAgICBwbGFjZWhvbGRlcjogXCJUZXh0byBkZSBsYSBub3RpY2lhXCJcclxuICAgIH0pO1xyXG5cclxuICAgIGRhdG9zLmFkZEV2ZW50TGlzdGVuZXIoIFwic3VibWl0XCIsIGV2dCA9PntcclxuICAgICAgICB2YXIgZWRpdG9yID0gZGF0b3MucXVlcnlTZWxlY3RvciggXCIucWwtZWRpdG9yXCIgKTtcclxuICAgICAgICBkYXRvc1tcImNvbnRlbmlkb1wiXS52YWx1ZSA9IGVkaXRvci5pbm5lckhUTUw7XHJcbiAgICB9KTtcclxufSk7XHJcblxyXG5cclxuICAgXHJcbn0pO1xyXG5cclxuXHJcbiJdLCJmaWxlIjoiZ2VuZXJhbC5qcyJ9
